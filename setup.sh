# Export package specific environmental variables
echo "Setting up GPIODrivers environment..."

# Any special setup can be configured here.
#export CC=/opt/ohpc/pub/compiler/gcc/8.3.0/bin/gcc
#export CXX=/opt/ohpc/pub/compiler/gcc/8.3.0/bin/g++
#export THIRD_PARTY_DIRECTORIES="/usr/include/nlohmann"

# So that dynamically linked libraries can be found
#export LD_LIBRARY_PATH=/opt/ohpc/pub/compiler/gcc/8.3.0/lib64/:/usr/local/lib64/:$LD_LIBRARY_PATH
#export PATH=/opt/ohpc/pub/compiler/gcc/8.3.0/bin:$PATH

if ! [[ -z "${THIRD_PARTY_DIRECTORIES}" ]];
  then echo "THIRD_PARTY_DIRECTORIES: ${THIRD_PARTY_DIRECTORIES}"
fi

BASH_SOURCE_DIR="$(dirname ${BASH_SOURCE[0]})"
pushd $BASH_SOURCE_DIR > /dev/null
GPIODRIVERS_TOP=$PWD
popd > /dev/null
echo "GPIODRIVERS_TOP directory = $GPIODRIVERS_TOP"

echo "Loading gpiodrivers apps"
export PATH=${GPIODRIVERS_TOP}/build/TRBAccess/src/app/:${GPIODRIVERS_TOP}/build/TLBAccess/src/app/:$PATH

echo "Done"
