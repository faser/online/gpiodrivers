/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 
#include<iostream>
#include <iomanip>
#include <bitset>
#include <unistd.h> //for usleep
#define _ms 1000 // used for usleep
#include "TLBAccess/TLBAccess.h"
#include "TLBAccess/TLB_ConfigRegisters.h"

using namespace FASER;
using namespace FASER::TLB;
using namespace FASER::TLBConstants;

ConfigReg::ConfigReg(uint16_t FW_version) : m_FPGA_version(FW_version){

  INFO("TLB::ConfigReg: Configuring for FW version 0x"<<std::hex<<m_FPGA_version<<std::dec);

  kCONFIG_DATA_BITSHIFT = 12;
  if (m_FPGA_version >= FW_VERSION_1p9) {
      kTOTAL_CONFIGWORDS = 18;
      kCONFIG_DATA_BITSHIFT = 11;
  }
  else if (m_FPGA_version >= FW_VERSION_1p6) {
      kTOTAL_CONFIGWORDS = 16;
  }
  else kTOTAL_CONFIGWORDS = 15;

  DEBUG("Expecting "<<static_cast<int>(kTOTAL_CONFIGWORDS)<<" total usr configuration words for this FW version.");

  m_configBits.resize(kCONFIG_DATA_BITSHIFT*kTOTAL_CONFIGWORDS,0);

  // define mask for config value and index parts of a config word: config words are 16 bits,
  // with the first N=kCONFIG_DATA_BITSHIFT LSB defining the value and the (remainder) MSB the index.
  USR_CFG_DATA_MASK = static_cast<uint16_t>((1<<kCONFIG_DATA_BITSHIFT)-1);
  USR_CFG_IDX_MASK  = static_cast<uint16_t>(((1<<(16-kCONFIG_DATA_BITSHIFT))-1)<<kCONFIG_DATA_BITSHIFT);

}

/**
 *  @brief Sets the bits for a given configuration setting.
 *
 *  @details .
 *
 *  @param configParam Of type ConfigMap and provides the position of the configuration that is to be set.
 *  @param value The value of the setting
 *  @param channel An optional parameter to specify the channel to be set for configurations that apply to an array of channels.
 *
 *  @note The following is checked and will result in WARNING or ERRROR mesages if not satisfied: 
 *        - \p value cannot be larger than its maximum possible value (determined by the number of bits it occupies in the memory layout).
 *             If this check fails, \p value is set to the maximum possible value along with a WARNING.
 *        - \p channel cannot exceed the total number of channels settable, by ensuring that no bits not mapped to the given configuration are modified.
 *             If this check fails, no bits are modified and an ERROR message is printed.
 *        - A bit cannot be modified if it lies outside of the range of bits mapped to the configuration.
 *          If this check failes, no bits are modified and an ERROR message is printed. This should never happen and would indicate a deeper software bug.
 *
 *  @return void
 */
template<uint8_t P1, uint8_t P2, size_t P3=1>
void ConfigReg::SetConfig( const ConfigMap<P1,P2,P3>& configParam, unsigned int value, uint8_t channel ){

  switch ( configParam.bit_size ) {
   case 1: {
     if ( value > sizeof(bool) ) {
       WARNING("Given value ("<<value<<") exceeds maximum configurable value. Setting value to maximum configurable ("<<sizeof(bool)<<")");
       value = sizeof(bool);
     }
     break;
   } 
   case 2: {
     if ( value > kUINT2_MAX ) {
       WARNING("Given value ("<<value<<") exceeds maximum configurable value. Setting value to maximum configurable ("<<kUINT2_MAX<<")");
       value = kUINT2_MAX;
     }
     break;
   } 
   case 3: {
     if ( value > kUINT3_MAX ) {
       WARNING("Given value ("<<value<<") exceeds maximum configurable value. Setting value to maximum configurable ("<<kUINT3_MAX<<")");
       value = kUINT3_MAX;
     }
     break;
   } 
   case 7: {
     if ( value > kUINT7_MAX ) {
       WARNING("Given value ("<<value<<") exceeds maximum configurable value. Setting value to maximum configurable ("<<(int)kUINT7_MAX<<")");
       value = kUINT7_MAX;
     }
     break;
   } 
   case 8: {
     if ( value > UINT8_MAX ) {
       WARNING("Given value ("<<value<<") exceeds maximum configurable value. Setting value to maximum configurable ("<<(int)UINT8_MAX<<")");
       value = UINT8_MAX;
     }
     break;
   } 
   case 12: {
     if ( value > kUINT12_MAX ) {
       WARNING("Given value ("<<value<<") exceeds maximum configurable value. Setting value to maximum configurable ("<<kUINT12_MAX<<")");
       value = kUINT12_MAX;
     }
     break;
   } 
   case 20: {
     if ( value > kUINT20_MAX ) {
       WARNING("Given value ("<<value<<") exceeds maximum configurable value. Setting value to maximum configurable ("<<kUINT20_MAX<<")");
       value = kUINT20_MAX;
     }
     break;
   } 
   case 32: {
     if ( value > UINT32_MAX ) {
       WARNING("Given value ("<<value<<") exceeds maximum configurable value. Setting value to maximum configurable ("<<UINT32_MAX<<")");
       value = UINT32_MAX;
     }
     break;
   } 
   default:
     WARNING("No check on configurations of bit size "<<(int)configParam.bit_size<<" exists. Be sure value to configure does not exceed maximum allowed value."); 
  }

  uint16_t channelStartIdx = configParam.start_bit_idx+configParam.bit_size*channel;
  if ( channelStartIdx > configParam.end_bit_idx ){
      ERROR("Trying to set a configuration for channel "<<(int)channel<<", which is out of range.");
  }

  for ( uint8_t i = 0; i < configParam.bit_size; i++ ) {
    bool bit = (value & ( 1 << i )) >> i;
    uint8_t bitIdx = channelStartIdx + i;
    if ( bitIdx > configParam.end_bit_idx ){
      ERROR("Attempting to set a configuration bit that is out of range. bitIdx ("<<(int)bitIdx<<") cannot be higher than "<<(int)configParam.end_bit_idx);
    }
    else {
      m_configBits.set(bitIdx, bit);
    }
  }
}

/**
 *  @brief Converts and retrieves the configuration words as a vector.
 *
 *  @details The bitset of configurations is converted to string and split into substrings of size 16 bits.
 *           Each substring is pushed into a vector as a 16 bit unsigned integer. 
 *           The 16 bit items are in the correct format to be sent directly via TLBCmdID::USER_SET_CONFIG.
 *
 *  @return std::vector<uint16_t>
 */
std::vector<uint16_t> ConfigReg::GetConfigWords(){

  std::string bigCfgStr;
  boost::to_string(m_configBits,bigCfgStr);
  std::reverse(bigCfgStr.begin(), bigCfgStr.end());
  std::vector<uint16_t> configWords;

  // there are kTOTAL_CONFIGWORDS words in total with idx 0 .. kTOTAL_CONFIGWORDS-1
  // kTOTAL_CONFIGWORDS and kCONFIG_DATA_BITSHIFT depend on FW version (see constructor)
  for ( uint8_t idx = 0; idx < kTOTAL_CONFIGWORDS; idx++ ){
    std::string part = bigCfgStr.substr( kCONFIG_DATA_BITSHIFT*idx, kCONFIG_DATA_BITSHIFT);
    std::reverse(part.begin(), part.end());
    uint16_t word = (uint16_t)std::stoi(part, nullptr, 2);
    word|=(idx<<kCONFIG_DATA_BITSHIFT); // set word idx
    configWords.push_back(word);
  }
  
  return configWords;

}

/**
 *  @brief Together with SetSamplingPhase2, configures the sampling phase at 160 MHz interval for a given channel
 *
 *  @details The sampling phase bit together with sampling phase 2 bit configures the following relative delay of the phase with respect to the clock rising edge:
 *           Phase 1 bit: 0 Phase 2 bit: 0 -> sampling delay 0 ns
 *           Phase 1 bit: 1 Phase 2 bit: 1 -> sampling delay 6.25 ns
 *           Phase 1 bit: 1 Phase 2 bit: 0 -> sampling delay 12.5 ns
 *           Phase 1 bit: 0 Phase 2 bit: 1 -> sampling delay 18.25 ns
 *
 *           The channel is the number of the LVDS input channel from the digitizer to the TLB.
 *
 *  @param channel The LVDS input channel for which the sampling phase is to be set.
 *  @param bit Value of the sampling phase.
 *
 *  @return void
 */
void ConfigReg::SetSamplingPhase(uint8_t channel,bool bit) {
  SetConfig( map_SamplingPhase, (uint8_t)bit, channel);
}

/**
 *  @brief Sets the input delay for a given channel.
 *
 *  @details An input delay of 0-3 clock cycles can be set for a given channel.
 *           The channel is the number of the LVDS input channel from the digitizer to the TLB.
 *
 *  @param channel The LVDS input channel for which the input delay is to be set.
 *  @param value Value of the input delay.
 *
 *  @note If the users sets a value greater than 3 then the values is automatically reset to 3.
 *
 *  @return void
 */
void ConfigReg::SetInputDelay(uint8_t channel, unsigned int value) {
  SetConfig( map_InputDelay, value, channel );
}


/**
 *  @brief Sets the internal trigger rate.
 *
 *  @details A rate setting of 0 (maximum rate) to 7 (minimum rate) can be set. 
 *           The resulting rate will depend on the setting of the TriggerGeneratorDerandomizer. 
 *           See table of rates for TriggerGeneratorDerandomizer ON: <a href="https://espace.cern.ch/faser-share/Shared%20Documents/Trigger,%20DAQ%20and%20DCS/TLB_fixed_trigger_settings.pdf">link Mapping of Fixed Trigger Rate Settings</a> 
 *           @see ConfigReg:SetInternalTriggerGeneratorDerandomizer.
 *            
 *
 *  @param value Value of the internal trigger rate setting.
 *
 *  @note If the users sets a value greater than 7 then the value is automatically reset to 7.
 *
 *  @return void
 */
void ConfigReg::SetInternalTriggerRate(unsigned int value){
  SetConfig( map_InternalTriggerRate, value );
}
  
/**
 *  @brief Sets the prescale for a given trigger line.
 *
 *  @details A prescale of 0-255 can be set for a trigger line.
 *           Trigger line mapping: idx 0 = physics trigger line 0
 *                                 idx 1 = physics trigger line 1
 *                                 idx 2 = physics trigger line 2
 *                                 idx 3 = physics trigger line 3
 *                                 idx 4 = internal trigger
 *                                 idx 5 = software & calibration trigger (calibration trigger enabled for TLB FW v0.15+)
 *
 *  @param channel The trigger line for which the prescale is to be set.
 *  @param value Value of the prescale.
 *
 *  @note If the users sets a value greater than 255 then the values is automatically reset to 255.
 *
 *  @return void
 */
void ConfigReg::SetPrescale( uint8_t channel, unsigned int value){
 SetConfig( map_Prescales, value, channel ); 
}

/**
 *  @brief Sets the L1A/BCR delay to tracker/pixel preshower on
 *  RJ45 port 0 (labelled "Tracker 0" on TLB front panel)
 *  Setting labelled "TrackerDelay" in FW docs.
 *
 *  @details This configures the delay of the L1A & BCR signal from the TLB to the TRB/pixel preshower.
 *           A delay of 0-127 clock cycles can be set.
 *
 *  @param value Value of the delay.
 *
 *  @note If the users sets a value greater than 127 then the values is automatically reset to 127.
 *
 *  @return void
 */
void ConfigReg::SetRJ45Port0Delay( unsigned int value){
 SetConfig( map_RJ45Port0Delay, value );
}

/**
 *  @brief Sets the L1A/BCR delay to tracker/pixel preshower on
 *  RJ45 port 1 (labelled "Tracker 1" on TLB front panel)
 *  Setting labelled "Tracker1Delay" in FW docs.
 *
 *  @details This configures the delay of the L1A & BCR signal from the TLB to the TRB/pixel preshower.
 *           A delay of 0-127 clock cycles can be set.
 *
 *  @param value Value of the delay.
 *
 *  @note If the users sets a value greater than 127 then the values is automatically reset to 127.
 *
 *  @return void
 */
void ConfigReg::SetRJ45Port1Delay( unsigned int value){
 SetConfig( map_RJ45Port1Delay, value );
}

/**
 *  @brief Sets the digitizer delay.
 *
 *  @details This configures the delay of the L1A & BCR signal from the TLB to the digitizer. 
 *           A digitizer delay of 0-127 clock cycles can be set.
 *
 *  @param value Value of the digitizer delay.
 *
 *  @note If the users sets a value greater than 127 then the values is automatically reset to 127.
 *
 *  @return void
 */
void ConfigReg::SetDigitizerDelay( unsigned int value){
 SetConfig( map_DigitizerDelay, value ); 
}

/**
 *  @brief Enables or disables the external clock orbit.
 *
 *  @details If enabled the TLB will use the LHC clock orbit signal, else the TLB will use the internally generated orbit (BCR) signal.
 *
 *  @param value Value of type bool that enables (1) or disables (0) this setting.
 *
 *  @return void
 */
void ConfigReg::SetLHC_CLK( bool value){
 SetConfig( map_LHC_CLK, (uint8_t)value ); 
}

/**
 *  @brief Sets the orbit delay.
 *
 *  @details This configures the delay of the BCR signal before it is processed and further transmitted.
 *           A orbit delay of 0-4095 clock cycles can be set.
 *
 *  @param value Value of the orbit delay.
 *
 *  @note If the users sets a value greater than 4095 then the values is automatically reset to 4095.
 *
 *  @return void
 */
void ConfigReg::SetOrbitDelay( unsigned int value){
 SetConfig( map_OrbitDelay, value ); 
}

/**
 *  @brief Sets the simple deadtime.
 *
 *  @details This configures the number of clock cyles that are vetoed after a TLB L1A signal (simple deadtime).
 *           A deadtime of 0-4095 clock cycles can be set. 
 *
 *  @param value Value of the deadtime.
 *
 *  @note If the users sets a value greater than 4095 then the values is automatically reset to 4095.
 *
 *  @return void
 */
void ConfigReg::SetDeadtime( unsigned int value){
 SetConfig( map_Deadtime, value ); 
}

/**
 *  @brief Sets the monitoring rate.
 *
 *  @details Sets the rate at which the TLB sends out monitoring fragments. The units of the setting is in number of bunch crossing resest (BCRs).
 *  A value of 11245 corresponds to a monitoring fragment per second (assuming LHC clock frequency of 40.078973 MHz).
 *  A monitoring rate setting of 0-1048575 can be set.
 *
 *  @param value Value of the monitoring rate setting.
 *
 *  @note If the users sets a value greater than 1048575 then it automatically sets the value to 1048575.
 *
 *  @return void
 */
void ConfigReg::SetMonitoringRate( uint32_t value){
 SetConfig( map_MonitoringRate, value ); 
}

/**
 *  @brief Sets the output destination.
 *
 *  @details Sets the IP address to which the TLB will send data to. Only applicable with ethernet communication. Currently not implemented.
 *
 *  @param value Value of the output destination
 *
 *  @note If the users sets a value greater than 4294967295 then it automatically sets the value to 4294967295. A
 *  @bug If setting ever implemented, fall back value should be a default not max value (or throw exception).
 *
 *  @return void
 */
void ConfigReg::SetOutputDestination( uint32_t value){
 SetConfig( map_OutputDest, value ); 
}
// FIXME fall back value should be exceptionally handled to use a default rather than max value if ever implemented

/**
 *  @brief Enables or disables a given input channel.
 *
 *  @details A setting of 1 (0) enables (disables) the given input channel.
 *           The channel is the number of the LVDS input channel from the digitizer to the TLB.
 *
 *  @param channel The LVDS input channel for which the input is to be enabled or disabled.
 *  @param value Value of the input enable setting.
 *
 *  @return void
 */
void ConfigReg::SetInputEnable( uint8_t channel, bool value){
 SetConfig( map_InputEnable, (uint16_t)value, channel ); 
}

/**
 *  @brief Configures the internal trigger derandomizer.
 *
 *  @details If setting given is true, the internal trigger becomes a fixed trigger. If false, it becomes a random trigger.
 *
 *  @param value A bool to set the derandomizer ON/OFF.
 *
 *  @return void
 */
void ConfigReg::SetInternalTriggerGeneratorDerandomizer( bool value){
 SetConfig( map_Derandomize, (uint16_t)value ); 
}

/**
 *  @brief Configures the rate limiter maximum rate.
 *
 *  @details 
 *
 *  @param value A 3-bit value to set the rate limit. "0": RateLimiter default (2.2kHz), 4-7: Minimum possible (11.2kHz)
 *
 *  @return void
 */
void ConfigReg::SetRateLimiterNumber( uint16_t value){
 SetConfig( map_RateLimiter, value ); 
}

/**
 *  @brief Together with SetSamplingPhase, configures the sampling phase at 160 MHz interval for a given channel
 *
 *  @details The sampling phase bit together with sampling phase 1 bit configures the following relative delay of the phase with respect to the clock rising edge:
 *           Phase 1 bit: 0 Phase 2 bit: 0 -> sampling delay 0 ns
 *           Phase 1 bit: 1 Phase 2 bit: 1 -> sampling delay 6.25 ns
 *           Phase 1 bit: 1 Phase 2 bit: 0 -> sampling delay 12.5 ns
 *           Phase 1 bit: 0 Phase 2 bit: 1 -> sampling delay 18.25 ns
 *
 *           The channel is the number of the LVDS input channel from the digitizer to the TLB.
 *
 *  @param value A 8-bit value to set the sampling phase 2.
 *
 *  @return void
 */
void ConfigReg::SetSamplingPhase2( uint8_t channel,bool bit ){
 SetConfig( map_SamplingPhase2, (uint8_t)bit, channel );
}

/////////////////////
//Direct Parameters//
/////////////////////

/**
 *  @brief Sets the bit for a direct parameter.
 *
 *  @details Modifies DirectParam by setting bit at position bitIdx to given bit value. 
 *
 *  @param bitIdx Index of bit in DirectParam to be set.
 *  @param bit Bool value of bit to be set.
 *
 *  @return void
 */
void ConfigReg::SetDirectParamBit(uint8_t bitIdx, bool bit){
  uint16_t to_set = ((uint16_t)bit)<<bitIdx;
  DirectParam&=~to_set;   
  DirectParam|=to_set;
}

/**
 *  @brief Sets the reset bit.
 *
 *  @details Sets the reset bit in DirectParam to value given.
 *
 *  @param bit Bool value to be set
 *
 *  @return void
 */
void ConfigReg::SetReset(bool bit){
  SetDirectParamBit(kRESET_BITIDX, bit);
}

/**
 *  @brief Sets the ECR bit.
 *
 *  @details Sets the ECR bit in DirectParam to value given.
 *
 *  @param bit Bool value to be set
 *
 *  @return void
 */
void ConfigReg::SetECR(bool bit){
  SetDirectParamBit(kECR_BITIDX, bit);
}

/**
 *  @brief Sets the ECR bit.
 *
 *  @details Sets the ECR bit in DirectParam to value given.
 *
 *  @param bit Bool value to be set
 *
 *  @return void
 */
void ConfigReg::SetTriggerEnable(bool bit){
  SetDirectParamBit(kTRIGENABLE_BITIDX, bit);
}

/**
 *  @brief Sets the software trigger bit.
 *
 *  @details Sets the software trigger bit in DirectParam to value given.
 *           This will generate an internal software trigger once upon sendind direct parameters to TLB.
 *
 *  @param bit Bool value to be set
 *
 *  @return void
 */
void ConfigReg::SetSoftwareTrigger(bool bit){
  SetDirectParamBit(kSWTRIGGER_BITIDX, bit);
}

/**
 *  @brief Sets the Busy0 disable bit.
 *
 *  @details Sets the Busy0 disable bit in DirectParam to value given.
 *           This will disable (1) or enable (0) the busy asserted via the RJ45 port 0.
 *
 *  @param bit Bool value to be set
 *
 *  @return void
 */
void ConfigReg::SetBusy0Disable(bool bit){
  SetDirectParamBit(kBUSY0DIS_BITIDX, bit);
}

/**
 *  @brief Sets the Busy1 disable bit.
 *
 *  @details Sets the Busy1 disable bit in DirectParam to value given.
 *           This will disable (1) or enable (0) the busy asserted via the RJ45 port 1.
 *
 *  @param bit Bool value to be set
 *
 *  @return void
 */
void ConfigReg::SetBusy1Disable(bool bit){
  SetDirectParamBit(kBUSY1DIS_BITIDX, bit);
}

/////////////////////////////
//Data Readout Parameters////
/////////////////////////////

/**
 *  @brief Sets the bit for a readout parameter.
 *
 *  @details Modifies DataReadoutParam by setting bit at position bitIdx to given bit value. 
 *
 *  @param bitIdx Index of bit in DataReadoutParam to be set.
 *  @param bit Bool value of bit to be set.
 *
 *  @return void
 */
void ConfigReg::SetReadoutParamBit(uint8_t bitIdx, bool bit){
  uint16_t to_set = ((uint16_t)bit)<<bitIdx;
  DataReadoutParam&=~to_set;   
  DataReadoutParam|=to_set;
}

/**
 *  @brief Sets the EnableTriggerData bit.
 *
 *  @details Sets the EnableTriggerData bit in DataReadoutParam to value given.
 *           If enabled, the TLB will read out trigger data fragments.
 *
 *  @param bit Bool value to be set
 *
 *  @return void
 */
void ConfigReg::SetEnableTriggerData(bool bit){
  SetReadoutParamBit( kENABLETRIGDATA_BITIDX, bit );
}

/**
 *  @brief Sets the EnableMonitoringData bit.
 *
 *  @details Sets the EnableMonitoringData bit in DataReadoutParam to value given.
 *           If enabled, the TLB will read out monitoring data fragments.
 *
 *  @param bit Bool value to be set
 *
 *  @return void
 */
void ConfigReg::SetEnableMonitoringData(bool bit){
  SetReadoutParamBit( kENABLEMONDATA_BITIDX, bit );
}

/**
 *  @brief Sets the FIFOReset bit.
 *
 *  @details Sets the FIFOSet bit in DataReadoutParam to value given.
 *
 *  @param bit Bool value to be set
 *
 *  @return void
 */
void ConfigReg::SetReadoutFIFOReset(bool bit){
  SetReadoutParamBit( kFIFORESET_BITIDX, bit );
}

/**
 *  @brief Prints the configuration settings in human readable format contained in the vector given.
 *
 *  @details Prints the complete set of configuration settings in human readable format contained in the vector of 16 bit words given.
 *           The words are first extracted and added to one long sring.
 *           The string is transferred to Config::Reg print().
 *
 *  @note If the size of the given vector is smaller than the expected total number of configurations, an ERROR message will be displayed and no configurations will be printed.
 *
 *  @return void
 */
void ConfigReg::PrintConfigs(const std::vector<uint16_t>& configWords){

  if (configWords.size() != kTOTAL_CONFIGWORDS){
    ERROR("ERROR in printing TLB configurations. Total number of configuration words should be "<<kTOTAL_CONFIGWORDS<<" but is "<<configWords.size());
    return; // FIXME Throw exception?
  }

  std::string bigCfgStr (kTOTAL_CONFIGWORDS*kCONFIG_DATA_BITSHIFT, '0');

  for ( unsigned int i = 0; i < configWords.size(); i++ ){
     uint16_t idx = (configWords.at(i) & USR_CFG_IDX_MASK) >> kCONFIG_DATA_BITSHIFT;
     std::string word;
     auto bitset = boost::dynamic_bitset<>(kCONFIG_DATA_BITSHIFT, configWords.at(i) & USR_CFG_DATA_MASK);
     boost::to_string(bitset,word);
     std::reverse(word.begin(), word.end());
     bigCfgStr.replace(idx*kCONFIG_DATA_BITSHIFT, kCONFIG_DATA_BITSHIFT, word);
  }

  print(bigCfgStr);

}

/**
 *  @brief Prints the configuration settings in human readable format contained in string given.
 *
 *  @details Decodes string given and prints configuration settings and decoded values to the output stream.
 *
 *  @note This is called by ConfigReg::PrintConfigs() where the config string is initialized to the correct size.
 *        This calls ConfigReg::GetConfigSetting(std::string, const ConfigMap<P1,P2,P3>&) to decode the values of the confiuration settings.
 *
 *  @return std::ostream&
 */
void ConfigReg::print(std::string cfgStr) {

  std::stringstream printStr;

  printStr << "Trigger Readout Board Configurations:\n"<<std::dec
     <<std::setw(27)<<" Enabled inputs: "<<std::setfill(' ')<<std::setw(32)<<GetConfigSetting<std::string>(cfgStr, map_InputEnable)<<std::setfill(' ')<<std::endl
     <<std::setw(27)<<" Sampling phase: "<<std::setfill(' ')<<std::setw(32)<<GetConfigSetting<std::string>(cfgStr, map_SamplingPhase)<<std::setfill(' ')<<std::endl;
     if (m_FPGA_version >= FW_VERSION_1p6) printStr<<std::setw(27)<<" Sampling phase2: "<<std::setfill(' ')<<std::setw(32)<<GetConfigSetting<std::string>(cfgStr, map_SamplingPhase2)<<std::setfill(' ')<<std::endl;
     printStr << std::setw(27)<<" Input delay: "<<std::setfill(' ')<<std::setw(32)<<GetConfigSetting<std::string>(cfgStr, map_InputDelay)<<std::setfill(' ')<<std::endl
     <<std::setw(27)<<" Prescale: "<<std::setfill(' ')<<std::setw(32)<<GetConfigSetting<std::string>(cfgStr, map_Prescales)<<std::setfill(' ')<<std::endl
     <<std::setw(59)<<" (prescale mapping: physics trig0, physics trig1, physics trig2, physics trig3, internal trig, software/calibration trig) "<<std::endl
     <<std::setw(27)<<" Internal trigger rate: "<<std::setw(32)<<GetConfigSetting<unsigned int>(cfgStr, map_InternalTriggerRate)
     <<" --> Rate type: "<<(GetConfigSetting<bool>(cfgStr, map_Derandomize)?"fixed":"random")<<std::setfill(' ')<<std::endl
     <<std::setw(27)<<" RateLimiter Setting: "<<std::setw(32)<<GetConfigSetting<unsigned int>(cfgStr, map_RateLimiter)<<std::endl
     <<std::setw(27)<<" Monitoring rate: "<<std::setfill(' ')<<std::setw(32)<<GetConfigSetting<unsigned int>(cfgStr, map_MonitoringRate)<<std::setfill(' ')<<std::endl
     <<std::setw(27)<<" RJ45 port 0 delay: "<<std::setw(32)<<GetConfigSetting<unsigned int>(cfgStr, map_RJ45Port0Delay)<<std::setfill(' ')<<std::endl;
     if (m_FPGA_version >= FW_VERSION_1p9) printStr<<std::setw(27)<<" RJ45 port 1 delay: "<<std::setw(32)<<GetConfigSetting<unsigned int>(cfgStr, map_RJ45Port1Delay)<<std::setfill(' ')<<std::endl;
     printStr<<std::setw(27)<<" Digitizer delay: "<<std::setfill(' ')<<std::setw(32)<<GetConfigSetting<unsigned int>(cfgStr, map_DigitizerDelay)<<std::setfill(' ')<<std::endl
     <<std::setw(27)<<" Simple deadtime: "<<std::setfill(' ')<<std::setw(32)<<GetConfigSetting<unsigned int>(cfgStr, map_Deadtime)<<std::setfill(' ')<<std::endl
     <<std::setw(27)<<" Use LHC orbit signal: "<<std::setfill(' ')<<std::setw(32)<<(GetConfigSetting<bool>(cfgStr, map_LHC_CLK)?"TRUE":"FALSE")<<std::setfill(' ')<<std::endl
     <<std::setw(27)<<" Orbit signal delay: "<<std::setfill(' ')<<std::setw(32)<<GetConfigSetting<unsigned int>(cfgStr, map_OrbitDelay)<<std::setfill(' ')<<std::endl
     <<std::setw(27)<<" Output destination: "<<std::setfill(' ')<<std::setw(32)<<GetConfigSetting<unsigned int>(cfgStr, map_OutputDest)<<std::setfill(' ')<<std::endl;

     INFO(printStr.str());
}

/**
 *  @brief Returns decoded configuration setting.
 *
 *  @details Decodes the part of string given mapped to range in given ConfigMap and returns decoded value.
 *
 *  @note This calls ConfigReg::GetConfigSetting(std::string, const ConfigMap<P1,P2,P3>&, T&).
 *
 *  @return T template type
 */
template<class T, uint8_t P1, uint8_t P2, size_t P3=1>
T ConfigReg::GetConfigSetting( std::string cfgStr, const ConfigMap<P1,P2,P3>& configMap){
  T value;
  GetConfigSetting(cfgStr, configMap, value);
  return value;
}

/**
 *  @brief Fills value with decoded configuration setting.
 *
 *  @details Decodes the part of string given mapped to range in given ConfigMap and fills the value.
 *
 *  @return void
 */
template<class T, uint8_t P1, uint8_t P2, size_t P3=1>
void ConfigReg::GetConfigSetting( std::string cfgStr, const ConfigMap<P1,P2,P3>& configMap, T& value){
  auto subCfgStr = cfgStr.substr(configMap.start_bit_idx, configMap.size);
  std::reverse(subCfgStr.begin(), subCfgStr.end());
  value = (T)std::stoi(subCfgStr, nullptr,2);
}

/**
 *  @brief Fills value with decoded configuration setting.
 *
 *  @details Decodes the part of string given mapped to range in given ConfigMap and fills the value.
 *
 *  @note Overload of ConfigReg::GetConfigSetting(std::string, const ConfigMap<P1,P2,P3>&, T&).
 *
 *  @return void
 */
template<uint8_t P1, uint8_t P2, size_t P3=1>
void ConfigReg::GetConfigSetting( std::string cfgStr, const ConfigMap<P1,P2,P3>& configMap, std::string& value){ 
  uint8_t total_ch = (uint8_t)configMap.size/configMap.bit_size;
  for ( uint8_t i = 0; i < total_ch; i++ ){ 
    auto pos = configMap.start_bit_idx+i*configMap.bit_size;
    auto subCfgStr = cfgStr.substr(pos, configMap.bit_size);
    std::reverse(subCfgStr.begin(), subCfgStr.end());
    unsigned int ch_value = std::stoi(subCfgStr, nullptr,2);
    value+=std::to_string(ch_value)+" ";
  }
}

/**
 *  @brief Print the Direct Parameters configuration.
 *
 *  @details
 *
 *  Example: 
 *  @code std::cout<<"DirectParam:  "<<std::bitset<16>(DirectParam)<<std::endl;
    @endcode
 *  @return void
 */
void ConfigReg::PrintDirectParam(){
  INFO("Printing the DirectParameters");
  INFO("______________________________");
  INFO("DirectParam:  "<<std::bitset<16>(DirectParam));

  INFO("______________________________");  
}

/**
 *  @brief Print the Data Readout parameters configuration.
 *
 *  @details
 *
 *  Example: 
 *  @code std::cout<<"DataReadoutParam:  "<<std::bitset<16>(DataReadoutParam)<<std::endl;
    @endcode
 *  @return void
 */
void ConfigReg::PrintDataReadoutParam(){
  INFO("Printing the Data Readout Parameters");
  INFO("___________________________________");
  INFO("DataReadoutParam:  "<<std::bitset<16>(DataReadoutParam));

  INFO("___________________________________");  
}
