/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/

#include <unistd.h>
#include <iostream>
#include "../../TLBAccess/TLBAccess.h"

using namespace FASER;

//Program for testing of methods used for work with LUT
int main()
{
    TLBAccess *t = new TLBAccess();
    uint8_t DevNum = 0;
    std::vector<uint16_t> ReceivedLUT;
    std::vector<uint16_t> DecodedLUT;
    std::vector<uint16_t> testLUT(64);
    uint16_t CRC = 0;
    //Path has to be absolut
    std::string PathToLUT1 = "/home/otheiner/GPIO/gpiodrivers/TLBAccess/test/testLUT1.txt";
    std::string PathToLUT2 = "/home/otheiner/GPIO/gpiodrivers/TLBAccess/test/testLUT2.txt";

    for (int i = 0; i < testLUT.size(); i++)
    {
        testLUT[i] = i;
        //testLUT[i] = 0xffff;
    }
    
    t->SetLUT(DevNum, PathToLUT1);
    std::cout<<std::endl;

    if(!t->SetLUT(DevNum, PathToLUT2)){ 
        std::cout << "--------------Testing send LUT failed-------------------"<< std::endl;    
        return 0;
    }
    std::cout<<std::endl;

    ReceivedLUT = t->GetLUT(DevNum, &CRC);
    std::cout<<std::endl;
    
    DecodedLUT = t->DecodeLUT(ReceivedLUT);
    std::cout<<std::endl;

    std::cout << "Decoded LUT: " << std::endl;
    int i=0;
    for (auto& item : DecodedLUT){std::cout<<std::dec<<i<<": "<<item<< ", "; i++;}
    std::cout<<std::hex<<std::endl<<std::endl;
    
    std::cout << "CRC is " << CRC << std::endl<<std::endl;

    if (t->VerifyLUT_byCRC(DevNum)){ std::cout << "Verification OK"<< std::endl;}
    else {std::cout << "Verification failed."<< std::endl;}
    
    return 0;
}
