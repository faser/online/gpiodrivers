/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 
//============================================================================
//// Name        : runTLB
//// Author      : Claire Antel
//// Description : Executable for running the TLB in standalone given two arguments: json file to TLB configurations, text file to the LUT mapping.
////============================================================================

#include "../../TLBAccess/TLBAccess.h"
#include "EventFormats/TLBDataFragment.hpp"
#include "EventFormats/TLBMonitoringFragment.hpp"
#include <iostream>
#include <fstream>

#define _ms 1000 // used for usleep
#define _us 1000000

using namespace FASER;
using json = nlohmann::json;

static void usage() {
   std::cout<<"Usage: runTLB <path_to_configs_file.json>"<<std::endl;
   std::cout<<"Example of json configuration file: https://gitlab.cern.ch/faser/gpiodrivers/-/blob/master/TLBAccess/config/TLBConfiguration.JSON"<<std::endl;
   exit(1);
}


int main(int argc, char *argv[]) 
{
  if(argc<2) usage();
  // CONFIGURE
  unsigned int DAQseconds= 10;
  bool useETHER(true);
  bool RUN_IN_DEBUG(false); // run TLBAccess in RUN_IN_DEBUG mode and here
  bool PRINT(true); // write decoded TLB data to screen.

  unsigned total_orbits_lost_cnt(0);

  //JSON readout
  std::string configfilename(argv[1]);
  std::ifstream f(configfilename);
  if (!f) { std::cout<<"ERROR Invalid file: "<<configfilename<<std::endl; exit(1);}
  json myjson = json::parse(f);
  f.close();

  int boardID = myjson["BoardID"];
  std::string daqIP("");
  if (myjson.contains("DAQIP")) {
     daqIP = myjson["DAQIP"];
     INFO("Found DAQ IP "<<daqIP);
  }

  TLBAccess *tlb;
  if (useETHER) {
      std::string boardIP = myjson["BoardIP"];
      INFO("Communicating via the ether!");
      tlb = new TLBAccess(daqIP, daqIP, boardID, boardIP);
    //}
  }
  else {
    INFO("Communicating via USB!");
    tlb = new TLBAccess();
  }
  //usleep(1000*_ms);

  tlb->SetDebug(RUN_IN_DEBUG); //Set to 0 for no debug, to 1 for debug.
  

  std::cout<<"INFO: running DAQ for "<<std::dec<<DAQseconds<<" seconds."<<std::endl; 
  unsigned int DAQuseconds = DAQseconds*1000000;
  if (PRINT)
    std::cout<<"INFO: PRINT is ON, will print out decoded TLB data "<<std::endl;
  else
    std::cout<<"INFO: PRINT IS OFF, will NOT print out decoded TLB data "<<std::endl; 

  std::cout<<"INFO: Configuring TLB ... "<<std::endl;

  try { 
   TLBAccess::GPIOCheck(tlb, &TLBAccess::ConfigureAndVerifyTLB, myjson);
  }
  catch (TLBAccessException &e) {
    ERROR(e.what());
    exit(1);
  }

  json json_LUTConfig = myjson["LUTConfig"];
  if ( json_LUTConfig==nullptr ) {
    ERROR("Invalid LUT configuration. FIX ME!");
    exit(1);
  }
  try {
    tlb->ConfigureLUT(json_LUTConfig);
  }
  catch (TLBAccessException &e) {
    ERROR(e.what());
    exit(1);
  }

  std::ofstream m_dataOutStream;
  m_dataOutStream.open("RawData.out", std::ios::trunc);

  if (!m_dataOutStream.is_open()){
    std::cout << "ERROR: could not open file. no data will be written to disk!"<<std::endl;
  }

  std::ofstream debugfile;
  debugfile.open("debug_tlb.log");
  
  /*
  //Testing the SoftwareTrigger
  for (int i=0; i<10;i++){
    tlb->m_tlb_config.SetSoftwareTrigger(1);
    tlb->SendDirectParameters();
    tlb->m_tlb_config.PrintDirectParam();
    usleep(100*_ms);
    tlb->m_tlb_config.SetSoftwareTrigger(0);
    tlb->SendDirectParameters();
    tlb->m_tlb_config.PrintDirectParam();
    usleep(100*_ms);
  } 
  */


  //THESE ARE OVERWRITTEN BY COMMAND READOUT DATA
  //tlb->m_tlb_config.SetEnableTriggerData(myjson["EnableTriggerData"].get<bool>());
  //tlb->m_tlb_config.SetEnableMonitoringData(myjson["EnableMonitoringData"].get<bool>());
  //tlb->m_tlb_config.SetReadoutFIFOReset(myjson["ReadoutFIFOReset"].get<bool>());
  //tlb->SendDirectParameters();
  //tlb->m_tlb_config.PrintDataReadoutParam();
  
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////
  //////////// Read out Data ////////////////////////////////////
  uint16_t ReadoutParams=0x0;
  ReadoutParams=(ReadoutParams|(myjson["EnableTriggerData"].get<bool>()<<13));
  ReadoutParams=(ReadoutParams|(myjson["EnableMonitoringData"].get<bool>()<<14));
  ReadoutParams=(ReadoutParams|(myjson["ReadoutFIFOReset"].get<bool>()<<15));
  int gpio_status = TLBAccess::GPIOCheck(tlb, &TLBAccess::StartReadout,ReadoutParams);
  if (gpio_status) {
    ERROR("Something went wrong, can't start readout");
    exit(1);
  }
  sleep(5);

  std::vector<std::vector<uint32_t>> vector_of_raw_events;
  bool status;
  uint64_t L1ID;
  uint16_t BCID;
  uint16_t ORBITID;
  int delay(0);
  int new_pos(0);
  int old_pos(0);
  int last_L1ID(0);

  auto startDAQ = std::chrono::high_resolution_clock::now();
  auto stopDAQ = startDAQ;
  auto durationDAQ = std::chrono::duration_cast<std::chrono::microseconds>(stopDAQ - startDAQ).count();

  gpio_status = TLBAccess::GPIOCheck(tlb, &TLBAccess::EnableTrigger, true,true);
  if (gpio_status == 2) { 
    ERROR("Something went wrong, can't enable trigger. Stopping TLB process and exiting.");
    tlb->StopReadout();
    exit(1);
  }
  else if (gpio_status == 1) { 
    ERROR("Something didn't look right while enabling trigger. A glitch in communication with TLB? Continuing anyways....");
  }

  bool running(true);
  bool daq_stopped(false);
  unsigned total_events(0);
  unsigned total_good_events(0);
  
  while( running ){
    vector_of_raw_events = tlb->GetTLBEventData();
    if (RUN_IN_DEBUG) std::cout<<"number of events retrieved = "<<std::dec<<vector_of_raw_events.size()<<std::endl;
    
    if (vector_of_raw_events.size()){
          for(int i=0; i<vector_of_raw_events.size(); i++){

            status = false; 
            if (PRINT) std::cout<<"Event size (32bit words) "<<std::dec<<vector_of_raw_events[i].size()<<std::endl;

            for (auto word : vector_of_raw_events[i] ) {
              m_dataOutStream <<std::bitset<32>(word)<<"\n";
            }

            if (PRINT) { 
              if (!vector_of_raw_events[i].size()) { std::cerr<<"empty event!!!"<<std::endl; continue;}
              size_t size_in_bytes = vector_of_raw_events[i].size() * sizeof(uint32_t);
              if ( TLBDecode::IsTriggerHeader(vector_of_raw_events[i][0])) {
                  total_events+=1;
                  TLBDataFormat::TLBDataFragment tlb_fragment = TLBDataFormat::TLBDataFragment(vector_of_raw_events[i].data(), size_in_bytes );
                  if (!tlb_fragment.valid() ) status = true;
                  if (!status) total_good_events+=1;
                  else status = false;
                  std::cout<<"--- Trigger event --- "<<std::endl;
                  //tlb_fragment.set_debug_on();
                  std::cout<<std::dec<<tlb_fragment<<std::endl; 
                  L1ID = tlb_fragment.event_id();
                  if (L1ID != last_L1ID+1) WARNING("Skipped L1 ID!!!"); 
                  last_L1ID = L1ID;
		  debugfile<<L1ID<<"\t";
                  ORBITID = tlb_fragment.orbit_id();
		  //debugfile<<ORBITID<<"\t";
                  BCID = tlb_fragment.bc_id();
                  if ( BCID > 3564) WARNING("BCID out of range! BCID = "<<BCID);
		  debugfile<<BCID<<"\t";
                  if ( tlb_fragment.valid() ) {
                    new_pos = (3564*ORBITID+BCID);
                    delay =  (3564*ORBITID+BCID) - old_pos;
                    old_pos = new_pos;
                    debugfile<<delay;
                  }
                  debugfile<<std::endl;
              } else {
                  TLBMonFormat::TLBMonitoringFragment tlb_fragment = TLBMonFormat::TLBMonitoringFragment(vector_of_raw_events[i].data(), size_in_bytes );
                  if (!tlb_fragment.valid() ) status = true;
                  else status = false;
                  std::cout<<"--- Monitoring event --- "<<std::endl;
                  std::cout<<std::dec<<tlb_fragment<<std::endl; 
                  L1ID = tlb_fragment.event_id();
                  BCID = tlb_fragment.bc_id();
                  total_orbits_lost_cnt+=tlb_fragment.orbits_lost_counter();
              } 
              if (status) {
               std::cout<<"WARNING CORRUPTED EVENT!"<<std::endl;
              }
            }
         } // loop over events
    } // if events in vector
    else usleep(100*_ms);

    stopDAQ = std::chrono::high_resolution_clock::now();
    durationDAQ = std::chrono::duration_cast<std::chrono::microseconds>(stopDAQ - startDAQ).count();

    if ( durationDAQ > DAQuseconds && !daq_stopped ) {
      std::cout<<"   End of DAQ timer, stopping trigger and readout."<<std::endl;
      tlb->DisableTrigger();
      usleep(100);
      tlb->StopReadout();
      daq_stopped = true;
    }
    if (daq_stopped && (durationDAQ > DAQuseconds+ 5*_us) ) running = false;

   } //end of while

   INFO("TOTAL EVENTS: "<<total_events);
   INFO("TOTAL GOOD EVENTS: "<<total_good_events);
   INFO("TOTAL ORBITS LOST: "<<total_orbits_lost_cnt);

   m_dataOutStream.close();

   debugfile.close();

   return 0;
}

