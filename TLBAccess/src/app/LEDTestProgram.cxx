/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 
#include "../../TLBAccess/TLBAccess.h"
#include<iostream>
#include <bitset>
#include <unistd.h>

using namespace FASER;


int main() 
{
  //GPIOBaseClass * MyGPIOAccess = new GPIOBaseClass(false);
  TLBAccess * MyGPIOAccess = new TLBAccess(false);

  uint16_t ans_arg(0);

  static const uint16_t SET_PRESCALE1  = 0x0001; // can be any number: 0 to 0x001F
  static const uint16_t ENABLE_TRIGGER  = 0x0004;
  static const uint16_t DISABLE_TRIGGER  = 0x0000;

  // -------------------
  // testing TLB LED D1: Enable trigger
  std::cout<<"Testing TLB LED D1: Enabling trigger ..."<<std::endl;
  MyGPIOAccess->SendAndRetrieve(TLBCmdID::SET_DIRECT_PARAM, ENABLE_TRIGGER);
  std::cout<<"Testing TLB LED D1: Disabling trigger ..."<<std::endl;
  usleep(2000000);
  MyGPIOAccess->SendAndRetrieve(TLBCmdID::SET_DIRECT_PARAM, DISABLE_TRIGGER);
  // -------------------

  // -------------------
  // testing TLB LED D3: Prescales other than 0.
  std::cout<<"Testing TLB LED D3: Setting a prescale to non-zero ..."<<std::endl;
  usleep(2000000);
  // now creating 15 words of 16 bits.
  unsigned int number_words = 15;
  std::vector<uint16_t> payload_vector;
  uint16_t word = 0, tmp = 0;
  for ( unsigned int i = 0; i < number_words; i++ ){
    std::cout<<"filling word # "<< i << std::endl; 
    tmp = 0;
    word = i; // word id starts at 0
    word <<= 12;
    if ( i == 3 ) { //artifically fill - not so nice, i know.
      tmp = SET_PRESCALE1;
    }
    word |= tmp;
    payload_vector.push_back(word);
  }
  for ( auto payload : payload_vector ) {
    //std::bitset<16> payload_bitset(payload);
    //std::cout << payload_bitset << std::endl;
    MyGPIOAccess->SendAndRetrieve(TLBCmdID::USER_SET_CONFIG, payload);
  }
  std::cout<<"Testing TLB LED D3: Setting all prescales back to zero ..."<<std::endl;
  usleep(2000000);
  payload_vector.at(3) = (uint16_t)0;
  for ( auto payload : payload_vector ) {
    //std::bitset<16> payload_bitset(payload);
    //std::cout << payload_bitset << std::endl;
    MyGPIOAccess->SendAndRetrieve(TLBCmdID::USER_SET_CONFIG, payload);
  }
  // -------------------

  return 0;
}

