# 
# Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
# 

#!/usr/bin/env python3

#signals assumed:
#Tin0: calorimeter bottom
#Tin1 calorimeter top
#Tin2: first veto layer
#Tin3: second veto layer
#Tin4: timing layer top
#Tin5: timing layer bottom
#Tin6: preshower layer
#Tin7: Unused

#trigger items:

#any scintillator trigger:
TBP0="Tin2"
TBP1="Tin3"
TBP2="Tin4|Tin5"
TBP3="Tin6"

for inp in range(256):
    for ibit in range(8):
        val=0
        if inp&(1<<ibit): val=1
        globals()["Tin"+str(ibit)]=val
        globals()["notTin"+str(ibit)]=1-val
    TBP=""
    for obit in range(4):
        TBP=str(eval(eval("TBP"+str(obit))))+TBP
    print(TBP)
        
