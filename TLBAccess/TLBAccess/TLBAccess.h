/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 
#ifndef __TLBAccess
#define __TLBAccess

#include "GPIOBase/GPIOBaseClass.h"
#include "GPIOBase/CommunicationInterface.h"
#include "TLB_ConfigRegisters.h"
#include <fstream>
#include <thread>
#include <mutex>
#include "GPIOBase/CRC.h"
#include <bitset>
#include <unistd.h>
#include "nlohmann/json.hpp"
#include "Exceptions/Exceptions.hpp"
#include <iostream>
#include <atomic>

using json = nlohmann::json;

class TLBAccessException : public Exceptions::BaseException { using Exceptions::BaseException::BaseException; };

namespace FASER {

  namespace TLBConstants {
    constexpr int FW_VERSION_1p6 = 0x116;
    constexpr int FW_VERSION_1p9 = 0x119;
  }
 
  using namespace TLB;  

  /** *******************************************************
   \brief API to communicate with the Trigger Logic Board (TLB).
   ******************************************************** */


  class TLBAccess : public GPIOBaseClass
  {
 
  public:
    // CONSTRUCTOR / DESTRUCTOR
    TLBAccess(bool emulateInterface = false) : GPIOBaseClass(emulateInterface){}
    TLBAccess(std::string SCIP, std::string DAQIP, int boardID, std::string boardIP, bool emulateInterface = false) : GPIOBaseClass(SCIP, DAQIP, boardID, boardIP, emulateInterface), m_daqThread(nullptr) {}
    ~TLBAccess();
    
    void SendDirectParameters(const uint16_t&);
    void DisableTrigger();
    void EnableTrigger(bool ECR, bool Reset);
    void SendECR();
    void SendDataReadoutParameters();
    void ConfigureTLB(const json& myjson);
    void ConfigureAndVerifyTLB(const json& myjson);
    bool VerifyConfiguration();
    void ConfigureLUT(const json& trigjson);
    bool SetLUT(uint8_t x_device, const json trigjson);
    bool VerifyLUT_byCRC(uint8_t x_device);
    std::vector<uint16_t> GetLUT(uint16_t x_device, uint16_t *CRC = nullptr);
    std::vector<uint16_t> DecodeLUT(std::vector<uint16_t> x_LUT);
    void StartReadout(const uint16_t& param);
    void StopReadout();
    std::vector< std::vector<uint32_t> > GetTLBEventData();
    float GetDataRate() {return m_dataRate;}
  
    //bool ApplyConfig(uint8_t x_device);
  
  private:
    std::thread *m_daqThread;
    bool m_daqRunning;
    void PollData();
    
    ConfigReg m_tlb_config = ConfigReg(m_FPGA_version);
    std::mutex mMutex_TLBEventData; //mutex object vector of vectors
    std::vector <uint32_t> m_DataWordOutputBuffer; //vector of words
    std::vector<std::vector<uint32_t>> m_TLBEventData; // complete TRB raw data for every event
    std::atomic<float> m_dataRate;
    std::unique_ptr<const std::map<std::string, std::string>> m_trig_name_map;
    
    void SendConfig();
    bool LoadLUT(std::string x_PathToLUT, std::vector<uint16_t> *x_LUT); 
    bool VerifyLUT_byBits(uint8_t x_device, std::vector<uint16_t> x_LUT);
    bool IsBinary(std::string l_value);
    bool GenerateLUT(std::vector<uint16_t>& lut, std::vector<std::string> configs);
    void TranslateConfigs(std::vector<std::string>& configs);
    void VerifyConfigs(std::vector<std::string> configs);
    void replaceAll(std::string& str, std::string from, std::string to);
    std::string ConvertIntToWord(unsigned int word, int nbits);
  };

/** *******************************************************
 * \brief Functions for decoding TLB data.
 ******************************************************** */
  class TLBDecode {
    private:
    
      static const uint32_t TRIGGER_HEADER_V1    = 0xFEAD000A;
      static const uint32_t TRIGGER_HEADER_V2    = 0xFEAD00A0;
      static const uint32_t TRIGGER_HEADER_V3    = 0xFEAD00AA;
      static const uint32_t MONITORING_HEADER_V1 = 0xFEAD0050;
      static const uint32_t MONITORING_HEADER_V2 = 0xFEAD0005;
      static const uint32_t MONITORING_HEADER_V3 = 0xFEAD0055;
      static const uint32_t TRIGGER_TRAILER      = 0xE0000000;
      static const uint32_t MONITORING_TRAILER   = 0xD0000000;
      static const uint32_t END_OF_DAQ = 0xCAFEDECA;
    
    public:
      
      static bool IsTriggerHeader(const uint32_t& data){ 
        return (data == TRIGGER_HEADER_V1 || data == TRIGGER_HEADER_V2 || data == TRIGGER_HEADER_V3);
      };
      static bool IsMonitoringHeader(const uint32_t& data){
        return (data == MONITORING_HEADER_V1 || data == MONITORING_HEADER_V2 || data == MONITORING_HEADER_V3);
      };
      static bool IsTriggerTrailer(const uint32_t& data){
        return ((data&0xFF000000) == TRIGGER_TRAILER);
      };
      static bool IsMonitoringTrailer(const uint32_t& data){
        return ((data&0xFF000000) == MONITORING_TRAILER);
      };
      static bool IsEndOfDAQ(const uint32_t& data){
        return (data == END_OF_DAQ);
      };
  };

}

/** *******************************************************
 * \brief Definitions of CommandRequest IDs.
 ******************************************************** */
class TLBCmdID:public GPIOCmdID {
public:
  static const uint8_t USER_SET_CONFIG     = 0x08;
  static const uint8_t USER_GET     = 0x10;
};

/** *******************************************************
 * \brief Definitions of readout parameters.
 ******************************************************** */
struct TLBReadoutParameters {
  public:
    static const uint16_t EnableTriggerData = 0x2000;
    static const uint16_t EnableMonitoringData = 0x4000;
    static const uint16_t ReadoutFIFOReset = 0x8000;
};

/** *******************************************************
 \brief Definitions of ERROR codes retrieved from TLB.
 ******************************************************** */

class TLBErrors:public GPIOErrors {
};

/** *******************************************************
 \brief Definitions of WORD ID (bit 31-28 of command request header)
 ******************************************************** */
class TLBWordID:public GPIOWordID{

};

#endif /* __TLBAccess */
