/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 
#include "GPIOBase/udpInterface.h"
#include <string>
#include <iomanip>
#include <cstring> //memset
#include <stdexcept>
#include <sstream>      // std::stringstream

using namespace FASER;

/** \brief Constructor
 */
udpInterface::udpInterface(std::string SCIP, std::string DAQIP, std::string BoardIP)
{
  SetSCIP(SCIP, serialPort);
  SetDAQIP(DAQIP, daqPort);
  SetBoardIP(BoardIP, boardPort);
}

/** \brief Destructor: cleanup
 */
udpInterface::~udpInterface()
{
  if (m_interfaceOK) {
    close(m_socket);
    close(m_recvSerial_socket);
    close(m_recvData_socket);
  }
}

/** *******************************************************
 * \brief Open the socket used for UDP communication
 ******************************************************* */
void udpInterface::Connect()
{
  if (m_boardAddr.sin_family == 0){
    throw std::runtime_error("Board address not set. Call SetBoardIP() first.");
  }
  // Creating socket file descriptor
  if ( (m_socket = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
    throw std::runtime_error("Socket creation failed");
  }
 
  if ( (m_recvSerial_socket = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
    throw std::runtime_error("Socket creation failed");
  }

  if ( (m_recvData_socket = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
    throw std::runtime_error("Socket creation failed");
  }

  int enable = 1;
  if (setsockopt(m_recvSerial_socket, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0){
     throw std::runtime_error("setsockopt(SO_REUSEADDR) failed for serial data receiver");
  }
 
  if (setsockopt(m_recvData_socket, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0){
     throw std::runtime_error("setsockopt(SO_REUSEADDR) failed for DAQ data receiver");
  }

  struct timeval timeout;      
  timeout.tv_sec = 0;
  timeout.tv_usec = 500000; //500 ms - time out for DAQ and Serial socket (function rcvfrom in ReadData/Serial)

  if (setsockopt(m_recvSerial_socket, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout)) < 0){
     throw std::runtime_error("setsockopt(SO_RCVTIMEO) failed for serial data receiver");
  }
  if (setsockopt(m_recvData_socket, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout)) < 0){
     throw std::runtime_error("setsockopt(SO_RCVTIMEO) failed for DAQ data receiver");
  }

  if ( bind(m_recvSerial_socket, (const struct sockaddr *)&m_recvSerialAddr,
            sizeof(m_recvSerialAddr)) < 0 )
  {
    throw std::runtime_error("bind failed for serial data receiver");
  }

  if ( connect(m_recvSerial_socket, (const struct sockaddr *)&m_boardAddr,
            sizeof(m_boardAddr)) < 0 )
  {
    throw std::runtime_error("connect failed for serial data receiver");
  }

  if ( bind(m_recvData_socket, (const struct sockaddr *)&m_recvDataAddr,
            sizeof(m_recvDataAddr)) < 0 )
  {
    throw std::runtime_error("bind failed for DAQ data receiver");
  }

  if ( connect(m_recvData_socket, (const struct sockaddr *)&m_boardAddr,
            sizeof(m_boardAddr)) < 0 )
  {
    throw std::runtime_error("connect failed for DAQ data receiver");
  }

  m_interfaceOK = true;
  SendMagicWord(); // can throw exception
}

/** \brief sends data via socket opened in connect()
 */
void udpInterface::SendSerial(unsigned char *data, int length, int &bytesTransferred)
{
  bytesTransferred = sendto(m_socket, (const char *)data, length, 0, (const struct sockaddr *) &m_boardAddr, sizeof(m_boardAddr));
  if (bytesTransferred == -1){
     bytesTransferred = 0;
     switch (errno) {
       case ECONNRESET:
         ERROR("In SendSerial(): Connection closed "<<std::strerror(errno));
         break;
       case EWOULDBLOCK:
         ERROR("In SendSerial(): Sending blocked "<<std::strerror(errno));
         break;
       default:
         ERROR("In SendSerial(): unhandeled error occured: "<<std::strerror(errno));
     }
   }
}

/** *******************************************************
 * \brief Transfer magic word, DAQ host IP and SC host IP to the GPIO board.
 ******************************************************* */
void udpInterface::SendMagicWord()
{
  char ping_addr[INET_ADDRSTRLEN];
  inet_ntop(AF_INET, &(m_boardAddr.sin_addr), ping_addr, INET_ADDRSTRLEN);
  std::stringstream ss;
  ss <<"ping -c2 -s1 -w2 "<<ping_addr<<" > /dev/null 2>&1";
  INFO("PINGing board at address "<<ping_addr);
  int x = system((ss.str()).c_str());
  if (x==0){
      INFO("PING success");
  }else{
      WARNING("PING fail");
  }

  ClearSendBuffer();

  QueueData(IPMagicWord);
  QueueData(m_recvSerialAddr.sin_addr.s_addr); //GPIO IP
  QueueData(m_recvDataAddr.sin_addr.s_addr); //DAQ IP
  
  m_boardAddrConfig.sin_family = m_boardAddr.sin_family;
  m_boardAddrConfig.sin_port = htons(configPort);
  m_boardAddrConfig.sin_addr.s_addr =  m_boardAddr.sin_addr.s_addr; // this does not support IPv6

  int bytesTransferred = sendto(m_socket, (const char *)m_dataOutBuffer.data(), m_dataOutBuffer.size(), 0, (const struct sockaddr *) &m_boardAddrConfig, sizeof(m_boardAddrConfig));
  if (bytesTransferred != kMAGICWORD_SIZE ){
     if (bytesTransferred < 0) {
       switch (errno) {
         case ECONNRESET:
           ERROR("In SendMagicWord(): Connection closed "<<std::strerror(errno));
           break;
         case EWOULDBLOCK:
           ERROR("In SendMagicWord(): Sending blocked "<<std::strerror(errno));
           break;
         default:
           ERROR("In SendMagicWord(): unhandeled error occured: "<<std::strerror(errno));
       }
     }
     else ERROR("In SendMagicWord(): Wrong number of bytes ("<<bytesTransferred<<" bytes) transferred.");
     THROW(GPIOCommunicationException, "Sending magic word failed.");
  }
   
}

/** *******************************************************
 * \brief Read bytes from GPIO board
 ******************************************************* */
std::vector<unsigned char> udpInterface::ReadSerial(int &bytesTransferred)
{
  std::vector<unsigned char> returnBuffer;
  returnBuffer.resize(1024);
  
  unsigned int len = 0;
  bytesTransferred = recvfrom(m_recvSerial_socket, (char *)&(returnBuffer[0]), 1024, MSG_WAITALL, (struct sockaddr *) &m_anyAddr, &len);
  if (bytesTransferred == -1){
     bytesTransferred = 0;
     switch (errno) {
       case EFAULT:
         ERROR("In ReadSerial(): Invalid transfer "<<std::strerror(errno));
         returnBuffer.clear();
         break;
       case EWOULDBLOCK:
         ERROR("In ReadSerial(): No data "<<std::strerror(errno));
         returnBuffer.clear();
         break;
         
       default:
         ERROR("In ReadSerial(): unhandeled error occured: "<<std::strerror(errno));
         returnBuffer.clear();
     }
   }
  returnBuffer.resize(bytesTransferred);
   
  return returnBuffer;
}

/** *******************************************************
 * \brief Read bytes from GPIO board - same as ReadSerial for UPD
 ******************************************************* */
std::vector<unsigned char> udpInterface::ReadData(int &bytesTransferred){
  std::vector<unsigned char> returnBuffer;
  returnBuffer.resize(BUFF_SIZE);
  unsigned int len(0);
  len = sizeof(m_anyAddr);
  
  bytesTransferred = recvfrom(m_recvData_socket, (unsigned char *)&(returnBuffer[0]), returnBuffer.size(), MSG_WAITALL, (struct sockaddr *) &m_anyAddr, &len);

  if (bytesTransferred == -1){
    bytesTransferred = 0;
    switch (errno) {
      case EFAULT:
        ERROR("In ReadData: Invalid transfer"<<std::strerror(errno));
        returnBuffer.clear();
        break;
      case EWOULDBLOCK:
        // this error happens everytime the readout is stopped -> suppressing error output
        // ERROR("In ReadData: No data "<<std::strerror(errno));
        returnBuffer.clear();
        break;
        
      default:
        ERROR("In ReadData(): unhandeled error occured: "<<std::strerror(errno));
        returnBuffer.clear();
    }
  }
  returnBuffer.resize(bytesTransferred);
  return returnBuffer;
}
/** *******************************************************
 * \brief return bytes read in a vector of 32bit words
 ******************************************************** */
std::vector<uint32_t> udpInterface::ReadSerial32(int &bytesTransferred)
{
  std::vector<unsigned char> data = ReadSerial(bytesTransferred);
  std::vector<uint32_t> data32;
  uint32_t word = 0;
  uint32_t tmp = 0;
  unsigned int bytesStored = 0;
  if (m_DEBUG) {
    TRACE(" ReadSerial: Bytes read = "<<bytesTransferred<<" content = ");
  }
  for (int i = 0; i < bytesTransferred; i++){
    
    tmp = data[i];
    word |= (tmp << ((bytesStored*8)) );
    bytesStored++;
    
    if (bytesStored == 4){
      if (m_DEBUG) { TRACE(" 0x"<<std::hex<<std::setw(8)<<word); }
      data32.push_back(word);
      word = 0;
      bytesStored = 0;
    }
  }
  
  if (bytesStored != 0){
    data32.push_back(word);
  }
  return data32;
}

/** *******************************************************
 * \brief return bytes read in a vector of 32bit words
 ******************************************************** */
std::vector<uint32_t> udpInterface::ReadData32(int &bytesTransferred){
  std::vector<unsigned char> data = ReadData(bytesTransferred);
  std::vector<uint32_t> data32;
  
  uint32_t word = 0, tmp = 0;
  unsigned int bytesStored = 0;
  
  for (unsigned int i = 0; i < data.size(); i++){
    tmp = data[i];
    word |= tmp << ((bytesStored*8));
    bytesStored++;
    
    if (bytesStored == 4){
      data32.push_back(word);
      word = 0;
      bytesStored = 0;
    }
  }
  if (bytesStored != 0){
    data32.push_back(word);
  }
  return data32;
}


/** *******************************************************
 * \brief Transfer data in buffer
 ******************************************************** */
int udpInterface::SendBuffer(){
  int bytesTransferred = 0;
  if (m_dataOutBuffer.size() == 0){
    WARNING("No data in buffer ");
    return 0;
  }
  
  SendSerial(m_dataOutBuffer.data(), m_dataOutBuffer.size(), bytesTransferred);
  return bytesTransferred;
}

/** *******************************************************
 * \brief Fill output buffer in 8 bit chunks
 ******************************************************** */
void udpInterface::QueueData(uint32_t data){
  
  for (unsigned int i = 0; i < sizeof(data)/sizeof(unsigned char); i++) {
    m_dataOutBuffer.push_back( (unsigned char)(data & 0x000000ff));
    data = data >> 8;
  }
}

/** *******************************************************
 *  *  \brief Set S/C IP address 
 *   *   ******************************************************** */
void udpInterface::SetSCIP(std::string host, short port)
{
  std::string ipaddr;
  if (!IsIPAddress(host.c_str())) {
    ipaddr = GetHostByName(host.c_str());
  } else ipaddr = host;
  memset(&m_recvSerialAddr, 0, sizeof(m_recvSerialAddr));
  m_recvSerialAddr.sin_family = AF_INET;
  m_recvSerialAddr.sin_port = htons(port);
  m_recvSerialAddr.sin_addr.s_addr = inet_addr(ipaddr.c_str());
}


/** *******************************************************
 *  *  \brief Set DAQ IP address 
 *   *   ******************************************************** */
void udpInterface::SetDAQIP(std::string host, short port)
{
  std::string ipaddr;
  if (!IsIPAddress(host.c_str())) {
    ipaddr = GetHostByName(host.c_str());
  } else ipaddr = host;
  memset(&m_recvDataAddr, 0, sizeof(m_recvDataAddr));
  m_recvDataAddr.sin_family = AF_INET;
  m_recvDataAddr.sin_port = htons(port);
  m_recvDataAddr.sin_addr.s_addr = inet_addr(ipaddr.c_str());;
}

/** *******************************************************
 *  *  \brief Set DAQ IP address
 *   *   ******************************************************** */
void udpInterface::SetBoardIP(std::string host, short port)
{
  std::string ipaddr;
  if (!IsIPAddress(host.c_str())) {
    ipaddr = GetHostByName(host.c_str());
  } else ipaddr = host;
  memset(&m_boardAddr, 0, sizeof(m_boardAddr));
  m_boardAddr.sin_family = AF_INET;
  m_boardAddr.sin_port = htons(port);
  m_boardAddr.sin_addr.s_addr = inet_addr(ipaddr.c_str()); // this does not support IPv6
}

/** *******************************************************
 **  \brief Check if string has format of an IP address
 **  \param input: string to be tested
 ********************************************************** */
bool udpInterface::IsIPAddress(const char *input){
  // current character to check next character
  // starts at -1 and then '.'=0 and '<#>'=1
  int prev=-1;
  // counting the number of dots encountered
  int ndot=0;
  // the count for an sequence of three numbers
  int nnum=0;
  // count the number of dots and register that only numbers exist in between
  // there should be three dots with numbers before and after
  int length = (unsigned)strlen(input);

  for(int i=0; i<length; i++){
    if(prev==-1){
      // if the first character check that its a number
      if(isdigit(input[i])){
        prev=1;
      }
      else{
        DEBUG("IP address must begin with number ...");
        return false;
      }
    }
    else if(prev==0){
      // if the previous character was a dot, then anticipate a number
      if(isdigit(input[i])){
        prev=1;
        nnum++;
      }
      else if(input[i] == '.'){
        DEBUG("IP address can't have two dots in a row");
        return false;
      }
      else{
        DEBUG("IP address is not an allowed IP address character");
        return false;
      }
    }
    else if(prev==1){
      // if the previous character was a number, then anticipate a number
      if(isdigit(input[i])){
        prev=1;
        if(nnum<3){ // can't have more than three numbers in a row between dots
          nnum++;
        }
        else{
          DEBUG("IP address has too many numbers in a row");
          return false;
        }
      }
      else if(input[i] == '.'){
        prev=0;
        nnum=0;
        if(ndot<3){
          ndot++;
        }
        else{
          DEBUG("IP address has too many dots");
          return false;
        }
      }
      else{
        DEBUG("IP address is not an allowed this character");
        return false;
      }
    }
  }
  if(ndot<3){
    DEBUG("Not enough dots to be an IP address");
    return false;
  }

  INFO("Identified "<<input<<" as an IP address.");
  return true;
}

/** *******************************************************
 **  \brief Get IP given device name.
 **  \param input: device name
 ********************************************************** */
std::string udpInterface::GetHostByName(const char* input){
  char ip_addr_string[32];
  struct hostent *hp = gethostbyname(input);
  if (hp == NULL) {
    THROW(GPIOCommunicationException, "get host by name failed. Check host name given.");
  }
  INFO("Getting IP for host name "<<hp->h_name);
  unsigned int i=0;
  while ( hp -> h_addr_list[i] != NULL) {
    strcpy(ip_addr_string, inet_ntoa( *( struct in_addr*)( hp -> h_addr_list[i])));
    DEBUG( "Found IP address : "<< inet_ntoa( *( struct in_addr*)( hp -> h_addr_list[i])));
    i++;
  }
  INFO("Discovered IP address "<<ip_addr_string);
  return std::string(ip_addr_string);
}
