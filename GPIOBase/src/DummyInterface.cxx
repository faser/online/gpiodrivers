/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 
#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <unistd.h>
#include "GPIOBase/DummyInterface.h"
#include "../TRBAccess/TrackerReadout/ROR_Header.h"

using namespace FASER;

/** *******************************************************
 * \brief Constructor: initialises usbLib
 ******************************************************** */
dummyInterface::dummyInterface() : m_DEBUG(true), m_EMULATE(false)
{
  if (m_DEBUG){
  }
  
  // setup tranfer buffers
  m_serialBuffer.resize(1024); // 512 bytes recommended for HS devices
  m_dataBuffer.resize(4);
}

/** *******************************************************
 * \brief Destructor: cleanup resources and exit usblib
 ******************************************************** */
dummyInterface::~dummyInterface()
{
  if (m_inputFileStream.is_open()) {
    m_inputFileStream.close();
  }
}

/** *******************************************************
 * \brief Open new input file
 ******************************************************** */
bool dummyInterface::SetInputFile(std::string filename)
{
  if (m_inputFileStream.is_open()) {
    m_inputFileStream.close();
  }
  m_inputFileStream.open(filename, std::ios::binary);
  if (!m_inputFileStream.is_open()){
    ERROR("Could not open file "<<filename<<". No data will be written to disk!");
    return false;
  }
  return true;
}

/** *******************************************************
 * \brief Send data to GPIO board (config & control)
 ******************************************************** */
void dummyInterface::SendSerial(unsigned char */*data*/, int length, int &bytesTransferred){
  bytesTransferred = length;
}

/** *******************************************************
 * \brief Receive serial data from the GPIO board (config & control)
 ******************************************************** */
std::vector<unsigned char> dummyInterface::ReadSerial(int &bytesTransferred){
  bytesTransferred = m_dataOutBuffer.size();
  return m_dataOutBuffer;
}

/** *******************************************************
 * \brief return bytes read in a vector of 32bit words
 ******************************************************** */
std::vector<uint32_t> dummyInterface::ReadSerial32(int &bytesTransferred){
  std::vector<unsigned char> data = ReadSerial(bytesTransferred);
  std::vector<uint32_t> data32;
  
  uint32_t word = 0;
  uint32_t tmp = 0;
  unsigned int bytesStored = 0;
  if (m_DEBUG) {
    TRACE("ReadSerial: Bytes read = "<<bytesTransferred<<" content = ");
  }
  for (int i = 0; i < bytesTransferred; i++){
    
    tmp = data[i];
    word |= (tmp << ((bytesStored*8)) );
    bytesStored++;
    
    if (bytesStored == 4){
      if (m_DEBUG) { TRACE(" 0x"<<std::hex<<std::setw(8)<<word); }
      data32.push_back(word);
      word = 0;
      bytesStored = 0;
    }
  }
  
  if (bytesStored != 0){
    data32.push_back(word);
  }
  return data32;
}

/** *******************************************************
 * \brief Read one event from input stream and resturn TRB data only.
 ******************************************************** */
std::vector<unsigned char> dummyInterface::ReadData(int &bytesTransferred){
 
  // clear read buffer
  m_dataBuffer.clear();
  m_dataBuffer.resize(0);
  bytesTransferred = 0;
  // read up to 8 32bit words
   std::vector <unsigned char> outBuf;
  
  usleep(10);
  if (m_inputFileStream.is_open() && !m_inputFileStream.eof()) {
    // read ROR header
    RORHeader header;
    header.Read(m_inputFileStream);
    //header.Print();
   
    // read TRB data into dataBuffer
    if ((header.GetDataStatus() == RORHeader::DATA_STATUS_OK ) || (header.GetDataStatus() == RORHeader::DATA_STATUS_EOD)) {
      if ((int)header.GetDataSize() < 0) {
        ERROR("ReadData: Data size corrupted! Resetting stream");
        m_inputFileStream.seekg (0, m_inputFileStream.beg);
        return outBuf;
      }
      m_dataBuffer.resize(header.GetDataSize());
      m_inputFileStream.read(&m_dataBuffer[0], header.GetDataSize());
      
      bytesTransferred = header.GetDataSize();
    }
  }
  
  // hack to adapt type from char -> unsigned char
 
  for (auto i : m_dataBuffer) { outBuf.push_back(i);}
  
  DEBUG("Read "<<std::dec<<outBuf.size()<< " bytes from file");
  return outBuf;
}

/** *******************************************************
 * \brief return bytes read in a vector of 32bit words
 ******************************************************** */
std::vector<uint32_t> dummyInterface::ReadData32(int &bytesTransferred){
  std::vector<unsigned char> data = ReadData(bytesTransferred);
  std::vector<uint32_t> data32;
  
  uint32_t word = 0, tmp = 0;
  unsigned int bytesStored = 0;
  
  for (unsigned int i = 0; i < data.size(); i++){
    tmp = data[i];
    word |= tmp << ((bytesStored*8));
    bytesStored++;
    
    if (bytesStored == 4){
      data32.push_back(word);
      word = 0;
      bytesStored = 0;
    }
  }
  if (bytesStored != 0){
    data32.push_back(word);
  }
  DEBUG("Converted "<<data.size() << " bytes into "<< data32.size()<<" words.");
  return data32;
}


/** *******************************************************
 * \brief Transfer data in buffer
 ******************************************************** */
int dummyInterface::SendBuffer(){
  int bytesTransferred = 0;
  if (m_dataOutBuffer.size() == 0){
    WARNING("No data in buffer.");
    return 0;
  }
  
  SendSerial(m_dataOutBuffer.data(), m_dataOutBuffer.size(), bytesTransferred);
  return bytesTransferred;
}

/** *******************************************************
 * \brief Fill output buffer in 8 bit chunks
 ******************************************************** */
void dummyInterface::QueueData(uint32_t data){
  
  for (unsigned int i = 0; i < sizeof(data)/sizeof(unsigned char); i++) {
    m_dataOutBuffer.push_back( (unsigned char)(data & 0x000000ff));
    data = data >> 8;
  }
}
