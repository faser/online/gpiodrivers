/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 
#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <stdexcept>
#include "GPIOBase/usbInterface.h"

using namespace FASER;
/** *******************************************************
 * \brief Constructor: initialises usbLib
 ******************************************************** */
usbInterface::usbInterface() :  m_ctx(nullptr), m_usbDevices(nullptr), m_dev_handle(nullptr), m_interfaceOK(false),
                                m_DEBUG(true), m_EMULATE(false)
{
  if (m_DEBUG){
    //setenv("LIBUSB_DEBUG", "4", 1);
  }
  int rCode = libusb_init(&m_ctx);
  if (rCode < 0){
    ERROR("libusb: could not initialise usblib.ERR code = "<<rCode);
    throw std::runtime_error("libusb: could not initialise usblib");
  }

  // setup tranfer buffers
  m_serialBuffer.resize(1024); // 512 bytes recommended for HS devices
  m_dataBuffer.resize(1024*3);
}

/** *******************************************************
 * \brief Destructor: cleanup resources and exit usblib
 ******************************************************** */
usbInterface::~usbInterface()
{
  m_interfaceOK = false;
  if (m_dev_handle != nullptr){
    libusb_release_interface(m_dev_handle, 0);
    libusb_close(m_dev_handle);
    m_dev_handle = nullptr;
  }
  //libusb_exit(NULL);
}

/** *******************************************************
 * \brief m_DEBUG function: prints information of all attached devices
 ******************************************************** */
void usbInterface::ListDevices()
{
  if (m_usbDevices != nullptr){
    libusb_free_device_list(m_usbDevices, 1);
    m_usbDevices = nullptr;
  }
  
  ssize_t cnt = libusb_get_device_list(m_ctx, &m_usbDevices);
  if (cnt < 0){
    libusb_exit(NULL);
    ERROR("libusb: could not get list of connected deviced. ERR code: "<<cnt);
    throw std::runtime_error("libusb: could not get list of connected deviced.");
  }
  
  libusb_device *dev;
  int i = 0, j = 0;
  uint8_t path[8];
  
  INFO("List of attached USB devices: "
  << "VendorID:ProducID (bus, device, speed) Path of ports the device is conneted to");
  while ((dev = m_usbDevices[i++]) != NULL) {
    struct libusb_device_descriptor desc;
    int r = libusb_get_device_descriptor(dev, &desc);
    if (r < 0) {
      ERROR("failed to get device descriptor");
      return;
    }
    printf("%04x:%04x (bus %d, device %d, ",
           desc.idVendor, desc.idProduct,
           libusb_get_bus_number(dev), libusb_get_device_address(dev));
    
    int speed = libusb_get_device_speed(dev);
    INFO(" speed: ");
    switch (speed){
      case LIBUSB_SPEED_LOW:
        INFO("low");
        break;
      case LIBUSB_SPEED_FULL:
        INFO("full");
        break;
      case LIBUSB_SPEED_HIGH:
        INFO("high");
        break;
      case LIBUSB_SPEED_SUPER:
        INFO("super (USB3)");
        break;
      //case LIBUSB_SPEED_SUPER_PLUS:
      //  INFO("super+ (USB3)";
      //  break;
      default:
        INFO("unknown");
    }
    
    r = libusb_get_port_numbers(dev, path, sizeof(path));
    if (r > 0) {
      printf(" path: %d", path[0]);
      for (j = 1; j < r; j++)
        printf(".%d", path[j]);
    }
    printf("\n");
    
    libusb_config_descriptor *config;
    libusb_get_config_descriptor(dev, 0, &config);
    INFO("Interfaces: "<<(int)config->bNumInterfaces<<" ||| ");

    const libusb_interface *inter;
    const libusb_interface_descriptor *interdesc;
    const libusb_endpoint_descriptor *epdesc;
    for(int i=0; i<(int)config->bNumInterfaces; i++) {
      inter = &config->interface[i];
      INFO("Number of alternate settings: "<<inter->num_altsetting<<" | ");
      for(int j=0; j<inter->num_altsetting; j++) {
        interdesc = &inter->altsetting[j];
        INFO("           "<<" Interface Number: "<<(int)interdesc->bInterfaceNumber<<" | \n"
                          <<"Number of endpoints: "<<(int)interdesc->bNumEndpoints<<" | ");
        for(int k=0; k<(int)interdesc->bNumEndpoints; k++) {
          epdesc = &interdesc->endpoint[k];
          INFO("           "<<"           "<<"Descriptor Type: "<<(int)epdesc->bDescriptorType<<" | \n"
                            <<"EP Address: "<<(int)((epdesc->bEndpointAddress)) <<" | ");
          if ((epdesc->bEndpointAddress & LIBUSB_ENDPOINT_DIR_MASK) == LIBUSB_ENDPOINT_IN){
            INFO( "DIR = IN | ");
          } else {
            INFO( "DIR = OUT | ");
          }
          switch ((epdesc->bmAttributes & LIBUSB_TRANSFER_TYPE_MASK)){
            case LIBUSB_TRANSFER_TYPE_CONTROL:
              INFO( " type = control");
              break;
            case LIBUSB_TRANSFER_TYPE_ISOCHRONOUS:
              INFO( " type = isochronous");
              break;
            case LIBUSB_TRANSFER_TYPE_BULK:
              INFO( " type = bulk");
              break;
            case LIBUSB_TRANSFER_TYPE_INTERRUPT:
              INFO( " type = interrupt");
              break;
            case LIBUSB_TRANSFER_TYPE_BULK_STREAM:
              INFO( " type = bulk_stream");
              break;
            default:
               INFO( " type = unknown");
          }
          INFO(" wMaxPacketSize = "<<epdesc->wMaxPacketSize);
          INFO(" bInterval = "<<epdesc->bInterval);
        }
      }
    }
  }
  
  libusb_free_device_list(m_usbDevices, 1);
  m_usbDevices = nullptr;
  
}


/** *******************************************************
 * \brief Connect to the GPIO board (aka TrackerReadoutBoard)
 ******************************************************** */
void usbInterface::Connect()
{
  bool connectOK = false;
  if (m_EMULATE){
    WARNING("emulation mode active. No hardware access will follow.");
    m_interfaceOK = true;
    return;
  }
  // loop through all devices and find GPIO board : 206b:0c00
  if (m_usbDevices != nullptr){
    libusb_free_device_list(m_usbDevices, 1);
    m_usbDevices = nullptr;
  }
  
  ssize_t cnt =libusb_get_device_list(m_ctx, &m_usbDevices);
  if (cnt < 0){
    libusb_exit(NULL);
    ERROR("libusb: could not get list of connected deviced. ERR code: "<<cnt);
    throw std::runtime_error("libusb: could not get list of connected deviced.");
  }
  
  libusb_device *dev;
  int i = 0;
  
  while ((dev = m_usbDevices[i++]) != NULL) {
    struct libusb_device_descriptor desc;
    int r = libusb_get_device_descriptor(dev, &desc);
    if (r < 0) {
      fprintf(stderr, "failed to get device descriptor");
      throw;
    }
    if (desc.idVendor == VENDOR_ID && desc.idProduct == PRODUCT_ID){
      if (m_DEBUG) {TRACE("Found GPIO board. Connecting ...");}
      int r = libusb_open(dev, &m_dev_handle);
      if (r != 0){
        ERROR("Could not open usb device: ");
        switch (r){
          case LIBUSB_ERROR_NO_MEM:
            ERROR("  memory allocation failure");
            break;
          case LIBUSB_ERROR_ACCESS:
            ERROR("  no permission");
            break;
          case LIBUSB_ERROR_NO_DEVICE:
            ERROR("  device has been disconnected");
            break;
          default:
            ERROR("  unknown error occured");
        }
        INFO("Trying next device");
        // libusb_free_device_list(m_usbDevices, 1);
        // m_usbDevices = nullptr;
        continue;
      }
      int maxPacketSize = libusb_get_max_packet_size (dev, EP_DataRead);
      m_USBDataBufferSize = maxPacketSize*100;
      if (m_DEBUG) {TRACE("Max USB packet size on EP_DataRead enpoint = "<<std::dec<<maxPacketSize);}
      
      connectOK = true;
      
      // claim Interface: GPIO board provides one interface with interface number 0
      try {
        claimInterface(0 /* interface number of GPIO board is 0 */);
      } catch (...)
      {
        continue; // moving to the next device
      }
      break;
    }
  }
  // free device list. Needs to be reloaded any time it is used.
  libusb_free_device_list(m_usbDevices, 1);
  m_usbDevices = nullptr;
  if (!connectOK){
    throw std::runtime_error("Device not found! -> Check connection and power and if all devices are already in use");
  }
  
  m_interfaceOK = true;
}

/** *******************************************************
 * \brief Claim an interface (endpoint) on the opened device
 ******************************************************** */
void usbInterface::claimInterface(int interfaceNumber){
  if (m_DEBUG) {TRACE("Claiming Interface "<<interfaceNumber);}
  if (m_EMULATE){ return; }
    
  if (m_dev_handle == nullptr){
    throw std::runtime_error("Can't claim interface, no open device");
  }
  libusb_detach_kernel_driver(m_dev_handle, interfaceNumber);
  if (libusb_kernel_driver_active(m_dev_handle, interfaceNumber)){
    WARNING("Found another loaded USB driver for this interface / trying to detach it ... ");
    libusb_detach_kernel_driver(m_dev_handle, interfaceNumber);
  }
  
  int r = libusb_claim_interface(m_dev_handle, interfaceNumber);
  if (r != 0){
    switch (r){
      case LIBUSB_ERROR_NOT_FOUND:
        ERROR("Requested interface "<<interfaceNumber<<" does not exist");
        break;
      case LIBUSB_ERROR_NO_DEVICE:
        ERROR("Device disconnected");
        break;
      case LIBUSB_ERROR_BUSY:
        // This error regularly apperas in case more than one GPIO board is connected. Hence it is supressed.
        //ERROR("interface "<<interfaceNumber<<" is BUSY. Claimed by other driver?");
        break;
      // default:
        // if the interface was already claimed we end up here. Expect that another gpio board may be present which is tried next. No error message at this stage!
        //ERROR("can't claim interface, unknown error");
    }
    throw std::runtime_error("Could not claim interface");
  }
}


/** *******************************************************
 * \brief Send data to GPIO board (config & control)
 ******************************************************** */
void usbInterface::SendSerial(unsigned char *data, int length, int &bytesTransferred){
  
  if (!m_interfaceOK){
    throw std::runtime_error("USB interface not OK. Make sure to call ConnectToGPIO() before performing any IO");
  }
  
  if ( (length % 4) != 0 ) {
    ERROR("Cannot send data of length "<<length<<" bytes. Length must be divisible by 4.");
    throw std::runtime_error("Could not send data to GPIO board");
  }

  if (m_DEBUG) {
    TRACE("Writing "<<length<< " bytes to endpoint 0x"<<std::hex<<std::setw(2)<<std::setfill('0')<<(int)EP_SerialWrite<<":");
    for (int i = length-1; i >= 0; i--) { TRACE(std::hex<<std::setw(2)<<std::setfill('0') << (int)data[i]);}
  }
  
  if (m_EMULATE) { bytesTransferred = length; return; }
  
  int r = libusb_bulk_transfer(m_dev_handle, EP_SerialWrite, data, length, &bytesTransferred, 1000 /*timeout in ms */);
  
  if (r != 0){
    ERROR("ERROR sending data to GPIO board, "<< bytesTransferred <<" bytes written. ERROR returned: ");
    switch (r){
      case LIBUSB_ERROR_TIMEOUT:
        ERROR("  TIMEOUT");
        break;
      case LIBUSB_ERROR_PIPE:
        ERROR("  endpoint halted (broken pipe)");
        break;
      case LIBUSB_ERROR_OVERFLOW:
        ERROR("  data overflow");
        break;
      case LIBUSB_ERROR_NO_DEVICE:
        ERROR("  device disconnected");
        break;
      default:
        ERROR("  unknown error");
    }
    throw std::runtime_error("Could not send data to GPIO board");;
  }
}

/** *******************************************************
 * \brief Receive serial data from the GPIO board (config & control)
 ******************************************************** */
std::vector<unsigned char> usbInterface::ReadSerial(int &bytesTransferred){
  if (!m_interfaceOK){
    throw std::runtime_error("USB interface not OK. Make sure to call ConnectToGPIO() before performing any IO");
  }
  
  // clear read buffer
  m_serialBuffer.clear();
  m_serialBuffer.resize(512);
  
  if (m_EMULATE) {
    m_serialBuffer.push_back(0x00);
    m_serialBuffer.push_back(0x00);
    m_serialBuffer.push_back(0x00);
    m_serialBuffer.push_back(0x00);
    return m_serialBuffer;
  }
  
  int r = 0;
  // every usb read operation has to be preceeded by a control comman
  unsigned char controlReqData = 0x02;
  
  r = libusb_control_transfer(m_dev_handle, 0xA2 /* bmRequestType */,
                                     0x23 /* bRequest 0x23 = 35*/,
                                     0x002   /* wValue */,
                                     0x0 /*    wIndex */,
                                     &controlReqData,
                                     1   /* wLength */,
                                     500   /* timeout */
                                      );
  if (r <= 0){
    ERROR("ReadSerial: Control Transfer FAILED with ERROR");
    switch (r){
      case LIBUSB_ERROR_TIMEOUT:
        ERROR("  TIMEOUT");
        break;
      case LIBUSB_ERROR_PIPE:
        ERROR("  endpoint halted (broken pipe)");
        break;
      case LIBUSB_ERROR_INVALID_PARAM:
        ERROR("  transfer size is larger than the operating system and/or hardware can support ");
        break;
      case LIBUSB_ERROR_BUSY:
        ERROR("  called from event handling context ");
        break;
      default:
        ERROR("  unknown error");
    }
    //throw;
  }
  
  int retry = MAX_READ_RETRIES;
  while (retry > 0){
    retry--;
    
    r = libusb_bulk_transfer(m_dev_handle, EP_SerialRead, &(m_serialBuffer[0]), m_serialBuffer.size(), &bytesTransferred, 1000 /*timeout in ms */);
    
    if (r != 0){
      ERROR("ERROR reading control data from GPIO board, bytes read: "<< bytesTransferred <<". ERROR returned: ");
      switch (r){
        case LIBUSB_ERROR_TIMEOUT:
          ERROR("  TIMEOUT");
          break;
        case LIBUSB_ERROR_PIPE:
          ERROR("  endpoint halted (broken pipe)");
          break;
        case LIBUSB_ERROR_OVERFLOW:
          ERROR("  data overflow");
          retry = 0;
          break;
        case LIBUSB_ERROR_NO_DEVICE:
          ERROR("  device disconnected");
          retry = 0;
          break;
        default:
          ERROR("  unknown error");
          retry = 0;
      }
      //throw;
    }
  }
  return m_serialBuffer;
}

/** *******************************************************
 * \brief return bytes read in a vector of 32bit words
 ******************************************************** */
std::vector<uint32_t> usbInterface::ReadSerial32(int &bytesTransferred){
  std::vector<unsigned char> data = ReadSerial(bytesTransferred);
  std::vector<uint32_t> data32;
  
  uint32_t word = 0;
  uint32_t tmp = 0;
  unsigned int bytesStored = 0;
  if (m_DEBUG) {
    TRACE(" ReadSerial: Bytes read = "<<bytesTransferred<<" content = ");
  }
  for (int i = 0; i < bytesTransferred; i++){
    
    tmp = data[i];
    word |= (tmp << ((bytesStored*8)) );
    bytesStored++;
    
    if (bytesStored == 4){
      if (m_DEBUG) { TRACE(" 0x"<<std::hex<<std::setw(8)<<word); }
      data32.push_back(word);
      word = 0;
      bytesStored = 0;
    }
  }
  
  if (bytesStored != 0){
    data32.push_back(word);
  }
  return data32;
}

/** *******************************************************
 * \brief Receive serial data from the GPIO board (config & control)
 ******************************************************** */
std::vector<unsigned char> usbInterface::ReadData(int &bytesTransferred){
  if (!m_interfaceOK){
    ERROR("USB interface not OK. Make sure to call ConnectToGPIO() before performing any IO");
    throw;
  }
  
  // clear read buffer
  m_dataBuffer.clear();
  m_dataBuffer.resize(m_USBDataBufferSize);
  
  if (m_EMULATE) {
    m_dataBuffer.push_back(0x00);
    m_dataBuffer.push_back(0x00);
    m_dataBuffer.push_back(0x00);
    m_dataBuffer.push_back(0x00);
    return m_dataBuffer;
  }
  
 
  int r = 0;
  
    r = libusb_bulk_transfer(m_dev_handle, EP_DataRead, &(m_dataBuffer[0]), m_dataBuffer.size(), &bytesTransferred, 400 /*timeout in ms */);
    m_dataBuffer.resize(bytesTransferred); 
    if (r != 0 && r != LIBUSB_ERROR_TIMEOUT){ // here we ignore timeout errors, as those are regularly happening i there's no data to read back
      ERROR("ERROR reading DAQ data from GPIO board, bytes read:"<< bytesTransferred<<". ERROR returned:");
      switch (r){
        case LIBUSB_ERROR_TIMEOUT:
          ERROR("  TIMEOUT");
          break;
        case LIBUSB_ERROR_PIPE:
          ERROR("  endpoint halted (broken pipe)");
          break;
        case LIBUSB_ERROR_OVERFLOW:
          ERROR("  data overflow");
          break;
        case LIBUSB_ERROR_NO_DEVICE:
          ERROR("  device disconnected");
          break;
        default:
          ERROR("  unknown error");
      }
      INFO("Resetting USB dev handle ");
      r = libusb_reset_device(m_dev_handle);
      if (r != 0){
        ERROR("Could not reset USB device ");
      }
    }
  return m_dataBuffer;
}

/** *******************************************************
 * \brief return bytes read in a vector of 32bit words
 ******************************************************** */
std::vector<uint32_t> usbInterface::ReadData32(int &bytesTransferred){
  std::vector<unsigned char> data = ReadData(bytesTransferred);
  std::vector<uint32_t> data32;
  
  uint32_t word = 0, tmp = 0;
  unsigned int bytesStored = 0;
  
  for (unsigned int i = 0; i < data.size(); i++){
    tmp = data[i];
    word |= tmp << ((bytesStored*8));
    bytesStored++;
    
    if (bytesStored == 4){
      data32.push_back(word);
      word = 0;
      bytesStored = 0;
    }
  }
  if (bytesStored != 0){
    data32.push_back(word);
  }
  return data32;
}


/** *******************************************************
 * \brief Transfer data in buffer
 ******************************************************** */
int usbInterface::SendBuffer(){
  int bytesTransferred = 0;
  if (m_dataOutBuffer.size() == 0){
    WARNING("No data in buffer ");
    return 0;
  }
  
  SendSerial(m_dataOutBuffer.data(), m_dataOutBuffer.size(), bytesTransferred);
  return bytesTransferred;
}

/** *******************************************************
 * \brief Fill output buffer in 8 bit chunks
 ******************************************************** */
void usbInterface::QueueData(uint32_t data){
  
  for (unsigned int i = 0; i < sizeof(data)/sizeof(unsigned char); i++) {
    m_dataOutBuffer.push_back( (unsigned char)(data & 0x000000ff));
    data = data >> 8;
  }
}
