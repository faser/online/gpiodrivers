/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 
#include "GPIOBase/GPIOBaseClass.h"
// #include "cxxopts.hpp" -> requires gcc version >= 4.9
#include <unistd.h>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <chrono>
#include <iomanip>

using namespace FASER;
using std::cout;
using std::endl;
using std::cin;

unsigned int m_moduleMask;
int BoardID;
GPIOBaseClass *gpio = nullptr;
bool useEthernet=false;
bool m_D1ON = false;
bool m_D2ON = false;
bool m_D3ON= false;
bool m_D4ON = false;

const unsigned char lookup[16] = {
  0x0, 0x8, 0x4, 0xc, 0x2, 0xa, 0x6, 0xe,
  0x1, 0x9, 0x5, 0xd, 0x3, 0xb, 0x7, 0xf, };


uint8_t reverseBitOrder(uint8_t n) {
  // Reverse the top and bottom nibble then swap them.
  return (lookup[n&0b1111] << 4) | lookup[n>>4];
}
 

void SetupGPIO ()
{
  if (useEthernet) {
    gpio = new GPIOBaseClass("10.11.65.6", "10.11.65.6",false,  BoardID);
  } else {
    std::cout << "Connecting to TRB via USB"<<std::endl;
    gpio = new GPIOBaseClass(false,  BoardID);
  }

  m_D2ON = false;
  m_D3ON = false;
}

void toggleD1LED() {

  std::cout<<"INFO Toggling D1 LED"<<std::endl;
  static const uint8_t USER_SET_SOFTCOUNTERS = 0x09;
  uint16_t word;
  m_D1ON = !m_D1ON;
  word = word | m_D1ON;
  gpio->SendAndRetrieve(USER_SET_SOFTCOUNTERS,word);
  return;
}

void toggleD2LED() {
  std::cout<<"INFO Toggling D2 LED"<<std::endl;
  std::cout<<"Code Incomplete still, sorry"<<std::endl;
  //trb->GetConfig()->Set_Global_L2SoftL1AEn(!m_D2ON);
  //trb->WriteConfigReg();
  //static const uint8_t USER_SET_CONFIG = 0x08; 
  //uint64_t global_configs = 0x000000000FF800FF;
  //printGlobalConfigs(global_configs)
  uint16_t GlobalDirectParam = 0x0000;
  
  
  m_D2ON = !m_D2ON;
  return;
}

void toggleD3LED() {
  std::cout<<"INFO Toggling D3 LED"<<std::endl;

  uint16_t param = 0x0;
  // enable SoftCounterMux
  param = param | !m_D3ON;
  std::cout<<"Sending direct parameter "<<param<<std::endl;

  if (m_D3ON){
      gpio->SendAndRetrieve(GPIOCmdID::SET_DIRECT_PARAM, param);
      m_D3ON = false;
  } else {
      //trb->SetDirectParam(TRBDirectParameter::SoftCounterMuxEn);
      gpio->SendAndRetrieve(GPIOCmdID::SET_DIRECT_PARAM, param);
      m_D3ON = true;
  }
  return;
}

void toggleD4LED() {

  std::cout<<"INFO Toggling D4 LED"<<std::endl;

  m_D4ON = !m_D4ON;
  uint8_t Field3 = 0x00 | m_D4ON << 7  ;
  uint16_t word = reverseBitOrder(Field3);
  static const uint8_t USER_SET_SCTSLOWCOMMAND = 0x0C;

  gpio->SendAndRetrieve(USER_SET_SCTSLOWCOMMAND, word);

  return;
}

int PrintMenu(int level)
{
  int choice = 0;
  std::string cfgFile;
  std::string arg;
  char* pEnd;
  
  switch (level){
    case 0:
      cout << "Basic control of FASER tracker: "<<endl;
      cout << " 0: Exit"<<std::endl;
      cout << " 9: SetupTRB"<<std::endl;
      cout << " 1: Turn on LED1"<<std::endl;
      cout << " 2: Turn on LED2"<<std::endl;
      cout << " 3: Turn on LED3"<<std::endl;
      cout << " 4: Turn on LED4"<<std::endl;
      cout << "--------------------------------------"<<endl;
      std::cout << "Your choice: ";
      std::cin >> choice;
      if (choice == 0) return level-1;
      return choice;
      
    case 9: // config modules
      cout << " Enter ID of TRB to be used (-1 for don't care)"<<std::endl;
      cin >> arg;
      cin.get(); // also get \n from input stream
      BoardID = strtoul(arg.c_str(), &pEnd, 0);
      SetupGPIO();
      return 0;
      
    case 1: // config modules
      toggleD1LED();
      return 0;
      
    case 2: // config modules
      toggleD2LED();
      return 0;
      
    case 3: // config modules
      toggleD3LED();
      return 0;
      
    case 4: // config modules
      toggleD4LED();
      return 0;

    default:
      std::cout << "Unknown option"<<std::endl;
      return level;
  }
  return -1;
}

void printGlobalConfigs(const uint64_t& Global)
{
  std::cout<<"Content of TRB configuration register object: "<<std::endl
        << "Global config reg     = 0x" <<std::hex <<Global<<std::endl
        << "      RXTimeout       = "<<std::dec << (Global & 0xff) <<std::endl
        << "      Overflow        = "<<std::dec << ((Global >> 8) & 0xfff) <<std::endl
        << "      L1Timeout       = "<<std::dec << ((Global >> (8+12)) & 0xff) <<std::endl
        << "    L1TimeoutDisable  = "<<std::dec << ((Global >> (8+12+8)) & 0x1) <<std::endl
        << "      L2SoftL1AEn     = "<<std::dec << ((Global >> (8+12+8+1)) & 0x1) <<std::endl
        << "      HardwareDelay0  = "<<std::dec << ((Global >> (8+12+8+1+1)) & 0x7) <<std::endl
        << "      HardwareDelay1  = "<<std::dec << ((Global >> (8+12+8+1+1+3)) & 0x7) <<std::endl
        << "      RxTimeoutDisable= "<<std::dec << ((Global >> (8+12+8+1+1+3+3)) & 0x1)<<std::endl;
  //if ((m_FPGA_version >= 0x1C && m_FPGA_version < 0x110) || m_FPGA_version >= 0x11C){
  //  INFO(  "\n      CalLoopNb       = "<<std::dec << ((GlobalLoop ) & 0xFFFFF) <<std::endl
  //      << "      CalLoopDelay    = "<<std::dec << ((GlobalLoop >> (20)) & 0x7FF) <<std::endl
  //      << "      DynamicDelay    = "<<std::dec << ((GlobalLoop >> (20+11)) & 0x1) );
  //}
}

int main ( int argc, char ** argv)
{
  int menuLevel = 0;
  int choice;
  while (menuLevel > -1){
    menuLevel = PrintMenu(menuLevel);
  }
  delete gpio; 
  return 0;
}

