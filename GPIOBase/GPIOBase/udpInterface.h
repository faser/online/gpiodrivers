/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 
#ifndef __udpInterface
#define __udpInterface

#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h> // gethostname
#include <cstring> // strcpy
#include <cctype> // isdigit

#include <vector>
#include "CommunicationInterface.h"


/** *******************************************************
 * \brief This is the Interface communicates to the tracker readout board via UPD (Ethernet)
 *
 * TODO: Error handling needs an overhaul: close any open / initialised connections /
 *  objects in case of an error, automatic retry, throw a proper error object, catch
 *  catchable errors and retry.
 ******************************************************** */

namespace FASER{
  class udpInterface : public CommunicationInterface {
  public:
    udpInterface(std::string SCIP, std::string DAQIP, std::string BoardIP);
    ~udpInterface();
    
    void SetDebug(int v) override { if (v > 0){ m_DEBUG = true;} else {m_DEBUG = false;}}
    void SetEmulate(bool v) override { if (m_interfaceOK && !v) {m_interfaceOK = false;} m_EMULATE = v; }
    void Connect() override;
    void SetSCIP(std::string ipaddr, short port);
    struct sockaddr_in GetSCIP(){return m_recvSerialAddr;}
    void SetDAQIP(std::string ipaddr, short port);
    struct sockaddr_in GetDAQIP(){return m_recvDataAddr;}
    void SetBoardIP(std::string ipaddr, short port);
    struct sockaddr_in GetBoardIP(){return m_boardAddr;}
 
    void SendSerial(unsigned char *data, int length, int &bytesTransferred) override;
    std::vector<unsigned char> ReadSerial(int &bytesTransferred) override;
    std::vector<unsigned char> ReadData(int &bytesTransferred) override;
    static bool IsIPAddress(const char *input);
    static std::string GetHostByName(const char* input);
    // void ReadData();
    
    
  public: // helpers for handling 32bit data words
    void ClearSendBuffer() override {m_dataOutBuffer.resize(0);}
    void QueueData(uint32_t data) override;
    int SendBuffer() override;
    std::vector<uint32_t> ReadSerial32(int &bytesTransferred) override;
    std::vector<uint32_t> ReadData32(int &bytesTransferred) override;

  private:
    int m_socket;
    int m_recvSerial_socket;
    int m_recvData_socket;

    bool m_interfaceOK;
    const uint32_t IPMagicWord = 0x1031AAE1; 
    const int BUFF_SIZE = 4096; // 1500 bytes Ethernet Payload, 20 bytes IP header, 8 bytes UDP header
    const uint16_t boardPort  = 50000;
    const uint16_t serialPort = 50001;
    const uint16_t daqPort    = 50002;
    const uint16_t configPort = 50005;
    void SendMagicWord();
    static const unsigned kMAGICWORD_SIZE = 12;

  private:
    
    /// Definition of Endpoints
    struct sockaddr_in     m_boardAddr;
    struct sockaddr_in     m_recvSerialAddr;
    struct sockaddr_in     m_recvDataAddr;
    struct sockaddr_in     m_boardAddrConfig;
    struct sockaddr_in     m_anyAddr;
 
    /// Steering options
    bool m_DEBUG;
    bool m_EMULATE;
    
    /// Receiving buffer
    std::vector<unsigned char> m_serialBuffer; // for control data
    std::vector<unsigned char> m_dataBuffer; // for daq data
    std::vector<unsigned char> m_dataOutBuffer; // buffer for output data
    static const int MAX_READ_RETRIES = 1;
  };
}

#endif /* __udpInterface */
