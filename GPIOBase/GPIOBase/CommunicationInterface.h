/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/


#ifndef __CommunicationInterface
#define __CommunicationInterface

#include <string>
#include "Logging.hpp"
#include "Exceptions/Exceptions.hpp"

class GPIOCommunicationException : public Exceptions::BaseException { using Exceptions::BaseException::BaseException; };
class GPIOWrongCommunicationException : public Exceptions::BaseException { using Exceptions::BaseException::BaseException; };

namespace FASER {
  
/** *******************************************************
 * \brief Base class for the HW communication to the TRB
 *  Contains only virtual functions. Derived classes
 * contain actual HW dependent implementation
 ******************************************************** */
  class CommunicationInterface {
  
  public: 
    virtual ~CommunicationInterface() = default;
    virtual void Connect() = 0;
    
    virtual void ClearSendBuffer() = 0;
    virtual void QueueData(uint32_t data) = 0;
    void QueueData(std::vector<uint32_t> data) {for ( auto element : data ) {QueueData(element);}}
    virtual int SendBuffer() = 0;
    virtual void SetDebug(int v) = 0;
    virtual void SetEmulate(bool v) = 0;
    
    virtual void SendSerial(unsigned char *data, int length, int &bytesTransferred) = 0;
    virtual std::vector<unsigned char> ReadSerial(int &bytesTransferred) = 0;
    virtual std::vector<uint32_t> ReadSerial32(int &bytesTransferred) = 0;
    virtual std::vector<unsigned char> ReadData(int &bytesTransferred) = 0;
    virtual std::vector<uint32_t> ReadData32(int &bytesTransferred) = 0;
  };
  
}

#endif /* __CommunicationInterface */
