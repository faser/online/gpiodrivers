/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 
#ifndef __TRBAccess
#define __TRBAccess

#include <stdint.h>
#include <vector>
#include <fstream>
#include <thread>
#include <mutex>
#include <string>
#include <atomic>
#include "GPIOBase/GPIOBaseClass.h"
#include "GPIOBase/CommunicationInterface.h"
#include "../TrackerReadout/TRB_ConfigRegisters.h"
#include "../TrackerReadout/ConfigurationHandling.h"
#include "../TrackerReadout/ROR_Header.h"

namespace constant{
 const std::vector<uint16_t> unmasked_vmask(8,0xFFFF);
}

class TRBAccessException : public Exceptions::BaseException { using Exceptions::BaseException::BaseException; };
class TRBConfigurationException : public Exceptions::BaseException { using Exceptions::BaseException::BaseException; };

namespace FASER {
  
  /** *******************************************************
   \brief API to comminucate with the Tracker Readout Board (TRB).
   ******************************************************** */
  class TRBAccess : public GPIOBaseClass
  {
  public:
    /** ***************************************************
     \brief Constructor: Constructor for class using communication udpInterface
		 \param SCIP: Server Computer IP address in format X.X.X.X
		 \param DAQIP: IP address of the DAQ PC (it will be probably same as SCIP, however, it is possible to have two different PCs)
		 \param boardID: Set board ID of board we expect to be communicating with. If using USB connection: Connects to board with this board ID, and If boardID = -1, connects to the first board that is found
		 \param boardIP: Host name of TRB GPIO device. This must be provided if communicating via ethernet.
		 \param emulateInterface: this enables/disables emulated communication interface. It is possible to test software without actual GPIO board
    ****************************************************** */
    TRBAccess(std::string SCIP, std::string DAQIP, int boardID, std::string boardIP, bool emulateInterface = false) : GPIOBaseClass(SCIP, DAQIP, boardID, boardIP, emulateInterface),
        m_daqThread(nullptr), m_processingThread(nullptr), m_daqRunning(false), m_scanMode(false), m_showTransfers(true), m_writeDataFile(false), m_showDataRate(false)
      {
        m_RORHeader.SetSourceID(static_cast<uint32_t>(boardID));
        m_configReg.SetFWVersion(m_FPGA_version);
        m_TRBErrors.resize(m_maxTRBErrorID);
      }
    /** ***************************************************
     \brief Constructor: Constructor for class using communication usbInterface
		 \param emulateInterface: this enables/disables emulated communication interface. It is possible to test software without actual GPIO board
		 \param boardID: connect to the board with a given board ID. If boardID = 0xff: connects to the first board that is found
    ****************************************************** */
    TRBAccess(bool emulateInterface = false, int boardID = 0xff) : GPIOBaseClass(emulateInterface, boardID),
        m_daqThread(nullptr), m_processingThread(nullptr),  m_daqRunning(false), m_scanMode(false), m_showTransfers(true), m_writeDataFile(false), m_showDataRate(false)

      {
        m_RORHeader.SetSourceID(static_cast<uint32_t>(boardID));
        m_configReg.SetFWVersion(m_FPGA_version);
        m_TRBErrors.resize(m_maxTRBErrorID);
      }
    ~TRBAccess();    
    
  public:
    // public structures
    enum ABCD_ReadoutMode { HIT=0, LEVEL, EDGE, TEST };

    // High level functions
    void StartReadout(uint16_t param = 0);
    void StopReadout();
    void PollData();
    void PollData_working();
    void ProcessData();
 
    void SetupStorageStream(std::string filename);
    void ShowDataRate(bool v, int interval = 1) { m_showDataRate = v; m_rateMathInterval = interval;}
    void ShowTransfers(bool showTransfers) { m_showTransfers = showTransfers; }
    
    /** return a vector of 32-bit Words from TRB. Can be passed to event decoder. */
    std::vector<uint32_t> GetTRBData() {std::cout << "ERROR: DEPRECATED! Please use GetTRBEventData() instead!"<<std::endl; std::vector<uint32_t> test; return test;}
    std::vector< std::vector<uint32_t> > GetTRBEventData();
    std::vector <unsigned int> GetTRBErrors() {return m_TRBErrors;}
    //int GetBytesRead() {int tmp = m_bytesRead.exchange(0); return tmp;}
    float GetDataRate() {return m_dataRate;}
    
    void SaveScanStepConfig(ScanStepConfig scanConfig);
    void SCT_EnableDataTaking(unsigned int module);
    void SCT_CalibrationPulse(unsigned int module, unsigned int delay=0, bool CalLoop=false);
    bool IsCalibrationLoopRunning();
    void AbortLoop();

    void SCT_WriteStripmask(unsigned int module, int maskSelect);
    void SCT_WriteStripmaskAllON(unsigned int module);
    
    
    /** \brief Load BCID and L1A counts to SoftCounters register
     */
    void PresetSoftCounters(unsigned int bcid, unsigned int L1A);
    
    /** \brief Load BCID and L1A counts to SoftCounters register
     */
    void ReadSoftCounters(unsigned int &bcid, unsigned int &L1A);

    
    /** \brief Returns a pointer to the object holding the basic TRB configuration
     */
    ConfigReg* GetConfig(){return &m_configReg;}
    
    /** \brief Write the stored configuration to the TRB. Configuration can be accessed using GetConfig() function.
     */
    void WriteConfigReg();
    
    /** \brief ReadConfig from TRB
     */
    std::vector<uint16_t>  ReadConfigReg();
   
    /** \brief Read PLL error counter from TRB
     */
    uint16_t ReadPLLErrorCounter();
 
    /** \brief Returns a pointer to the object holding the phase delay configuration
     */
    PhaseReg* GetPhaseConfig(){return &m_phaseReg;}
    
    /** \brief Write the stored phase configuration to the TRB. Configuration can be accessed using GetPhaseConfig() function.
     */
    void WritePhaseConfigReg();
    void ApplyPhaseConfig();
    
    /** \brief Read PhaseConfig from TRB
     */
    std::vector<uint16_t>  ReadPhaseConfigReg();
    
    
    /** \brief Returns a pointer to the object holding the SCT slow command register config
     */
    SCT_SlowCommandReg* GetSCTSlowCommandBuffer(){return &m_SCTSlowCommandReg;}
    
    /** \brief Write the stored SCTSlowCommandBuffer to the TRB. Configuration can be accessed using GetSCTSlowCommandBuffer() function.
     */
    void WriteSCTSlowCommandBuffer();
    
    void SendSCTSlowCommandBuffer(uint8_t modules, bool CalLoop = false);
  
    /** \brief Send hardware reset signal to specidied module (hard reset)
     */
    void GenerateHardReset(uint8_t modules);
    
    /** \brief Send soft reset signal to specidied module (soft reset)
     */
    void GenerateSoftReset(uint8_t modules);
    
    /** \brief Send command to perform ECR and reset module conters (soft reset)
     */
    //bool ModuleCounterReset(unsigned int module){;}

    /** \brief Reset FIFO-L1 of RX data module manager
     */
    void FIFOReset();
    
    /** \brief Reset Err counter of RX data module manager
     */
    void ErrCntReset();

   /** \brief Reset L1A counter
    */
    void L1CounterReset();
    
    /** \brief generate an L1A
    */
    void GenerateL1A(uint8_t modules, bool loop = false);
    
   
    /** \brief Generates a BCR sent to the modules
     */
    void GenerateBCReset(uint8_t modules);
    
    /** \brief Set a specific register in the TRB
     */
    void SetDirectParam(uint16_t param);
    
    /** \brief Readout all counters.
     
     available counters: BCID, L1A / event
    */
    //bool ReadCounters(unsigned int &bcid, unsigned int &eventID){;}

    void VerifyConfigReg();
    
    /** \brief Read and decode the status bits of the TRB
     */
    void PrintStatus();
    
    /** \brief Readback configuration registers on TRB and print in human readable form
     */
    void ReadbackAndPrintConfig();

    void ConfigureSCTModule(ConfigHandlerSCT *cfg, int module);
    
  private:
    ConfigReg m_configReg; // trb config registers
    PhaseReg m_phaseReg; // trb phase registers
    SCT_SlowCommandReg m_SCTSlowCommandReg;
    std::ofstream m_dataOutStream;
    std::thread *m_daqThread;
    std::thread *m_processingThread;
    bool m_daqRunning;
    bool m_scanMode;
    RORHeader m_RORHeader;
    std::mutex mMutex_TRBEventData;
    std::mutex mMutex_TRBDataBuffer;
    std::mutex mMutex_TRBByteBuffer;
    std::vector <unsigned char> m_DataByteBuffer;
    std::vector <uint32_t> m_DataWordOutputBuffer;
    std::vector <uint32_t> m_TRBData;
    std::vector < std::vector <uint32_t> > m_TRBEventData; // complete TRB raw data for every event
    std::vector <unsigned int> m_TRBErrors; 
    
    bool m_EODWordFound;
    bool m_showTransfers; // show bytes transfered
    bool m_writeDataFile;
    bool m_showDataRate;
    std::atomic<float> m_dataRate;
    int m_rateMathInterval = 10;
    
    static const unsigned int m_maxTRBErrorID = 9;
  };
  
  class TRBReadoutParameters {
  public:
    
    /** Readout falgs, can be passed as bit mask to StartReadout()
     */
    static const uint16_t READOUT_L1COUNTER_RESET 	= 0x080;
    static const uint16_t READOUT_ERRCOUNTER_RESET  = 0x100;
    static const uint16_t READOUT_FIFO_RESET        = 0x200;
  };
  
  class TRBStatusParameters {
  public:
    
    /** Status bits
     */
    static const uint16_t STATUS_FINEPHASEOK 	= 0x1;
    static const uint16_t STATUS_LOCALCLKSEL 	= 0x2;
    static const uint16_t STATUS_TLBCLKSEL 	= 0x4;
    static const uint16_t STATUS_BUSY    	= 0x8;
    static const uint16_t STATUS_SAFEPHASEDET0 	= 0x10;
    static const uint16_t STATUS_SAFEPHASEDET1 	= 0x20;
    static const uint16_t STATUS_CALLOOPRUNNING = 0x40;
  };
  
}


/** *******************************************************
 * \brief Definitions of CommandRequest IDs.
 ******************************************************** */
class TRBCmdID:public GPIOCmdID {
public:
  static const uint8_t USER_SET_PHASE      = 0x08; // 1 words, paylod 1 MSB: Address, 15 LSB data
  static const uint8_t USER_SET_SOFTCOUNTERS     = 0x09; // 3 words, paylod 2 MSB: Address, 14 LSB data
  static const uint8_t USER_SET_CONFIG     = 0x0A; // 6 words, paylod 3 MSB: Address, 13 LSB data
  static const uint8_t USER_SET_APPLYPHASE = 0x0B; // no argument
  static const uint8_t USER_SET_SCTSLOWCOMMAND = 0x0C; // 15 words, paylod 4 MSB: Address, 12 LSB data
  static const uint8_t USER_SET_SENDCOMMAND= 0x0D; // 1 word paylod, send command to SCT module
  static const uint8_t USER_GET            = 0x10;
  static const uint8_t USER_SET_CMD        = 0x11;
};

class TRBErrors:public GPIOErrors {
};

class TRBWordID:public GPIOWordID{
};
    

#endif /* __TRBAccess */
