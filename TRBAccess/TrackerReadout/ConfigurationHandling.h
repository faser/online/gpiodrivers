/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 
#ifndef __ConfigurationHandling
#define __ConfigurationHandling

#include <string>

#include "nlohmann/json.hpp"
#include "Logging.hpp"

namespace FASER {
  namespace defaults {
    const bool sct_masking(false);
  }
  class ConfigHandlerSCT {
  public:
    ConfigHandlerSCT() {MakeTemplateConfig();}
    
  public:
    void MakeTemplateConfig ();
    void EnableSingleChip (unsigned int chipID);
    void EnableChips(std::vector<unsigned int> chipIDs);
    
    void ReadFromFile (std::string filename);
    void WriteToFile (std::string filename);
    
    // Accessors for Config objects
    void SetCalMode(uint16_t value);
    void SetReadoutMode(uint16_t value);
    void SetEdgeDetection(bool value);

    void SetStrobeDelay(unsigned int chipID, unsigned int value);
    unsigned int GetStrobeDelay(unsigned int chipID);
    
    void SetThreshold(unsigned int chipID, unsigned int value);
    unsigned int GetThreshold(unsigned int chipID);

    void SetThresholdCalibReg(unsigned int chipID, unsigned int thr, unsigned int calib = 0);
    unsigned int GetThresholdCalibReg(unsigned int chipID, unsigned int calib = 0);
    unsigned int GetThresholdCalibReg(unsigned int chipID, unsigned int thr, unsigned int calib);
    
    void SetBiasDac(unsigned int chipID, unsigned int value);
    void SetBiasDac(uint16_t value);
    unsigned int GetBiasDac(unsigned int chipID);
    
    void SetTrimDac(unsigned int chip, unsigned int channel, unsigned int value);
    unsigned int GetEncodedTrimDac(unsigned int chipID, unsigned int channel);
    
    void SetConfigReg(unsigned int chipID, uint16_t);
    unsigned int GetConfigReg(unsigned int chipID);
    
    void SetStripMask(unsigned int chipID, std::vector<uint16_t>);
    void SetStripMaskBit(unsigned int chipID, unsigned int strip, bool mask);
    std::vector<uint16_t> GetStripMask(unsigned int chipID);
    std::vector<uint16_t> GetStripMaskRegister(unsigned int chipID);

    void SetID(unsigned long value) { m_cfg["ID"] = value; }    
    unsigned long GetID() { return m_cfg["ID"]; }

    void SetPlaneID(unsigned int value) { m_cfg["PlaneID"] = value; }
    unsigned int GetPlaneID() { return m_cfg["PlaneID"]; }
    
    void SetTRBChannel(unsigned int value) { m_cfg["TRBChannel"] = value; }
    unsigned int GetTRBChannel() {return m_cfg["TRBChannel"];}
    std::vector <unsigned int> GetChipIDs();

    void SetApplySCTMasks(bool value){ m_cfg["ApplySCTMasks"]=value;}
    bool GetApplySCTMasks(){ return m_cfg["ApplySCTMasks"].get<bool>();}

    bool hasTrimDAC(unsigned int chipID);
    bool hasTrimTarget(unsigned int chipID);
    bool hasStripMask(unsigned int chipID);

    // threshold target to which the chip is trimmed from the TrimScan
    void SetTrimTarget(unsigned int chipID, double value);
    double GetTrimTarget(unsigned int chipID);
    
    // parameters from fit to response curve
    void SetP0(unsigned int chipID, double value);
    void SetP1(unsigned int chipID, double value);
    void SetP2(unsigned int chipID, double value);

    double GetP0(unsigned int chipID);
    double GetP1(unsigned int chipID);    
    double GetP2(unsigned int chipID);

  private:
    void SetParam(unsigned int chipID, double value, std::string param);
    double GetParam(unsigned int chipID, std::string param);
    
  private:
    nlohmann::json m_cfg;
    
  private: // helper functions
    bool check_chipID(unsigned int chipID);
    unsigned int mV2dac(float mV);
  };
}

#endif // -- ConfigurationHandling
