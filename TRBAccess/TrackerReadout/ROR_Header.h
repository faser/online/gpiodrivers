/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 
#ifndef __TRACKER_ROR_HEADER
#define __TRACKER_ROR_HEADER

#include <iostream>
#pragma GCC diagnostic push  // disable warning as unclear how to best fix
#pragma GCC diagnostic ignored "-Wold-style-cast"


namespace FASER {

/*****************************************************
 \brief Definition of storage structures for the Scan config payload.
 *************************************************** */
class ScanStepConfig
{
public:
  ScanStepConfig() {;} // Initialise all parameters
  // Storage of parameters
public:
  uint8_t ChipID;
  uint8_t CalCharge;
  uint8_t StrobeDelay;
  uint8_t TrimRange;
  std::vector<uint16_t> TrimData; // contains channel + DAC value
  uint8_t Threshold;
  uint8_t CalMode;
  uint32_t ExpectedL1A;
  
  /**
   \brief Write data to stream object
   TODO: for binary writes use read / write functions instead of <<
   */
  void Write(std::ostream &out)
  {
    out.write((char*)&ChipID, sizeof(ChipID));
    out.write((char*)&CalCharge, sizeof(CalCharge));
    out.write((char*)&StrobeDelay, sizeof(StrobeDelay));
    for (auto word : TrimData) {out.write((char*)&word, sizeof(word));}
    out.write((char*)&TrimRange, sizeof(TrimRange));
    out.write((char*)&Threshold, sizeof(Threshold));
    out.write((char*)&CalMode, sizeof(CalMode));
    out.write((char*)&ExpectedL1A, sizeof(ExpectedL1A));
  }
  
  /**
   \brief Read data from stream object and fill it in member variables
   TODO: for binary writes use read / write functions instead of >>
   */
  void Read(std::istream &in)
  {
    in.read((char*)&ChipID, sizeof(ChipID));
    in.read((char*)&CalCharge, sizeof(CalCharge));
    in.read((char*)&StrobeDelay, sizeof(StrobeDelay));
    for (unsigned int i = 0; i < TrimData.size(); i++) {in.read((char*)&TrimData[i], sizeof(TrimData[i]));}
    in.read((char*)&TrimRange, sizeof(TrimRange));
    in.read((char*)&Threshold, sizeof(Threshold));
    in.read((char*)&CalMode, sizeof(CalMode));
    in.read((char*)&ExpectedL1A, sizeof(ExpectedL1A));
  }
  
  unsigned int size() { return (sizeof(ChipID) + sizeof(CalCharge) + sizeof (StrobeDelay) + sizeof(TrimRange) + (TrimData.size()*sizeof(uint16_t)) + sizeof(Threshold) + sizeof(CalMode) + sizeof(ExpectedL1A));}

  void Print()
  {
    std::cout <<  "-- ScanStepConfig -- "<<std::endl<< "  Threshold = "<<std::dec<<(int)((unsigned char)Threshold)<<std::endl
    << " ExpectedL1A = "<<std::dec<<ExpectedL1A<<std::endl;
  }
  
};


/*****************************************************
 \brief Class defining the ROR event header
 *************************************************** */
class RORHeader
{
public:
  RORHeader() {
    m_SizeHeader = sizeof(m_StartOfHeaderMarker) + sizeof(m_FragmentTag) + sizeof(m_TriggerBits) + sizeof(m_FormatVersion) + sizeof(m_SizeHeader) + sizeof(m_SizeData) + sizeof(m_SourceID) + sizeof(m_EventID) + sizeof(m_BCID) + sizeof(m_DataStaus) + sizeof(m_Timestamp);
    
    if (m_SizeHeader != 36) { std::cout << "WARNING: unexpected ROR header size"<<std::endl;}
    
  }
  
  void SetFragmentTag(uint8_t val) {m_FragmentTag = val;}
  uint8_t GetFragmentTag() {return m_FragmentTag;}
  
  void SetTriggerBits(uint16_t val) {m_TriggerBits = val;}
  uint16_t GetTriggerBits() {return m_TriggerBits;}
  
  void SetFormatVersion(uint16_t val) {m_FormatVersion = val;}
  uint16_t GetFormatVersion() {return m_FormatVersion;}
  
  void SetDataSize(uint32_t val) {m_SizeData = val;}
  uint32_t GetDataSize() {return m_SizeData;}
  
  void SetSourceID(uint32_t val) {m_SourceID = val;}
  uint32_t GetSourceID() {return m_SourceID;}
  
  void SetEventID(uint64_t val) {m_EventID = val;}
  void SetEventID(uint32_t L1ID, uint64_t ECRC) { m_EventID = L1ID; m_EventID |= (ECRC<<24);}
  uint64_t GetEventID() {return m_EventID;}
  void GetEventID(uint32_t &L1ID, uint64_t &ECRC) { L1ID = (m_EventID & 0xFFFFFF); ECRC = (m_EventID >> 24);}
  
  void SetBCID(uint16_t val) {m_BCID = val;}
  uint16_t GetBCID() {return m_BCID;}
  
  void SetDataStatus(uint16_t val) {m_DataStaus = val;}
  uint16_t GetDataStatus() {return m_DataStaus;}
  
  void SetTimeStamp(uint64_t val) {m_Timestamp = val;}
  uint64_t GetTimeStamp() {return m_Timestamp;}
  
  /**
   \brief Write data to stream object
   TODO: for binary writes use read / write functions instead of <<
   */
  void Write(std::ostream &out)
  {
    out.write((const char*)&m_StartOfHeaderMarker, sizeof(m_StartOfHeaderMarker));
    out.write((char*)&m_FragmentTag, sizeof(m_FragmentTag));
    out.write((char*)&m_TriggerBits, sizeof(m_TriggerBits));
    out.write((char*)&m_FormatVersion, sizeof(m_FormatVersion));
    out.write((char*)&m_SizeHeader, sizeof(m_SizeHeader));
    out.write((char*)&m_SizeData, sizeof(m_SizeData));
    out.write((char*)&m_SourceID, sizeof(m_SourceID));
    out.write((char*)&m_EventID, sizeof(m_EventID));
    out.write((char*)&m_BCID, sizeof(m_BCID));
    out.write((char*)&m_DataStaus, sizeof(m_DataStaus));
    out.write((char*)&m_Timestamp, sizeof(m_Timestamp));
  }
  
  /**
   \brief Read data from stream object and fill it in member variables
   TODO: for binary writes use read / write functions instead of >>
   */
  void Read(std::istream &in)
  {
    in.read(reinterpret_cast<char*>(const_cast<unsigned char*>(&m_StartOfHeaderMarker)), sizeof(m_StartOfHeaderMarker));
    in.read((char*)&m_FragmentTag, sizeof(m_FragmentTag));
    in.read((char*)&m_TriggerBits, sizeof(m_TriggerBits));
    in.read((char*)&m_FormatVersion, sizeof(m_FormatVersion));
    in.read((char*)&m_SizeHeader, sizeof(m_SizeHeader));
    in.read((char*)&m_SizeData, sizeof(m_SizeData));
    in.read((char*)&m_SourceID, sizeof(m_SourceID));
    in.read((char*)&m_EventID, sizeof(m_EventID));
    in.read((char*)&m_BCID, sizeof(m_BCID));
    in.read((char*)&m_DataStaus, sizeof(m_DataStaus));
    in.read((char*)&m_Timestamp, sizeof(m_Timestamp));
  }
  
  void Print()
  {
    std::cout << std::endl << "ROR_Header: "<<std::endl
    << " FragmentTag      = 0x"<<std::hex<<(int)m_FragmentTag<<std::endl
    << " m_TriggerBits    = 0x"<<std::hex<<m_TriggerBits<<std::endl
    << " m_FormatVersion  = 0x"<<std::hex<<m_FormatVersion<<std::endl
    << " m_SizeHeader     = 0x"<<std::hex<<m_SizeHeader<<std::endl
    << " m_SizeData       = 0x"<<std::dec<<m_SizeData<<std::endl
    << " m_EventID        = "<<std::dec<<m_EventID<<std::endl
    << " m_DataStaus      = 0x"<<std::hex<<m_DataStaus<<std::endl;
  }
  
  
public:
  static const uint8_t TAG_DATA            = 0x10;
  static const uint8_t TAG_SCAN_THR        = 0x11;
  static const uint8_t TAG_SCAN_STROBE     = 0x12;
  static const uint8_t TAG_SCAN_TRIM       = 0x13;
  static const uint8_t TAG_SCAN_TRIMRANGE  = 0x14;
  
  static const uint16_t DATA_STATUS_OK       = 0x00;
  static const uint16_t DATA_STATUS_SCANSTEP = 0xf0;
  static const uint16_t DATA_STATUS_EOD      = 0xfe;
  
private:
  const uint8_t m_StartOfHeaderMarker = 0x0; // some magic word
  uint8_t m_FragmentTag;
  uint16_t m_TriggerBits;
  uint16_t m_FormatVersion;
  uint16_t m_SizeHeader;
  uint32_t m_SizeData;
  uint32_t m_SourceID; // this should hold the Plane ID
  uint64_t m_EventID;  // 24 LSB = Hardware counter, 48 MSB = ECR counts
  uint16_t m_BCID;
  uint16_t m_DataStaus;
  uint64_t m_Timestamp;
  
};
}
#pragma GCC diagnostic pop    
#endif // __TRACKER_ROR_HEADER
