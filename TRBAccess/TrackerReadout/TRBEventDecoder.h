/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 
#ifndef _TRBEventDecoder
#define _TRBEventDecoder

#include <string>
#include <cstdint>
#include <vector>
#include <map>
#include "../TrackerReadout/ROR_Header.h"
#include "EventFormats/FletcherChecksum.hpp"
#include "Logging.hpp"

namespace FASER {
  
  /** \brief Support class holding event information of one SCT module
   */
  class SCTEvent {
  public:
    SCTEvent (unsigned int moduleID, unsigned int l1id, unsigned int bcid);
    ~SCTEvent();
    
    void AddHeader(unsigned int l1id, unsigned int bcid);
    void AddHit (unsigned int chip, unsigned int strip, unsigned int pattern);
    void AddError (unsigned int chip, unsigned int err);
    void AddUnknownChip (unsigned int chip) {m_UnkownChips.push_back(chip);}
    
    unsigned int GetNHits();
    unsigned int GetNHits(int chip);
    std::vector < std::vector < std::pair<uint8_t, uint8_t> > > GetHits() {return m_Hits;}
    std::vector < std::pair<uint8_t, uint8_t> >                 GetHits(int chip);
    std::vector < std::vector < uint8_t > >                     GetErrors() {return m_Errors;}
    std::vector < uint8_t >                                     GetErrors(int chip);
    std::vector < uint8_t >                                     GetUnknownChips() {return m_UnkownChips;}
    
    bool HasError() {return m_hasError;}
    bool IsComplete() {return m_complete;}
    void SetComplete() {m_complete = true;}
    bool MissingData() {return m_missingData;}
    void SetMissingData() {m_missingData = true;}
    bool BCIDMismatch() {return m_bcidMismatch;}
    void SetBCIDMismatch() {m_bcidMismatch = true;}
    unsigned short GetL1ID() {return m_l1id;}
    unsigned short GetBCID() {return m_bcid;}
    
  private:
    std::vector < std::vector < std::pair<uint8_t, uint8_t> > > m_Hits; // chips[12][nHits] , hit = pair {Strip Nr, Hit pattern}
    std::vector < std::vector < uint8_t > > m_Errors; // chips[12][error]
    std::vector < uint8_t > m_UnkownChips; // chipIDs that were not expected
    unsigned short m_moduleID;
    unsigned short m_bcid;
    unsigned short m_l1id;
   
    bool m_complete; // sepcifies if data from all chips (i.e. LED and LEDX lines is inserted
    bool m_missingData; // sepcifies if data from all chips (i.e. LED and LEDX lines is inserted
    bool m_bcidMismatch; // bcid mismatch between led and ledX data streams
    bool m_hasError; // bcid mismatch between led and ledX data streams
    std::map<unsigned int, unsigned int> m_chipIDMap; // maps chipID to vector index
  };
  
  /** \brief Support class holding the information for a single event.
   */
  class TRBEvent {
  public:
    TRBEvent (unsigned int L1ID, unsigned int BCID) : m_L1ID(L1ID), m_BCID(BCID) {;}
    ~TRBEvent();
    
    void AddModule(unsigned int moduleID, SCTEvent *event);
    void AddError(uint16_t error);
    SCTEvent* GetModule(unsigned int moduleID);
    
    std::vector<uint16_t> GetErrors(){return m_Errors;}
    unsigned int GetL1ID() {return m_L1ID;}
    void SetL1ID(int value) {m_L1ID = value;}
    unsigned int GetBCID() {return m_BCID;}
    void SetBCID(int bcid) {m_BCID = bcid;}
    unsigned int GetNModulesPresent() { return m_HitsPerModule.size();}
    bool GetIsChecksumValid(){return m_IsChecksumValid;} 
    void SetIsChecksumValid(bool value) {m_IsChecksumValid = value;} 
 
  private:
    std::vector<uint16_t> m_Errors;
    std::vector< SCTEvent *> m_HitsPerModule;
    std::map<unsigned int, unsigned int> m_moduleID_Index; // mapping moduleID to vector index
    
    unsigned int m_L1ID;
    unsigned int m_BCID;
    bool m_IsChecksumValid = false;
    
  };
  
  
  /** \brief Support class providing a contineous bitstream, used to analyse SCT module data.
   The input vector is expected to contain the module data in the 24 LSB of the 32 bit words,
   as received from the tracker readout board. Bitstream data is stringed together and available in
   junks of 32-bits using Get32BitWord(). RemoveBits(n) will remvove the n MSB from the bitstream.
   Next call to Get32BitWord() will have the reamining bitstream aligned properly at the MSB of the
   word.
   */
  class Bitstream {
  public:
    Bitstream(std::vector<uint32_t> data);
    
    void Add(Bitstream stream) {m_bitstreamData.insert(m_bitstreamData.end(), stream.GetDataVector().begin(), stream.GetDataVector().end()); }
    void FillData(std::vector<uint32_t> data){ m_bitstreamData = data;}
    void RemoveBits(unsigned int n);
    uint32_t GetWord32() {return m_currentBuffer;}
    std::vector<uint32_t> GetDataVector() {return m_bitstreamData;}
    bool BitsAvailable() {return (m_bitsAvailable > 0);}
    unsigned int NBitsAvailable() {return m_bitsAvailable;}
    unsigned int AvailableWords() {return m_bitstreamData.size();}
    
  private:
    uint32_t m_currentBuffer;
    std::vector<uint32_t> m_bitstreamData;
    unsigned int m_bitsUsedOfNextWord;
    long m_bitsAvailable;
    
    const unsigned int m_usedBitsPerWord = 24;
  };



  /** \brief This class contains the decoding functionality of the data bitstream received from the tracker readout board.
      Data is received in words of 32 bits and contain
      1 word TRB header, n words TRB data, m words module data, 2 words trailer
   */
  class TRBEventDecoder {
    
    // these are the definition of bitmasks and data word identifieres
    const uint32_t MASK_TRBFRAME            = 0xC7000000;
    const uint32_t TRB_HEADER               = 0x00000000;
    const uint32_t TRB_TRAILER              = 0x01000000;
    const uint32_t TRB_END                  = 0x07000eee;
    //const uint32_t TRB_END                  = 0xCAFEDECA;
    
    const uint32_t MASK_WORDTYPE            = 0xC0000000;
    const uint32_t WORDTYPE_TRBHEADER       = 0x0;
    const uint32_t WORDTYPE_TRBTRAILER      = 0x0;
    const uint32_t WORDTYPE_TRBDATA         = 0x40000000;
    const uint32_t WORDTYPE_MODULEDATA_LED  = 0x80000000;
    const uint32_t WORDTYPE_MODULEDATA_LEDX = 0xC0000000;
    
    const uint32_t MASK_TRBDATATYPE             = 0x7000000;
    static const uint32_t TRBDATATYPE_BCID             = 0x0000000;
    static const uint32_t TRBDATATYPE_TRBERROR         = 0x1000000;
    static const uint32_t TRBDATATYPE_MODULEERROR_LED  = 0x2000000;
    static const uint32_t TRBDATATYPE_MODULEERROR_LEDX = 0x3000000;
    
    const uint32_t MASK_FRAMECNT    = 0x38000000;
    const uint32_t RSHIFT_FRAMECNT  = 27;
    const uint32_t MASK_BCID     = 0xFFF;
    const uint32_t MASK_ERROR    = 0xF;
    const uint32_t MASK_MODULEDATA_MODULEID    = 0x7000000;
    const uint32_t RSHIFT_MODULEDATA_MODULEID  = 24;
    const uint32_t MASK_MODULEDATA = 0xFFFFFF;
    const uint32_t MASK_TRBDATA_MODULEID       = 0x0700000;
    const uint32_t SHIFT_TRBDATA_MODULEID       = 20;
    const uint32_t MASK_EVNTCNT  = 0xFFFFFF;
    const uint32_t MASK_CRC      = 0xFFFFFF;
    const uint32_t END_OF_DAQ    = 0x7000EEE; // magic word
    
    
    const unsigned int usefulBitsModuleData = 32 ; // How many bits are used per word? All MASK / TAG constants rely on this alignment
    // MASKS for module data
    
    const uint32_t MASK_MODULE_HEADER = 0xFC002000;
    const uint32_t TAG_MODULE_HEADER  = 0xE8002000;
    const uint32_t MASK_MODULE_TRAILER= 0xFFFF0000;
    const uint32_t TAG_MODULE_TRAILER = 0x80000000;
    const uint32_t MASK_MODULE_ERROR  = 0xE0200000;
    const uint32_t TAG_MODULE_ERROR   = 0x00200000;
    const uint32_t MASK_MODULE_CONFIG = 0xE1C02010;
    const uint32_t TAG_MODULE_CONFIG  = 0x1C02010;
    const uint32_t MASK_MODULE_DATA   = 0xC0040000;
    const uint32_t TAG_MODULE_DATA    = 0x40040000;
    const uint32_t MASK_MODULE_NODATA = 0xE0000000;
    const uint32_t TAG_MODULE_NODATA  = 0x20000000;
    
    // access data modules with bit shift & mask
    const uint32_t RSHIFT_MODULE_BCID = 14;
    const uint32_t MASK_MODULE_BCID   = 0xff;
    const uint32_t RSHIFT_MODULE_L1ID = 22;
    const uint32_t MASK_MODULE_L1ID   = 0xf;
    
    const uint32_t RSHIFT_CHIPADD_CONF = 25;
    const uint32_t MASK_CHIPADD_CONF   = 0xF;
    const uint32_t RSHIFT_CONF1        = 14;
    const uint32_t RSHIFT_CONF2        = 5;
    const uint32_t MASK_CONF           = 0xFF;
    const uint32_t RSHIFT_CHIPADD_ERR  = 25;
    const uint32_t MASK_CHIPADD_ERR    = 0xF;
    const uint32_t RSHIFT_ERR          = 22;
    const uint32_t MASK_ERR            = 0x7;
    const uint32_t RSHIFT_CHIPADD_DATA = 26;
    const uint32_t MASK_CHIPADD_DATA   = 0xF;
    const uint32_t RSHIFT_CHANNEL_DATA = 19;
    const uint32_t MASK_CHANNEL_DATA   = 0x7F;

    
  public:
    TRBEventDecoder();
    ~TRBEventDecoder();
    
    void LoadData(std::vector<uint32_t> data); // Broken & Deprecated
    void LoadTRBEventData(std::vector < std::vector<uint32_t> > data);
    void LoadTRBEventData(std::vector<uint32_t> event);
    void LoadDataFromFile(std::string filename);
    void LoadModuleDataFromFile(std::string filename);
    void LoadRORDataFromFile(std::string filename, unsigned int module = 0, std::vector< std::vector <uint16_t> > *moduleConfig = nullptr, std::ofstream *outstream = nullptr);
    
    void Read_RORHeader(std::ifstream &in, std::ofstream *out = nullptr);
    
    
    TRBEvent* DecodeData(bool verbose = true, bool singleEvent = false);
    void DecodeModuleData(unsigned int i, std::vector< std::vector<uint32_t> > moduleData, bool firstpass, std::vector< std::vector <uint16_t> > *moduleConfig = nullptr);
    void DecodeModuleData(unsigned int i, std::vector< std::vector <uint16_t> > *moduleConfig = nullptr);
    void DecodeModuleData(unsigned int module, std::vector< std::vector<uint32_t> > moduleData, TRBEvent *trbEvent, std::vector< std::vector <uint16_t> > *moduleConfig);
    void DecodeModuleData(unsigned int module, TRBEvent *trbEvent, std::vector < std::vector <uint16_t> > *moduleConfig);
    
    void CountHeaderTrailer( Bitstream bitstream, int &foundHeaders, int &foundTrailers);
    
    /** \brief Workaround for FW version < 1.2: Decode SCT module data to find module header
     */
    bool IsHeader(std::vector<uint32_t> data, unsigned int &L1ID, unsigned int &BCID);
    /** \brief Check TRB word for TRB header. Extracts L1ID.
     */
    bool IsHeader(uint32_t data, unsigned int &L1ID);
    /** \brief Workaround for FW version < 1.2: Decode SCT module data to find module trailer
     */
    bool IsTrailer(std::vector<uint32_t> data);
    /** \brief Check TRB word for TRB trailer. Extracts checksum.
     */
    bool IsTrailer(uint32_t data, uint32_t &checksum);

    bool IsModuleData(uint32_t data, unsigned int &ModuleData);
    /** \brief Check TRB word for BCID data.
     */
    bool IsTRBData(uint32_t data, unsigned int &BCID);
    
    /** \brief Check TRB word for TRB ERROR
     */
    bool HasError(uint32_t data, uint16_t &error);
    bool IsModuleError(uint32_t data, uint16_t &error);
    /** \brief Check TRB word for end of DAQ word
     */
    bool IsEndOfDAQ(uint32_t data);
    
    int  GetTRBFrameCnt(uint32_t data);
    
    bool ModuleDataHasHeader(unsigned int &L1ID, unsigned int &BCID);
    bool ModuleDataHasTrailer();
   
    std::vector <ScanStepConfig> GetScanStepConfigs() { return m_ScanSteps;}
    std::vector <std::vector <TRBEvent *> > GetEventsPerScanStep() {return m_EventsPerScanStep;}
    
    void SetEventInfoVerbosity(int v) {m_PrintEventInformation = v;}
    
    // Accessors
    Bitstream* GetModuleBitstream(unsigned int module, bool led);
    std::vector <TRBEvent *> GetEvents() {return m_Events;}
    
    void ClearEvents();
    
    std::string TRBErrorToString(uint16_t error);
    
  private:
    int m_PrintEventInformation;
    
    std::vector <TRBEvent *> m_Events;
    std::vector <std::vector <TRBEvent *> > m_EventsPerScanStep;
    std::vector <ScanStepConfig> m_ScanSteps;
    
    std::vector<uint32_t> m_data;
    std::vector<char> m_dataBytes;
    std::vector< std::vector<uint32_t> > m_moduleData_led; /// Vector holding 8 vectors of 32bit words = 8 modules
    std::vector< std::vector<uint32_t> > m_moduleData_ledx; /// Vector holding 8 vectors of 32bit words = 8 modules
    RORHeader m_RORHeader;
    ScanStepConfig m_ScanStepConfig;
    bool m_headerOK;
    bool m_trailerOK;
    int m_eventCnt;
    int m_eventCntEOD;
    
  
  };
  
}
#endif // _TRBEventDecoder
