/*
  Copyright (C) 2019-2025 CERN for the benefit of the FASER collaboration
*/
 
/** \file safePhaseDetection.cxx
 * Standalone program to determine the LED (RX) CLK finePhase settings for modules connected on a TRB
 * for a specific input clk phase.
 * Outputs the scan result to terminal and a json file of maximally safe phase settings
 * for the LED CLK signals for given input CLK settings.
 * CLK0/LED0 and CLK1/LED1 control SCT modules 0-3 and modules 4-7 of a plane, respectively.
 */

#include "../../TrackerReadout/ConfigurationHandling.h"
#include "../../TrackerReadout/TRBAccess.h"
#include <iostream>
#include <iomanip>
#include <cstring> // strcmp
#include <unistd.h>
#include <getopt.h>
#include <boost/asio/ip/host_name.hpp> // for getting server host name
#include <nlohmann/json.hpp>

using namespace FASER;
using json = nlohmann::json;

const unsigned kARGCNT=10;

void usage(){
  std::cout<<"Usage: safePhaseDetection -b <trb_board_id> -i <trb_board_ip> -c <clk_fine_phase> <out_file.json>\n"
           <<"  -b board ID: board ID of TRB (range 0-15)\n"
           <<"  -i board IP: board IP address of TRB (usually faser-trb-<board_id>)\n"
           <<"  --clk0 clk 0 fine phase: The fine phase of the clock 0 input at which the LED0 safe phase should be searched for.\n"
           <<"  --clk1 clk 1 fine phase: The fine phase of the clock 1 input at which the LED1 safe phase should be searched for.\n"
           <<"  out_file.json: The output file for the scan results (last argument).\n"
           <<" All arguments are required."<<std::endl;
}

//------------------------------------------------------
int main(int argc, char *argv[]){

  const int N_STEPS = 64;
  uint8_t moduleMask = 0xFF;

  int boardID(-1);
  std::string boardIP("xxx");
  int finePhaseClk0(99);
  int finePhaseClk1(99);
  int opt;
  int opt_idx;
  static struct option long_options[] = {
    {"clk0", required_argument, 0, 0},
    {"clk1", required_argument, 0, 0},
    {nullptr, no_argument, nullptr, 0}
  };

  while ( (opt = getopt_long(argc, argv, "b:i:h", long_options, &opt_idx)) != -1 ) {
    switch (opt) {
      case 'h':
        usage();
        return 0;
      case 'b':
        boardID = std::stoi(optarg);
        break;
      case 'i':
        boardIP = optarg;
        break;
      case 0:
        if (long_options[opt_idx].flag != 0)
          break;
        if (strcmp(long_options[opt_idx].name,"clk0") == 0)
          finePhaseClk0 = atoi(optarg);
        if (strcmp(long_options[opt_idx].name,"clk1") == 0)
          finePhaseClk1 = atoi(optarg);
        break;
      case ':':
        ERROR("Missing value for some of parameters : "<<optopt);
        usage();
        return 1;
      case '?':  // unknown option...
        ERROR("Unknown input arguments detected!");
        usage();
        return 1;
    }
  }

  if (optind >= argc || argc < kARGCNT){
    ERROR("Too few arguments given!");
    usage();
    return 1;
  }

  std::string outFileName(argv[optind]);
  std::ofstream outFile(outFileName);
  if (!outFile.is_open()){
    std::cout << "ERROR: can't open file "<<outFileName<<std::endl;
    return 1;
  }

  // arguments check
  // sensible board ID and IP already checked by GPIOBase
  // check clk fine phase here, as only issues warning in GPIODrivers
  for (int clk_phase : {finePhaseClk0,finePhaseClk1}){
    if (clk_phase < 0 || clk_phase > 63){
      ERROR("Clock fine phase "<<clk_phase<<" out of range! Should be between 0 and 63.")
      return 1;
    }
  }

  json scan_output;
  scan_output["BoardID"] = boardID;
  scan_output["BoardIP"] = boardIP;
  scan_output["FinePhaseClk0"] = finePhaseClk0;
  scan_output["FinePhaseClk1"] = finePhaseClk1;

  const auto host_name = boost::asio::ip::host_name();
  INFO("Found host name "<<host_name);
  INFO("Establishing connection with TRB using board IP address "<<boardIP<<" and board ID "<<boardID);
  INFO("Using module mask 0x"<<std::hex<<static_cast<int>(moduleMask)<<std::dec);

  TRBAccess * trb = new TRBAccess(host_name, host_name, boardID, boardIP, false);

  std::cout << "====================================================================" << std::endl << std::endl;
  std::cout << "MAKE SURE THAT:" << std::endl << std::endl;
  std::cout << " 1.- At least one module from each module group 0-3 and 4-7 is connected and powered." << std::endl;
  std::cout << " 2.- The modules have been power-cycled." << std::endl << std::endl;
  std::cout << "====================================================================" << std::endl;

  std::cout<< " STARTING FINE PHASE SCAN " << std::endl;
  std::cout<< " Scanning at clock 0 fine phase " << std::dec << finePhaseClk0 << ", clock 1 fine phase " << finePhaseClk1 << std::endl;

  int v0[N_STEPS];
  int v1[N_STEPS];
  for(int i=0; i<N_STEPS; i++){
    v0[i]=-1;
    v1[i]=-1;
  }

  // make sure TRB locked on internal clock
  uint16_t status;
  trb->SetDirectParam(0); // disable all: ext CLK selection, L1A enable and BC enable.
  trb->ReadStatus(status);
  if (status & FASER::TRBStatusParameters::STATUS_TLBCLKSEL){
         std::cout<<"ERROR: TRB running on TLB clock! Returning..."<<std::endl;
  } else std::cout<<"TRB running on local clock - Can proceed with scan."<<std::endl;

  PhaseReg *phaseReg = trb->GetPhaseConfig();
  
  phaseReg->SetFinePhase_Clk0(finePhaseClk0);
  phaseReg->SetFinePhase_Clk1(finePhaseClk1);
  trb->WritePhaseConfigReg();
  trb->ApplyPhaseConfig();

  usleep(1e5);
  
  for(unsigned int step=0; step<N_STEPS; step++){
    phaseReg->SetFinePhase_Led0(step);
    phaseReg->SetFinePhase_Led1(step);
    trb->WritePhaseConfigReg();
    trb->ApplyPhaseConfig();

    usleep(1e5);

    trb->ReadStatus(status);
    v0[step] = int(status & TRBStatusParameters::STATUS_SAFEPHASEDET0) != 0 ? 1 : 0;
    v1[step] = int(status & TRBStatusParameters::STATUS_SAFEPHASEDET1) != 0 ? 1 : 0;
  }


  std::cout << "----------------" << std::endl;
  std::cout << "Results for CLK0/1 fine phase "<<finePhaseClk0<<"/"<<finePhaseClk1<<": " << std::endl;
  unsigned int idx0(0), idxbef0(0), transition0(0);
  unsigned int idx1(0), idxbef1(0), transition1(0);
  for(unsigned int step=0; step<64; step++){
    idx0 = v0[step];
    idx1 = v1[step];
    
    if(step >= 1){
      idxbef0 = v0[step-1];
      if(idx0 != idxbef0){
        transition0=step-1;
      }
      idxbef1 = v1[step-1];
      if(idx1 != idxbef1){
        transition1=step-1;
      }
    }

    std::cout << " step " << std::setfill('0') << std::setw(2) << std::dec << step << " =>";    
    std::cout << " SafePhaseDetect [0] = [" << v0[step] <<  "] ;";
    std::cout << " SafePhaseDetect [1] = [" << v1[step] <<  "]";
    std::cout << std::endl;
  }

  std::cout << "Transition for Led0 at : " << transition0 << std::endl;
  std::cout << "Transition for Led1 at : " << transition1 << std::endl;

  int finePhaseLed0 = (transition0 + 32) % 64 ;
  int finePhaseLed1 = (transition1 + 32) % 64 ;

  std::cout << std::endl;
  std::cout << "---------------------------------------------" << std::endl;
  std::cout << "Optimal finePhase led 0 for input CLK finePhase "<<finePhaseClk0<<" = " << finePhaseLed0 << std::endl;
  std::cout << "---------------------------------------------" << std::endl;
  std::cout << "---------------------------------------------" << std::endl;
  std::cout << "Optimal finePhase led 1 for input CLK finePhase "<<finePhaseClk1<<" = " << finePhaseLed1 << std::endl;
  std::cout << "---------------------------------------------" << std::endl;

  std::cout << std::endl << "Bye ! " << std::endl;

  scan_output["FinePhaseLed0"] = finePhaseLed0;
  scan_output["FinePhaseLed1"] = finePhaseLed1;
  INFO("Dumping results to "<<outFileName);
  outFile<<scan_output<<std::endl;
}
