/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 
#include <iostream>
#include <string>
#include <bitset>
#include <unistd.h>
#include "../../../GPIOBase/GPIOBase/udpInterface.h"
#include "../../../GPIOBase/GPIOBase/GPIOBaseClass.h"
#include "../../TrackerReadout/TRBAccess.h"
#include "../../TrackerReadout/TRBEventDecoder.h"
#include "../../TrackerReadout/TRB_ConfigRegisters.h"

using namespace FASER;
using namespace std;


void TestSockets(std::string SCIP, std::string DAQIP, int l_boardID, std::string boardIP)
{
  std::cout << std::endl << "---------------Testing UDP interface standalone (connect + transfer UDP packet)---------------" << std::endl;
  std::cout << "Connecting to GPIO board with IP " << boardIP << std::endl;

  CommunicationInterface *udp = new udpInterface(SCIP, DAQIP, boardIP);

  try {
    udp->Connect();
    std::cout << "\nIf no errors appeared, test was successful." << std::endl;
    } 
  catch (...) {
    std::cout << "Could not open connection to GPIO. Good luck next time"<<std::endl;
    return;
    }

  delete udp;
}

void TestConnection(std::string SCIP, std::string DAQIP, int l_boardID, std::string boardIP)
{
  std::cout << "-------------Initializing TRBAccess using UDP interface--------------" << std::endl;
  TRBAccess *trb = new TRBAccess(SCIP, DAQIP, l_boardID, boardIP);

  cout << "\nIf no errors appeared, test was successful." << std::endl;
  delete trb;
}

void TestConfiguration(std::string SCIP, std::string DAQIP, int l_boardID, std::string boardIP)
{  
  std::cout << "-------------Initializing TRBAccess using UDP interface--------------" << std::endl; 
  TRBAccess *trb = new TRBAccess(SCIP, DAQIP, l_boardID, boardIP);
  uint8_t mask;

  std::cout << "------------Sending some testing configuration-------------------" << std::endl;
  trb->GetConfig()->Set_Module_L1En(0x0);  
  trb->GetConfig()->Set_Global_L2SoftL1AEn(true);
  trb->WriteConfigReg();
  
  cout << "\nIf no errors appeared, test was successful." << std::endl;
  delete trb;
}

void TestDataTaking(std::string SCIP, std::string DAQIP, int NumberOfTriggers, int l_boardID, std::string boardIP)
{
  std::cout << "-------------Initializing TRBAccess using UDP interface--------------" << std::endl;
  auto ed = new TRBEventDecoder();
  TRBAccess *trb = new TRBAccess(SCIP, DAQIP, l_boardID, boardIP);
  unsigned short L1ID, L1ID_previous, BCID, BCID_previous;
  std::vector<std::vector<uint32_t>> vector_of_raw_events;
  uint8_t mask; 
  bool printEvents;

  printEvents = false;

  std::cout << "Enter module mask:" << std::endl;
  std::cin >> mask;

  trb->GetConfig()->Set_Module_L1En(mask);
  trb->GetConfig()->Set_Global_L2SoftL1AEn(true);
  trb->GetConfig()->Set_Module_ClkCmdSelect(0x0);
  trb->GetConfig()->Set_Module_LedRXEn(mask);
  trb->GetConfig()->Set_Module_LedxRXEn(mask);
  trb->GetConfig()->Set_Global_RxTimeoutDisable(true);
  trb->GetConfig()->Set_Global_L1TimeoutDisable(false);
  trb->GetConfig()->Set_Global_Overflow(4095);
  trb->WriteConfigReg();
  trb->GetPhaseConfig()->SetFinePhase_Clk0(0);
  trb->GetPhaseConfig()->SetFinePhase_Led0(55);
  trb->GetPhaseConfig()->SetFinePhase_Clk1(0);
  trb->GetPhaseConfig()->SetFinePhase_Led1(55);
  trb->WritePhaseConfigReg();
  trb->ApplyPhaseConfig();
  trb->PrintStatus();
 
  trb->SetupStorageStream("RawData.daq");
  trb->SetDebug(false);   
  trb->WriteConfigReg();

  std::cout << std::endl << "--------------Starting readout------------"<<std::endl;
  trb->StartReadout();
  usleep(100);

  auto start = std::chrono::high_resolution_clock::now();
  auto stop = start;
  std::cout << "Generating " << std::dec<<NumberOfTriggers << " L1As:" << std::endl;
  for (int i = 0; i < NumberOfTriggers; i++){
    trb->GenerateL1A(mask);
    if ((i%1000) == 0){
      stop = std::chrono::high_resolution_clock::now();
      auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
      std::cout << "triggered events: "<<std::dec<<i<< " at "<<1000000.0/float(duration.count()) <<" kHz"<< std::endl;
      start = stop;
    }
  //  usleep(500);
  }
  usleep(100000);
  std::cout << "--------------Stopping readout------------" << std::endl;
  trb->StopReadout();
  usleep(100000);
  vector_of_raw_events = trb->GetTRBEventData();

  if (printEvents){
    for(std::vector<uint32_t> event : vector_of_raw_events){
    std::cout << "Printing new event raw binary data:" << std::endl;
      for (uint32_t word : event){
        std::bitset<32> y(word);
        std::cout << y << std::endl;
      }
    }
  }
  else{
    std::cout << "Printing out received data has been set to false." << std::endl;
  }
  start = std::chrono::high_resolution_clock::now();
  ed->LoadTRBEventData(vector_of_raw_events); // This loads all events into the event decoder. Could also be done after every event, but is slower.
  stop = std::chrono::high_resolution_clock::now();
  auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
  std::cout << "Loading and decoding fo events took: "<< float(duration.count())/1000000.0 <<" seconds"<<std::endl;
  auto events = ed->GetEvents();
  if (events.size() != NumberOfTriggers)
  {
    std::cout << "ERROR: Number of events does not correspond to number of triggers generated." << std::endl;
    std::cout << "triggers generated: " << NumberOfTriggers << ", events received: " << events.size()<< std::endl;
    /*
    for (auto event : vector_of_raw_events){
      for (auto word : event) {
        std::cout << " 0x"<<std::hex<<word;
      }
      std::cout << endl;
    }
    */
  }
  std::cout << "TRB Events found = "<<events.size()<<std::endl;
 
  L1ID_previous = events[0]->GetL1ID();
  BCID_previous = events[0]->GetBCID();

  for (int i = 1; i < events.size(); i++){
    L1ID = events[i]->GetL1ID();
    BCID = events[i]->GetBCID();
    
    if (L1ID != L1ID_previous + 1){
      std::cout << "ERROR: L1ID did not increase by 1. Two consecutive L1IDs are: " << L1ID_previous << " and " << L1ID << std::endl;
      if (events[i-1]->GetErrors().size() != 0){
        std::cout << "Event " << i-1 << " has following errors: " << std::endl;
        for (auto error : events[i-1]->GetErrors()){
          std::cout << ed->TRBErrorToString(error) << std::endl;
        } 
      }
      if (events[i]->GetErrors().size() != 0){
        std::cout << "Event " << i << " has following errors: " << std::endl;
        for (auto error : events[i]->GetErrors()){
          std::cout << ed->TRBErrorToString(error) << std::endl;
        }
        std::cout << std::endl; 
      }
    }
    //TODO Can we do some check on BCID - it is a random number with iternal trigger
    L1ID_previous = L1ID;
    BCID_previous = BCID; 
  }
  
  cout << "\nIf no errors appeared, test was successful." << std::endl;
  delete ed;
  delete trb;       
}


void TestFunctions(std::string SCIP, std::string DAQIP, int NumberOfAttempts, int l_boardID, std::string boardIP){
  std::cout << "-------------Initializing TRBAccess using UDP interface--------------" << std::endl;
  TRBAccess *trb = new TRBAccess(SCIP, DAQIP, l_boardID, boardIP);
  trb->SetupStorageStream("RawData.daq");
  trb->SetDebug(false);   
 
  cout << "----------Testing GetFirmwareVersion------------" << endl;
  static const uint16_t argFPGAtop       = 0x0000;
  static const uint16_t argFPGAdecoder   = 0x0001;
  static const uint16_t argHardware      = 0x0002;
  static const uint16_t argProductID     = 0x0003;
  static int gpio_status(0);
  
  unsigned int FPGAmajor = 0, FPGAminor = 0, FPGAencoder = 0, hardware = 0, product = 0;
  uint16_t answArg = 0;
  
  gpio_status = TRBAccess::GPIOCheck(trb, &TRBAccess::GetSingleFirmwareVersion, argFPGAtop, answArg);
  if (gpio_status){
    std::cout << "ERROR: GetSingleFirmwareVersion(argFPGAtop) failed"<<std::endl;
    return;
    }
      
  uint16_t m_FPGA_version = answArg;        
  FPGAminor = 0x000f & answArg;
  FPGAmajor = (0xfff0 & answArg) >> 4;
               
  gpio_status = TRBAccess::GPIOCheck(trb, &TRBAccess::GetSingleFirmwareVersion, argFPGAdecoder, answArg);
  if (gpio_status){
    std::cout << "ERROR: GetSingleFirmwareVersion(argFPGAdecoder) failed"<<std::endl;
    return;
  }
  FPGAencoder = answArg;
  gpio_status = TRBAccess::GPIOCheck(trb, &TRBAccess::GetSingleFirmwareVersion, argHardware, answArg);
  if (gpio_status){
    std::cout << "ERROR: GetSingleFirmwareVersion(argHardware) failed"<<std::endl;
    return;
  }
  hardware = answArg;
   
  gpio_status = TRBAccess::GPIOCheck(trb, &TRBAccess::GetSingleFirmwareVersion, argProductID, answArg);
  if (gpio_status){
    std::cout << "ERROR: GetSingleFirmwareVersion(argProductID) failed"<<std::endl;
    return;
  }
  product = answArg;
    
  cout << "   Firmware versions: "<<endl;
  cout << "   FPGA:       "<<FPGAmajor<<"."<<FPGAminor<<" = 0x"<<std::hex<<m_FPGA_version<<endl;
  cout << "   Encoder:    "<<FPGAencoder<<endl;
  cout << "   Hardware:   "<<hardware<<endl;
  cout << "   Product ID: "<<product<<endl  <<endl;
                                                           
  for (int i = 0; i <= NumberOfAttempts; i++){
    trb->GetSingleFirmwareVersion(argFPGAtop, answArg);
    if ((FPGAminor != (0x000f & answArg)) || (FPGAmajor != ((0xfff0 & answArg) >> 4))){
      cout << "Get Firmware Version failed in attempt: " << i << endl;
      return;
    }
    trb->GetSingleFirmwareVersion(argFPGAdecoder, answArg);
    if (FPGAencoder != answArg){
      cout << "Get Firmware Version failed in attempt: " << i << endl;
      return;
    } 
    trb->GetSingleFirmwareVersion(argHardware, answArg);
    if (hardware != answArg){
      cout << "Get Firmware Version failed in attempt: " << i << endl;
      return;
    }
    trb->GetSingleFirmwareVersion(argProductID, answArg);
    if (product != answArg){
      cout << "Get Firmware Version failed in attempt: " << i << endl;
      return;
    }
  } 


  cout <<  "----------Testing ReadBoardID------------" << endl;
  uint16_t boardID = 0;
  uint8_t cmd = GPIOCmdID::GET_BOARD_ID;
  if (TRBAccess::GPIOCheck(trb, &TRBAccess::SendAndRetrieve, cmd, 0, &answArg)){ 
    cout << "Read BoardID failed" << endl;
    return;
  }
  
  std::cout << "GPIO board ID = 0x"<<std::hex<<boardID<<std::endl;
  boardID = answArg;

  for( int i = 0; i <= NumberOfAttempts; i++){
    trb->SendAndRetrieve(GPIOCmdID::GET_BOARD_ID, 0, &answArg);
    if (boardID != answArg){
      cout << "Read BoardID failed in attempt: " << i << endl;
      return;
    }
  }   

  cout <<  "\n----------Testing User Set/Get configuration------------" << endl;
  unsigned int Set_bcid = 0;
  unsigned int Set_L1A = 0;
  unsigned int Answ_bcid = 0;
  unsigned int Answ_L1A = 0;
  vector<uint16_t> phase_cfg;
  vector<uint16_t> sct_cfg;
  vector<uint16_t> cfg_reg;
  vector<uint16_t> tmp;

  for (int i = 0; i <= NumberOfAttempts; i++){
    if (i % 50 == 0){
      std::cout << "Running cycle: "<<std::dec<<i<<std::endl;
    }
    Answ_L1A = 42;  //this is to make sure that we really read the value from TRB and we are not only getting what we had in varaible before
    Answ_bcid = 42; //this is to make sure that we really read the value from TRB and we are not only getting what we had in varaible before
    
    trb->GetPhaseConfig()->SetFinePhase_Clk0(i % 64);
    trb->GetPhaseConfig()->SetFinePhase_Clk1(i % 64);
    trb->GetPhaseConfig()->SetFinePhase_Led0(i % 64);
    trb->GetPhaseConfig()->SetFinePhase_Led1(i % 64);
    phase_cfg = trb->GetPhaseConfig()->GetPayloadVector();
    trb->WritePhaseConfigReg();
    trb->ApplyPhaseConfig();
    
    trb->GetConfig()->Set_Module_L1En(i % 256);
    trb->GetConfig()->Set_Global_L2SoftL1AEn((i+1) % 2);
    trb->GetConfig()->Set_Module_ClkCmdSelect(i % 256);
    trb->GetConfig()->Set_Module_LedRXEn(i % 256);
    trb->GetConfig()->Set_Module_LedxRXEn(i % 256);
    trb->GetConfig()->Set_Global_RxTimeoutDisable((i+1) % 2);
    trb->GetConfig()->Set_Global_L1TimeoutDisable(i % 2);
    trb->GetConfig()->Set_Global_Overflow(4095);
    cfg_reg = trb->GetConfig()->GetPayloadVector();
    trb->WriteConfigReg();
    
    trb->PresetSoftCounters(Set_bcid, Set_L1A);

    tmp = trb->ReadPhaseConfigReg();
    for (int j = 0; j < tmp.size(); j++){
      if (phase_cfg[j] != tmp[j]){
        cout << "Get/Set phase config failed in attempt: " << i << endl;
        cout << "Sent: " << endl;
        for (auto item : phase_cfg){
          cout << item << endl;
        }        
        cout << "But got: " << endl;
        for (auto item : tmp){
          cout << item << endl;
        }
        return;
      }
    }
 
    tmp = trb->ReadConfigReg(); 
    for (int j = 0; j < tmp.size(); j++){
      if (cfg_reg[j] != tmp[j]){
        cout << "Get/Set config reg failed in attempt: " << i << endl;
        cout << "Sent: " << endl;
        for (auto item : cfg_reg){
          cout << item << endl;
        }        
        cout << "But got: " << endl;
        for (auto item : tmp){
          cout << item << endl;
        }
        return;
      }
    }

    trb->ReadSoftCounters(Answ_bcid, Answ_L1A);
    if (Set_bcid != Answ_bcid || Set_L1A != Answ_L1A){
        cout << "Get/Set soft counters failed in attempt: " << i << endl;
        return;
     }
  }
 
  cout << "\nIf no errors appeared, test was successful." << endl; 
  delete trb;
}

void usage(){
    cout << "\nUsage: ethernetTestApp -b <trb_ip> -s <server_ip> -d <daq_ip>" << endl;
    cout << "   -b     Board ID of the TRB (this is in range 0-15)" << endl;
    cout << "   -i     Board IP host name" << endl;
    cout << "   -s     Server IP addres or host name" << endl;
    cout << "   -d     DAQ PC IP address or host name" << endl;
    cout << "\nNote: values of parameters -s and -d will be probably always the same." << endl;
    cout << "\nIn case of problems with this program contact Ondrej Theiner (ondrej.theiner@cern.ch)" << endl;
}

int main(int argc, char **argv)
{
  char choice;
  std::string l_SCIP, l_DAQIP, l_BoardIP;
  int opt, n, l_BoardID;
  bool s_param, d_param, b_param, i_param;

  s_param = false;
  d_param = false;
  b_param = false;
  i_param = false;

  if(argc<2){
    usage();
    return 0;
  }
  
  while ( (opt = getopt(argc, argv, "s:d:b:i:")) != -1 ) {
    switch ( opt ) {
    case 's':
      l_SCIP = optarg;
      s_param = true;  
      break;
    case 'd':
      l_DAQIP = optarg;
      d_param = true;
      break;
    case 'b':
      l_BoardID = atoi(optarg);   
      b_param = true;
      if (l_BoardID < 0 || l_BoardID > 15){
        cout << "ERROR: Board ID out of range." << endl;
        usage();
        return 0;
      } 
      break;
    case 'i':
      l_BoardIP = optarg;
      i_param = true;
      break;
    case ':':
      std::cout<<"ERROR: Missing value for some of parameters : "<<optopt<<std::endl;
      usage();
      return 0;
    case '?':  // unknown option...
      cout << "\nERROR: Unknown parameter. For allowed parameters see hint bellow." << endl;
      usage();
      return 0;
    }
  }

  if ((b_param || d_param || s_param || i_param) == false){
    cout << "ERROR: All three arguments have to be provided." << endl;
    usage();
    return 0;
  }

  cout << "\nUsing Server/client PC IP:    " << l_SCIP
       << "\n      DAQ PC IP:              " << l_DAQIP
       << "\n      Board IP:               " << l_BoardIP
       << "\n      Board ID:               " << l_BoardID << endl;
 
  choice = '0';
  std::cout << "\nSelect one of following options:" << std::endl;
  std::cout << "   0: Exit" <<
               "\n   1: Test socket creation" <<
               "\n   2: Test connection" <<
               "\n   3: Test configuration" << 
               "\n   4: Test DAQ" <<
               "\n   5: Test commands" << endl;
  std::cin >> choice;
  
  switch(choice){
    case '0':
      return 0;
      break;
    
    case '1':
      TestSockets(l_SCIP, l_DAQIP, l_BoardID, l_BoardIP);
      break;

    case '2':
      TestConnection(l_SCIP, l_DAQIP, l_BoardID, l_BoardIP);
      break;

    case '3':
       TestConfiguration(l_SCIP, l_DAQIP, l_BoardID, l_BoardIP);
       break;

    case '4':
      std::cout << "Enter number of repetitions for each test" << std::endl;
      std::cin >> n;
      TestDataTaking(l_SCIP, l_DAQIP, n, l_BoardID, l_BoardIP);
      break;

    case '5':
      std::cout << "Enter number of repetitions for each test" << std::endl;
      std::cin >> n;
      TestFunctions(l_SCIP, l_DAQIP, n, l_BoardID, l_BoardIP);
      break;

    default:
      std::cout << "Invalid option" << std::endl;
  }
}
