
if (APPLE)
   SET(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -Wno-c++11-extensions")
	include_directories(/usr/local/include/libusb-1.0/)
	link_directories(/usr/local/lib/)
elseif(UNIX)
   SET(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -pthread")
	include_directories(/usr/include/libusb-1.0/)
	link_directories(/usr/lib64/)
endif()


add_executable(CmdLineTest CmdLineTestProgram.cxx)
add_executable(TestEvntDecoding TestEvntDecoding.cxx)
add_executable(MenuTRB MenuTRB.cxx)
add_executable(ethernetTestApp ethernetTestApp.cxx)
add_executable(safePhaseDetection safePhaseDetection.cxx)

target_link_libraries(TestEvntDecoding TRBEventDecoder)

if (APPLE)
   target_link_libraries(CmdLineTest ConfigurationHandling TRBAccess TRBEventDecoder usbInterface usb-1.0.0)
   target_link_libraries(MenuTRB ConfigurationHandling TRBAccess TRBEventDecoder usbInterface usb-1.0.0)
   target_link_libraries(safePhaseDetection ConfigurationHandling TRBAccess TRBEventDecoder usbInterface usb-1.0.0)
   target_link_libraries(ethernetTestApp TRBAccess ConfigurationHandling TRBEventDecoder udpInterface usb-1.0.0)
else()
   target_link_libraries(CmdLineTest ConfigurationHandling TRBAccess TRBEventDecoder usbInterface usb-1.0)
   target_link_libraries(MenuTRB ConfigurationHandling TRBAccess TRBEventDecoder usbInterface usb-1.0)
   target_link_libraries(safePhaseDetection TRBAccess ConfigurationHandling TRBEventDecoder usbInterface usb-1.0)
   target_link_libraries(ethernetTestApp TRBAccess ConfigurationHandling TRBEventDecoder udpInterface usb-1.0)
   target_compile_options(ethernetTestApp PRIVATE -g)
endif()

