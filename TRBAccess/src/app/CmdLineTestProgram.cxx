/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 
#include "../../TrackerReadout/TRBAccess.h"
#include "GPIOBase/DummyInterface.h"
#include "../../TrackerReadout/ConfigurationHandling.h"
#include "../../TrackerReadout/ROR_Header.h"
#include "../../TrackerReadout/TRBEventDecoder.h"
#include <unistd.h>

#include <chrono>
#include <iostream>
#include <sstream>
using namespace FASER;

int finePhaseDelay=  55;

void CheckEventStructureWothoutModules()
{
  TRBAccess *trb = new TRBAccess(false, 0xff /* optionally specify board ID here instead of 0xff */);
  trb->SetDebug(false);
  
  // configure the TRB
  uint8_t moduleMask = 0x0; // disable all modules
  
  trb->GetConfig()->Set_Module_L1En(0x0); // disable hardware L1A for all modules
  trb->GetConfig()->Set_Module_ClkCmdSelect(0x0); // all modules use CMD_0
  trb->GetConfig()->Set_Module_LedRXEn(moduleMask); // enable led lines
  trb->GetConfig()->Set_Module_LedxRXEn(moduleMask); // enable ledx lines
  
  trb->GetConfig()->Set_Global_RxTimeoutDisable(true);  // default config as specified by Yannick
  trb->GetConfig()->Set_Global_L1TimeoutDisable(false); // default config as specified by Yannick
  trb->GetConfig()->Set_Global_L2SoftL1AEn(true);       // default config as specified by Yannick
  trb->GetConfig()->Set_Global_Overflow(4095);
  
  trb->WriteConfigReg(); // uplaod the config to the TRB
  
  /// Set the Fine phase adjustment this is only needed when running with modules
  trb->GetPhaseConfig()->SetFinePhase_Clk0(0);
  trb->GetPhaseConfig()->SetFinePhase_Led0(45);
  trb->GetPhaseConfig()->SetFinePhase_Clk1(0);
  trb->GetPhaseConfig()->SetFinePhase_Led1(45);
  trb->WritePhaseConfigReg();
  trb->ApplyPhaseConfig();
  
  // here we setup the output file to use for data storage
  std::stringstream outfileName;
  outfileName << "Test.daq";
  trb->SetupStorageStream(outfileName.str());
  
  
  // start readout
  trb->FIFOReset();
  trb->StartReadout();
  
  // send some L1As and monitor the L1A rate
  auto time_start = std::chrono::high_resolution_clock::now();
  auto time_end = std::chrono::high_resolution_clock::now();
  
  long int expectedL1A = 1000;
  for (unsigned i = 0; i < expectedL1A; i++){
    trb->GenerateL1A(moduleMask);
    
    if (i % 1000 == 0 && i != 0){
      time_end = std::chrono::high_resolution_clock::now();
      auto duration = std::chrono::duration_cast<std::chrono::microseconds>(time_end - time_start);
      std::cout << "L1A cnt: "<<i<<" Trigger Rate = "<<1.0e6/duration.count()<<" kHz"<<std::endl;
      time_start = std::chrono::high_resolution_clock::now();
    }
  }
  
  trb->StopReadout();
  
  ///
  /// decode events on the fly:
  /// Events will be only be recognised if a header and a trailer is found. Hence, if any of those should be missing the number of events will not correspond to the number of triggers sent.
  auto ed = new TRBEventDecoder();
  ed->LoadTRBEventData(trb->GetTRBEventData()); // This loads all events into the event decoder. Could also be done after every event, but is slower.
  
  auto evnts = ed->GetEvents();
  std::cout << "TRB Events foud = "<<evnts.size()<<std::endl;
  for (auto event : evnts){
    auto L1ID = event->GetL1ID();
    auto BCID = event->GetBCID();
    
  }
  delete ed;
}
                                               


/// Testing the TRB emulator
void TestEmulator(std::string inputFile)
{
  TRBAccess *trb = new TRBAccess(0, true /* turn on emulation */);
  trb->SetDebug(false);
  (static_cast<dummyInterface*>(trb->m_interface))->SetInputFile(inputFile); // set the file to read TRB events from
  trb->SetupStorageStream("TestEmulatorOutFile.daq");
  std::cout << "starting readout" << std::endl;
  trb->StartReadout(); // This starts the readout thread which should now read one event every time it calls PollData
  while(1){
    TRBEventDecoder *ed = new TRBEventDecoder();
    ed->SetEventInfoVerbosity(0);
    auto events = trb->GetTRBEventData();
    for (auto event : events){
      std::cout << "trb->GetTRBEventData() conaints [] = "<<std::dec<<events.size()<<" events with "<<event.size()<< " words"<<std::endl ;
      for (auto word : event){
        //std::cout << "   0x"<<std::hex<<word;
      }
      std::cout << std::endl;
      ed->LoadTRBEventData(event);
      auto trbEvents = ed->GetEvents();
      if (trbEvents.size() ==0) {usleep(1); continue;}
      std::cout << "Decoded "<<trbEvents.size()<<" Events"<<std::endl;
      for (auto trbEvent : trbEvents){
        if (trbEvent == nullptr){
          std::cout <<" -- Failed to decode event"<<std::endl;
        } else {
          std::cout << " -- Decoded event OK"<<std::endl;
        }
      }
    }
  }
  sleep(2);
  trb->StopReadout(); // this will terminate the readout thread once there is no more data to be read from the file.
  std::cout << "stopped readout" << std::endl;
  delete trb;
}



/** This is a minimal working example of how to retrieve and analyse TRB data on the fly  
 */
void OnlineDecodingThresholdScan()
{
  ///
  /// Setup the TRB readout board
  ///
  TRBAccess *trb = new TRBAccess(false);
  trb->SetDebug(false);
  
  uint8_t moduleMask = 0xF; // enable a single module. In this case module 3
  
  trb->GetConfig()->Set_Module_L1En(0x0); // disable hardware L1A for all modules
  trb->GetConfig()->Set_Module_ClkCmdSelect(0x0); // all modules use CMD_0
  trb->GetConfig()->Set_Module_LedRXEn(moduleMask); // enable led lines
  trb->GetConfig()->Set_Module_LedxRXEn(moduleMask); // enable ledx lines
  
  trb->GetConfig()->Set_Global_RxTimeoutDisable(true);  // default config as specified by Yannick
  trb->GetConfig()->Set_Global_L1TimeoutDisable(false); // default config as specified by Yannick
  trb->GetConfig()->Set_Global_L2SoftL1AEn(true);       // default config as specified by Yannick
  trb->GetConfig()->Set_Global_Overflow(4095);
  
  trb->WriteConfigReg(); // uplaod the config to the TRB
  
  /// Set the Fine phase adjustment
  trb->GetPhaseConfig()->SetFinePhase_Clk0(0);
  trb->GetPhaseConfig()->SetFinePhase_Led0(45);
  trb->GetPhaseConfig()->SetFinePhase_Clk1(0);
  trb->GetPhaseConfig()->SetFinePhase_Led1(45);
  trb->WritePhaseConfigReg();
  trb->ApplyPhaseConfig();
  
  std::vector<uint16_t> configIs  = trb->ReadConfigReg();
  ConfigReg actualCfg;
  actualCfg.FillFromPayload(configIs);
  actualCfg.Print();
  
  // here we setup the output file to use for data storage
  std::stringstream outfileName;
  outfileName << "SimpleScan.daq";
  trb->SetupStorageStream(outfileName.str());
  
  ///
  /// Now configure the module with the configuration to start from.
  /// In this case the default config from the ConfigHandlerSCT class is used.
  /// Could also be loaded from a JSON file instead.
  ///
  std::cout << "Loading configuration to modules 0x"<<std::hex<<moduleMask <<std::endl;
  ConfigHandlerSCT *cfg = new ConfigHandlerSCT();
  // cfg->ReadFromFile(cfgFileName);
  cfg->SetReadoutMode(1); // on top of default config set ReadoutMode to X1X => state (default for physics)
  cfg->SetEdgeDetection(0);
  
  int calMode = 0;
  cfg->SetCalMode(calMode);
  trb->ConfigureSCTModule(cfg, moduleMask); //  explicitely specify channel
  std::cout << "   Done."<<std::endl;
  
  ScanStepConfig *scanConfig = new ScanStepConfig; // This object is used to store the scan configuration in every scan step
  for (unsigned int calMode = 0; calMode < 4; calMode++){
    
    std::cout << std::endl<<"Mask Step: "<< calMode<<std::endl<<std::endl;
    // write Stripmask to module and store stripMask steip in scan config
    trb->SCT_WriteStripmask(moduleMask, calMode);
    scanConfig->CalMode = calMode;
    cfg->SetCalMode(calMode);
    trb->ConfigureSCTModule(cfg, moduleMask);
    
    // Set the strobe delay
    trb->GetSCTSlowCommandBuffer()->SetStrobeDelay(0x3f, 15);
    trb->WriteSCTSlowCommandBuffer();
    trb->SendSCTSlowCommandBuffer(moduleMask);
    
    // This is the loop over the threshold setting
    for (unsigned int thr = 0; thr <= 100; thr += 1){
      int expectedL1A = 50; // here you can set how many triggers are sent in every scan step
      std::cout << "Threshold scan loop: "<<std::dec<<thr<<std::endl;
      
      // set configuration
      scanConfig->Threshold = thr;
      scanConfig->ExpectedL1A = expectedL1A;
      trb->SaveScanStepConfig(*scanConfig); // this writes the scan step config to the data file
      
      uint16_t thrValue = thr<<8;   // threshold is stored in the 8 MSB
      thrValue |= 0x24;             // calibration charge to be injected is set in the 8 LSB
      //upload threshold register to module
      trb->GetSCTSlowCommandBuffer()->SetThreshold(0x3f, thrValue);
      trb->WriteSCTSlowCommandBuffer();
      trb->SendSCTSlowCommandBuffer(moduleMask);
      
      // start data taking
      trb->FIFOReset();
      trb->SCT_EnableDataTaking(moduleMask);
      trb->GenerateSoftReset(moduleMask);
      
      // start readout
      trb->StartReadout();

      // send some L1As and monitor the L1A rate
      auto time_start = std::chrono::high_resolution_clock::now();
      auto time_end = std::chrono::high_resolution_clock::now();
      for (unsigned i = 0; i < expectedL1A; i++){
        // send a calibration pulse followed by an L1A after 130 BCs
        trb->SCT_CalibrationPulse(moduleMask, 130);
        // Without charge injection: use GenerateL1A();
        //trb->GenerateL1A(moduleMask);
        
        // Adding some sleep after every L1A, could be tuned
        //usleep(600);
        if (i % 10000 == 0 && i != 0){
          time_end = std::chrono::high_resolution_clock::now();
          auto duration = std::chrono::duration_cast<std::chrono::microseconds>(time_end - time_start);
          std::cout << "L1A cnt: "<<i<<" Trigger Rate = "<<10.0e6/duration.count()<<" kHz"<<std::endl;
          time_start = std::chrono::high_resolution_clock::now();
        }
      }
      
      // stop data taking
      //usleep(10000);
      trb->StopReadout();
      
      ///
      /// decode events on the fly:
      ///
      auto ed = new TRBEventDecoder();
      ed->LoadTRBEventData(trb->GetTRBEventData());
      
      auto evnts = ed->GetEvents();
      std::cout << "TRB Events foud = "<<evnts.size()<<std::endl;
      std::vector<int> hitsPerModule;
      hitsPerModule.resize(8);
      for (auto evnt : evnts){ // loop over all events
        for (unsigned int module = 0; module < 4; module++){
          auto sctModule = evnt->GetModule(0);
          if (sctModule != nullptr){
            hitsPerModule[module] += sctModule->GetNHits();
          }
        }
      }
      std::cout << " Hits per module: ";
      for (unsigned int i = 0; i < 4; i++) std::cout << hitsPerModule[i]<<" ";
      std::cout << std::endl;
      delete ed;
    }
  }
  
  delete cfg;
  delete trb;
}

void TestSCTConfigHandling(){
  ConfigHandlerSCT *cfgHandler = new ConfigHandlerSCT();
  //cfgHandler->SetStrobeDelay(0x20, 5);
  cfgHandler->WriteToFile("TemplateModuleCfg.json");
}

void TestTRBConfigHandling(){
  TRBAccess *trb = new TRBAccess(true);
  
  int module = 0;
  
  trb->GetConfig()->Set_Module_L1En(module, true);
  trb->GetConfig()->Set_Module_ClkCmdSelect(module, false); // switch between CMD_0 and CMD_1 lines
  trb->GetConfig()->Set_Module_LedxRXEn(module, true, true); // switch between led and ledx lines
  
  
  std::vector<uint16_t> configSet = trb->GetConfig()->GetPayloadVector();
  
  ConfigReg actualCfg;
  actualCfg.FillFromPayload(configSet);
  
  std::cout << "Is Config:"<<std::endl;
  actualCfg.Print();
  std::cout << "Set Config:"<<std::endl;
  trb->GetConfig()->Print();
  std::cout << "   Done."<<std::endl;
  
}

void ConfigurationReadback(std::string cfgFileName, bool doCfg, uint8_t moduleMask, bool debug)
{
  unsigned int module = 0;
  
  std::cout << "Initialising TRB access "<<std::endl;
  TRBAccess *trb = new TRBAccess("10.11.65.7", "10.11.65.7", 0, "faser-trb-14", false);
  trb->SetDebug(debug);
  trb->SetupStorageStream("ConfigReadback.daq");
  
  std::cout << "TRB Firmware versions: "<<std::endl;
  
  //uint8_t moduleMask = 0xF; // enable a single module. In this case module 3
  

  trb->GetConfig()->Set_Module_L1En(0x0); // disable hardware L1A for all modules
  trb->GetConfig()->Set_Module_ClkCmdSelect(0x0); // all modules use CMD_0
  trb->GetConfig()->Set_Module_LedRXEn(moduleMask); // enable led lines
  trb->GetConfig()->Set_Module_LedxRXEn(moduleMask); // enable ledx lines
  
  trb->GetConfig()->Set_Global_RxTimeoutDisable(true);  // default config as specified by Yannick
  trb->GetConfig()->Set_Global_L1TimeoutDisable(false); // default config as specified by Yannick
  trb->GetConfig()->Set_Global_L2SoftL1AEn(true);       // default config as specified by Yannick
  trb->GetConfig()->Set_Global_Overflow(4095);
  
  trb->WriteConfigReg(); // uplaod the config to the TRB
  
  /// Set the Fine phase adjustment
  trb->GetPhaseConfig()->SetFinePhase_Clk0(0);
  trb->GetPhaseConfig()->SetFinePhase_Led0(55);
  trb->GetPhaseConfig()->SetFinePhase_Clk1(0);
  trb->GetPhaseConfig()->SetFinePhase_Led1(55);
  trb->WritePhaseConfigReg();
  trb->ApplyPhaseConfig();
  
  // Readback config from TRB
  std::vector<uint16_t> configSet = trb->GetConfig()->GetPayloadVector();
  std::vector<uint16_t> configIs  = trb->ReadConfigReg();
  // for (unsigned int i = 0; i < configSet.size(); i++) {
  //   std::cout << "TRB config = 0x"<<std::hex<<configIs[i]<<std::endl;
  //   std::cout << "Exppected  = 0x"<<std::hex<<configSet[i]<<std::endl<<std::endl;
  // }
  
  ConfigReg actualCfg;
  actualCfg.FillFromPayload(configIs);
  
  if (actualCfg == *(trb->GetConfig()) ){
    std::cout << "config is: "<<std::endl;
    actualCfg.Print();
  } else {
    std::cout << "WARNING: hardware shows different config than expected:"<<std::endl;
    
    std::cout << "Is Config:"<<std::endl;
    actualCfg.Print();
    std::cout << "Set Config:"<<std::endl;
    trb->GetConfig()->Print();
    std::cout << "   Done."<<std::endl;
  }
  
  
  // switch between CMD / CLK lines
  /*
   for (unsigned int i = 0; i < 100; i++){
   std::cout << "Set CMD line 1 active "<<std::endl;
   trb->GetConfig()->Set_Module_ClkCmdSelect(module, true); // switch between CMD_0 and CMD_1 lines
   trb->WriteConfigReg();
   for (unsigned int i = 0; i < 10; i++){
   std::cout << " issuing L1A "<<i<<std::endl;
   trb->GenerateL1A(0x1); // generate L1A for module 0
   sleep(1);
   }
   
   std::cout << "Set CMD line 0 active "<<std::endl;
   trb->GetConfig()->Set_Module_ClkCmdSelect(module, false); // switch between CMD_0 and CMD_1 lines
   trb->WriteConfigReg();
   
   for (unsigned int i = 0; i < 10; i++){
   std::cout << " issuing L1A "<<i<<std::endl;
   trb->GenerateBCReset(0x1); // generate L1A for module 0
   sleep(1);
   }
   }
   */
  
  if (doCfg){
    
    std::cout << "Loading configuration to modules 0x"<<std::hex<<(int)moduleMask <<std::endl;
    ConfigHandlerSCT *cfg = new ConfigHandlerSCT();
    // cfg->ReadFromFile(cfgFileName);
    cfg->SetReadoutMode(1); // on top of default config set ReadoutMode to X1X => state (default for physics)
    cfg->SetEdgeDetection(0);

    int calMode = 0;
    cfg->SetCalMode(calMode);
    cfg->SetBiasDac(0x1819);
    
    // configure module
    std::cout << "Configuring module "<<module<<std::endl;
    trb->ConfigureSCTModule(cfg, moduleMask ); //  explicitely specify channel
    std::cout << "   Done."<<std::endl;
  }
  
  std::cout << "Reading back configuration "<<std::endl;
  
  
  // readback config: If not placed in Data Taking mode the SCT chips are in Send ID mode, sending their ID and config upon an L1A
  
  
  trb->FIFOReset();
  
  //trb->SCT_EnableDataTaking(moduleMask);
  std::cout << "Sarting readout "<<std::endl;
  
  trb->StartReadout();
  for (int i = 0; i < 10000; i++){
    usleep(500);
    //std::cout << "Generating L1A for modules "<<std::hex<<(int)moduleMask<<std::endl;
    trb->GenerateL1A(moduleMask);
  }
  std::cout << "Stopping readout"<<std::endl;
  trb->StopReadout();
  std::cout << "   Done."<<std::endl;
  //delete cfg;
  delete trb;
  
}


void PrintEventSummary(TRBEventDecoder *ed, int module)
{
  int BCIDMismatches = 0;
  int IncompleteEvents = 0;
  int Errors = 0;
  std::vector<int> hitsPerChip;
  hitsPerChip.resize(13);
  
  auto evts = ed->GetEvents();
  std::cout << std::endl<<"Event recorded: "<<std::dec<<evts.size()<<std::endl;
  for (auto evt : evts){
    if (evt->GetModule(module)->BCIDMismatch()) {BCIDMismatches++;}
    if (evt->GetModule(module)->MissingData()) {IncompleteEvents++;}
    if (evt->GetModule(module)->HasError()) {Errors++;}
    int id = 0;
    for (unsigned int chip = 0x20; chip <= 0x2d; chip++) {
      if (chip == 0x26) chip = 0x28;
      hitsPerChip[id] += evt->GetModule(module)->GetNHits(chip);
      id++;
    }
    hitsPerChip.back() += evt->GetModule(module)->GetNHits(0);
    
    if (evt->GetL1ID() < 0){ // print details on first n L1As
      std::cout << "L1ID = "<< evt->GetL1ID()<< " Hits per Chip : ";
      for (unsigned int chip = 0x20; chip <= 0x2d; chip++) {
        if (chip == 0x26) chip = 0x28;
        std::cout << " " << evt->GetModule(module)->GetNHits(chip);
      }
      std::cout << std::endl;
    }
  }
  std::cout << "Total hits per chip: ";
  for (unsigned int chip = 0; chip < 12; chip++) {
    std::cout << ", " << std::dec<<hitsPerChip[chip];
    hitsPerChip[chip] = 0;
  }
  std::cout << " Unkown chips occurences: " << hitsPerChip[12]<<std::endl;
  std::cout << std::endl;
  std::cout << "Events with BCID mismatches between led and ledx data lines: "<<BCIDMismatches<<std::endl;
  std::cout << "Events with missing data: "<<IncompleteEvents<<std::endl;
  std::cout << "Events with module errors: "<<Errors<<std::endl;
  
}



/** This is a minimal working example of a StrobeDelay scan 
 */
void SimpleStrobeDelayScan()
{
  ///
  /// Setup the TRB readout board
  ///
  TRBAccess *trb = new TRBAccess(false);
  trb->SetDebug(false);
  
  uint8_t moduleMask = 0xF; // enable a single module. In this case module 3
  
  trb->GetConfig()->Set_Module_L1En(0x0); // disable hardware L1A for all modules
  trb->GetConfig()->Set_Module_ClkCmdSelect(0x0); // all modules use CMD_0
  trb->GetConfig()->Set_Module_LedRXEn(moduleMask); // enable led lines
  trb->GetConfig()->Set_Module_LedxRXEn(moduleMask); // enable ledx lines
  
  trb->GetConfig()->Set_Global_RxTimeoutDisable(true);  // default config as specified by Yannick
  trb->GetConfig()->Set_Global_L1TimeoutDisable(false); // default config as specified by Yannick
  trb->GetConfig()->Set_Global_L2SoftL1AEn(true);       // default config as specified by Yannick
  trb->GetConfig()->Set_Global_Overflow(4095); 

  trb->WriteConfigReg(); // uplaod the config to the TRB

  /// Set the Fine phase adjustment
  trb->GetPhaseConfig()->SetFinePhase_Clk0(0);
  trb->GetPhaseConfig()->SetFinePhase_Led0(55);
  trb->GetPhaseConfig()->SetFinePhase_Clk1(0);
  trb->GetPhaseConfig()->SetFinePhase_Led1(55);
  trb->WritePhaseConfigReg();
  trb->ApplyPhaseConfig();

  std::vector<uint16_t> configIs  = trb->ReadConfigReg();
  ConfigReg actualCfg;
  actualCfg.FillFromPayload(configIs);
  actualCfg.Print();

  // here we setup the output file to use for data storage
  std::stringstream outfileName;
  outfileName << "SimpleScan.daq";
  trb->SetupStorageStream(outfileName.str());
  
  ///
  /// Now configure the module with the configuration to start from.
  /// In this case the default config from the ConfigHandlerSCT class is used.
  /// Could also be loaded from a JSON file instead.
  ///
  std::cout << "Loading configuration to modules 0x"<<std::hex<<moduleMask <<std::endl;
  ConfigHandlerSCT *cfg = new ConfigHandlerSCT();
  // cfg->ReadFromFile(cfgFileName);
  cfg->SetReadoutMode(2); // on top of default config set ReadoutMode to 01X = edge (physics)
  cfg->SetEdgeDetection(1);
  int calMode = 0;
  cfg->SetCalMode(calMode);
  trb->ConfigureSCTModule(cfg, moduleMask); //  explicitely specify channel
  std::cout << "   Done."<<std::endl;
 
  ScanStepConfig *scanConfig = new ScanStepConfig; // This object is used to store the scan configuration in every scan step
  
  for (unsigned int calMode = 0; calMode < 4; calMode++){

    std::cout << std::endl<<"Mask Step: "<< calMode<<std::endl<<std::endl;
    // write Stripmask to module and store stripMask steip in scan config
    trb->SCT_WriteStripmask(moduleMask, calMode);
    scanConfig->CalMode = calMode;
    cfg->SetCalMode(calMode);
    trb->ConfigureSCTModule(cfg, moduleMask);
    
    uint16_t thrValue = 45<<8;   // threshold is stored in the 8 MSB
    thrValue |= 0x40;             // 0x40 = 4fC calibration charge to be injected is set in the 8 LSB
    trb->GetSCTSlowCommandBuffer()->SetThreshold(0x3f, thrValue);
    trb->WriteSCTSlowCommandBuffer();
    trb->SendSCTSlowCommandBuffer(moduleMask);

    
    // This is the loop over the threshold setting
    for (unsigned int delay = 0; delay < 64; delay += 2){
      int expectedL1A = 50; // here you can set how many triggers are sent in every scan step
      std::cout << "StrobeDelay scan loop: "<<std::dec<<delay<<std::endl;
      
      // set configuration
      scanConfig->Threshold = delay;
      scanConfig->ExpectedL1A = expectedL1A;
      trb->SaveScanStepConfig(*scanConfig); // this writes the scan step config to the data file
      
      trb->GetSCTSlowCommandBuffer()->SetStrobeDelay(0x3f, delay);
      trb->WriteSCTSlowCommandBuffer();
      trb->SendSCTSlowCommandBuffer(moduleMask);
      
      // start data taking
      trb->FIFOReset();
      trb->SCT_EnableDataTaking(moduleMask);
      trb->GenerateSoftReset(moduleMask);
      
      // slepp a bit to avoid communication errors, could be tuned ...
      // usleep(10000);
      
      // start readout
      trb->StartReadout();
      
      // send some L1As and monitor the L1A rate
      auto time_start = std::chrono::high_resolution_clock::now();
      auto time_end = std::chrono::high_resolution_clock::now();
      for (unsigned i = 0; i < expectedL1A; i++){
        // send a calibration pulse followed by an L1A after 130 BCs
        trb->SCT_CalibrationPulse(moduleMask, 130);
        // Without charge injection: use GenerateL1A();
        //trb->GenerateL1A(moduleMask);
        
        // Adding some sleep after every L1A, could be tuned
        //usleep(600);
        if (i % 10000 == 0 && i != 0){
          time_end = std::chrono::high_resolution_clock::now();
          auto duration = std::chrono::duration_cast<std::chrono::microseconds>(time_end - time_start);
          std::cout << "L1A cnt: "<<i<<" Trigger Rate = "<<10.0e6/duration.count()<<" kHz"<<std::endl;
          time_start = std::chrono::high_resolution_clock::now();
        }
      }
      
      // stop data taking
      //usleep(10000);
      std::cout << "Readout stopped "<<std::endl;
      trb->StopReadout();
    }
  }
  
  delete cfg;
  delete trb;
}


/** This is a minimal working example of a threshold scan on 1 module
 */
void ThresholdScan(TRBAccess *trb, uint8_t moduleMask, std::string outputFileName, unsigned int charge, unsigned int bias, unsigned int thr_stepSize, unsigned int thr_min, unsigned int thr_max)
{

  std::cout << " ################## START OF THRESHOLD SCAN ## charge = "<<std::dec<<charge
  << " = " <<0.0625 * charge <<"fC bias = 0x"<<std::hex<<bias<<std::endl;

  // here we setup the output file to use for data storage
  std::stringstream outfileName;
  outfileName << outputFileName;
  trb->SetupStorageStream(outfileName.str());
  
  ///
  /// Now configure the module with the configuration to start from.
  /// In this case the default config from the ConfigHandlerSCT class is used.
  /// Could also be loaded from a JSON file instead.
  ///
  std::cout << "Loading configuration to modules 0x"<<std::hex<<moduleMask <<std::endl;
  ConfigHandlerSCT *cfg = new ConfigHandlerSCT();
  // cfg->ReadFromFile(cfgFileName);
  cfg->SetReadoutMode(1); // on top of default config set ReadoutMode to X1X => state (default for physics)
  cfg->SetEdgeDetection(0);

  int calMode = 0;
  cfg->SetCalMode(calMode);
  cfg->SetBiasDac(bias);
  trb->ConfigureSCTModule(cfg, moduleMask); //  explicitely specify channel
  std::cout << "   Done."<<std::endl;
 
  ScanStepConfig *scanConfig = new ScanStepConfig; // This object is used to store the scan configuration in every scan step
  
  for (unsigned int calMode = 0; calMode < 4; calMode++){

    std::cout << std::endl<<"Mask Step: "<< calMode<<std::endl<<std::endl;
    // write Stripmask to module and store stripMask steip in scan config
    trb->SCT_WriteStripmask(moduleMask, calMode);
    scanConfig->CalMode = calMode;
    cfg->SetCalMode(calMode);
    trb->ConfigureSCTModule(cfg, moduleMask);
    
    // Set the strobe delay
    trb->GetSCTSlowCommandBuffer()->SetStrobeDelay(0x3f, 15);
    trb->WriteSCTSlowCommandBuffer();
    trb->SendSCTSlowCommandBuffer(moduleMask);

    // This is the loop over the threshold setting
    for (unsigned int thr = thr_min; thr <= thr_max; thr += thr_stepSize){
       int expectedL1A = 50; // here you can set how many triggers are sent in every scan step
       std::cout << "Threshold scan loop: "<<std::dec<<thr<<std::endl;

       // set configuration
       scanConfig->Threshold = thr;
       scanConfig->ExpectedL1A = expectedL1A;
       trb->SaveScanStepConfig(*scanConfig); // this writes the scan step config to the data file

       uint16_t thrValue = thr<<8;   // threshold is stored in the 8 MSB
       thrValue |= charge;           // calibration charge to be injected is set in the 8 LSB
       //std::cout << " ThrValue = 0x"<<std::hex<<thrValue<<std::endl;
       //upload threshold register to module
       trb->GetSCTSlowCommandBuffer()->SetThreshold(0x3f, thrValue);
       trb->WriteSCTSlowCommandBuffer();
       trb->SendSCTSlowCommandBuffer(moduleMask);

       // start data taking
       trb->FIFOReset();
       trb->SCT_EnableDataTaking(moduleMask);
       trb->GenerateSoftReset(moduleMask);

       // slepp a bit to avoid communication errors, could be tuned ...
       // usleep(10000);

       // start readout
       trb->StartReadout();

       // send some L1As and monitor the L1A rate
       auto time_start = std::chrono::high_resolution_clock::now();
       auto time_end = std::chrono::high_resolution_clock::now();
       for (unsigned i = 0; i < expectedL1A; i++){
          // send a calibration pulse followed by an L1A after 130 BCs
          trb->SCT_CalibrationPulse(moduleMask, 130);
          // Without charge injection: use GenerateL1A();
          //trb->GenerateL1A(moduleMask);

          // Adding some sleep after every L1A, could be tuned
          //usleep(600);
          if (i % 10000 == 0 && i != 0){
             time_end = std::chrono::high_resolution_clock::now();
             auto duration = std::chrono::duration_cast<std::chrono::microseconds>(time_end - time_start);
             std::cout << "L1A cnt: "<<i<<" Trigger Rate = "<<10.0e6/duration.count()<<" kHz"<<std::endl;
             time_start = std::chrono::high_resolution_clock::now();
          }
       }

       // stop data taking
       //usleep(10000);
       std::cout << "Readout stopped "<<std::endl;
       trb->StopReadout();
    }
  }

  delete cfg;
}
/** This is a minimal working example of a threshold scan on 1 module
 */
void SimpleThresholdScan(std::string outputFileName, unsigned int charge, unsigned int bias, unsigned int thr_stepSize, unsigned int thr_min, unsigned int thr_max)
{

  std::cout << " ################## START OF THRESHOLD SCAN ## charge = "<<std::dec<<charge
  << " = " <<0.0625 * charge <<"fC bias = 0x"<<std::hex<<bias<<std::endl;
  ///
  /// Setup the TRB readout board
  ///
  TRBAccess *trb = new TRBAccess(false);
  trb->SetDebug(false);
  
  uint8_t moduleMask = 0xF; // enable a single module. In this case module 3
  
  trb->GetConfig()->Set_Module_L1En(0x0); // disable hardware L1A for all modules
  trb->GetConfig()->Set_Module_ClkCmdSelect(0x0); // all modules use CMD_0
  trb->GetConfig()->Set_Module_LedRXEn(moduleMask); // enable led lines
  trb->GetConfig()->Set_Module_LedxRXEn(moduleMask); // enable ledx lines
  
  trb->GetConfig()->Set_Global_RxTimeoutDisable(true);  // default config as specified by Yannick
  trb->GetConfig()->Set_Global_L1TimeoutDisable(false); // default config as specified by Yannick
  trb->GetConfig()->Set_Global_L2SoftL1AEn(true);       // default config as specified by Yannick
  trb->GetConfig()->Set_Global_Overflow(4095); 

  trb->WriteConfigReg(); // uplaod the config to the TRB
  
  /// Set the Fine phase adjustment
  trb->GetPhaseConfig()->SetFinePhase_Clk0(0);
  trb->GetPhaseConfig()->SetFinePhase_Led0(45);
  trb->GetPhaseConfig()->SetFinePhase_Clk1(0);
  trb->GetPhaseConfig()->SetFinePhase_Led1(45);
  trb->WritePhaseConfigReg();
  trb->ApplyPhaseConfig();
  
  std::vector<uint16_t> configIs  = trb->ReadConfigReg();
  ConfigReg actualCfg;
  actualCfg.FillFromPayload(configIs);
  actualCfg.Print();

  // here we setup the output file to use for data storage
  std::stringstream outfileName;
  outfileName << outputFileName;
  trb->SetupStorageStream(outfileName.str());
  
  ///
  /// Now configure the module with the configuration to start from.
  /// In this case the default config from the ConfigHandlerSCT class is used.
  /// Could also be loaded from a JSON file instead.
  ///
  std::cout << "Loading configuration to modules 0x"<<std::hex<<moduleMask <<std::endl;
  ConfigHandlerSCT *cfg = new ConfigHandlerSCT();
  // cfg->ReadFromFile(cfgFileName);
  cfg->SetReadoutMode(1); // on top of default config set ReadoutMode to X1X => state (default for physics)
  cfg->SetEdgeDetection(0);

  int calMode = 0;
  cfg->SetCalMode(calMode);
  cfg->SetBiasDac(bias);
  trb->ConfigureSCTModule(cfg, moduleMask); //  explicitely specify channel
  std::cout << "   Done."<<std::endl;
 
  ScanStepConfig *scanConfig = new ScanStepConfig; // This object is used to store the scan configuration in every scan step
  
  for (unsigned int calMode = 0; calMode < 4; calMode++){

    std::cout << std::endl<<"Mask Step: "<< calMode<<std::endl<<std::endl;
    // write Stripmask to module and store stripMask steip in scan config
    trb->SCT_WriteStripmask(moduleMask, calMode);
    scanConfig->CalMode = calMode;
    cfg->SetCalMode(calMode);
    trb->ConfigureSCTModule(cfg, moduleMask);
    
    // Set the strobe delay
    trb->GetSCTSlowCommandBuffer()->SetStrobeDelay(0x3f, 15);
    trb->WriteSCTSlowCommandBuffer();
    trb->SendSCTSlowCommandBuffer(moduleMask);

    // This is the loop over the threshold setting
    for (unsigned int thr = thr_min; thr <= thr_max; thr += thr_stepSize){
       int expectedL1A = 50; // here you can set how many triggers are sent in every scan step
       std::cout << "Threshold scan loop: "<<std::dec<<thr<<std::endl;

       // set configuration
       scanConfig->Threshold = thr;
       scanConfig->ExpectedL1A = expectedL1A;
       trb->SaveScanStepConfig(*scanConfig); // this writes the scan step config to the data file

       uint16_t thrValue = thr<<8;   // threshold is stored in the 8 MSB
       thrValue |= charge;           // calibration charge to be injected is set in the 8 LSB
       //std::cout << " ThrValue = 0x"<<std::hex<<thrValue<<std::endl;
       //upload threshold register to module
       trb->GetSCTSlowCommandBuffer()->SetThreshold(0x3f, thrValue);
       trb->WriteSCTSlowCommandBuffer();
       trb->SendSCTSlowCommandBuffer(moduleMask);

       // start data taking
       trb->FIFOReset();
       trb->SCT_EnableDataTaking(moduleMask);
       trb->GenerateSoftReset(moduleMask);

       // slepp a bit to avoid communication errors, could be tuned ...
       // usleep(10000);

       // start readout
       trb->StartReadout();

       // send some L1As and monitor the L1A rate
       auto time_start = std::chrono::high_resolution_clock::now();
       auto time_end = std::chrono::high_resolution_clock::now();
       for (unsigned i = 0; i < expectedL1A; i++){
          // send a calibration pulse followed by an L1A after 130 BCs
          trb->SCT_CalibrationPulse(moduleMask, 130);
          // Without charge injection: use GenerateL1A();
          //trb->GenerateL1A(moduleMask);

          // Adding some sleep after every L1A, could be tuned
          //usleep(600);
          if (i % 10000 == 0 && i != 0){
             time_end = std::chrono::high_resolution_clock::now();
             auto duration = std::chrono::duration_cast<std::chrono::microseconds>(time_end - time_start);
             std::cout << "L1A cnt: "<<i<<" Trigger Rate = "<<10.0e6/duration.count()<<" kHz"<<std::endl;
             time_start = std::chrono::high_resolution_clock::now();
          }
       }

       // stop data taking
       //usleep(10000);
       std::cout << "Readout stopped "<<std::endl;
       trb->StopReadout();
    }
  }

  delete cfg;
  delete trb;
}


void L1ADelayScan(std::string cfgFileName, unsigned int moduleMask)
{
   ScanStepConfig *scanConfig = new ScanStepConfig;
   TRBAccess *trb = new TRBAccess("10.11.65.7", "10.11.65.7", 0, "faser-trb-14", false);
   trb->SetDebug(false);

   //int module = 0;
   //uint8_t moduleMask = 0x1; // module 0 enabled

   trb->GetConfig()->Set_Module_L1En(0x0);
   trb->GetConfig()->Set_Module_ClkCmdSelect(0x0); // switch between CMD_0 and CMD_1 lines
   trb->GetConfig()->Set_Module_LedRXEn(moduleMask);
   trb->GetConfig()->Set_Module_LedxRXEn(moduleMask);

   trb->GetConfig()->Set_Global_RxTimeoutDisable(true);
   trb->GetConfig()->Set_Global_L1TimeoutDisable(false);
   trb->GetConfig()->Set_Global_L2SoftL1AEn(true);

   //trb->GetConfig()->Set_Global_SoftL1AEn(true); //  enable soft L1As
   trb->WriteConfigReg();
/// Set the Fine phase adjustment
  trb->GetPhaseConfig()->SetFinePhase_Clk0(0);
  trb->GetPhaseConfig()->SetFinePhase_Led0(55);
  trb->GetPhaseConfig()->SetFinePhase_Clk1(0);
  trb->GetPhaseConfig()->SetFinePhase_Led1(55);
  trb->WritePhaseConfigReg();
  trb->ApplyPhaseConfig();

   std::cout << "Loading configuration to modules 0x"<<std::hex<<moduleMask <<std::endl;
   ConfigHandlerSCT *cfg = new ConfigHandlerSCT();
   // cfg->ReadFromFile(cfgFileName);
   sleep(1);
   trb->ConfigureSCTModule(cfg, moduleMask ); //  explicitely specify channel
   std::cout << "   Done."<<std::endl;


   std::stringstream outfileName;
   outfileName << "DelayScan.daq";
   trb->SetupStorageStream(outfileName.str());
   trb->GenerateL1A(moduleMask); // output module config

   TRBEventDecoder *ed = new TRBEventDecoder();

   uint16_t value = 60 << 8; // threshold
   value |= 0x30; // = 0x30
   std::cout << "Setting ThrCalib register to 0x"<<std::hex<<value<<std::endl;
   trb->GetSCTSlowCommandBuffer()->SetThreshold(0x3f, value);
   trb->WriteSCTSlowCommandBuffer();
   trb->SendSCTSlowCommandBuffer(moduleMask);

   // calibration loop, example strobe delay
   for (unsigned int delay = 125; delay < 135; delay += 1){
      int expectedL1A = 300;
      std::cout << "Delay value: "<<std::dec<<delay<<std::endl;

      scanConfig->ExpectedL1A = expectedL1A;
      trb->SaveScanStepConfig(*scanConfig);

      // start DT
      trb->FIFOReset();
      trb->SCT_EnableDataTaking(moduleMask);
      trb->GenerateSoftReset(moduleMask);
      trb->StartReadout();

      // send some L1As
      for (unsigned i = 0; i < expectedL1A; i++){
         usleep(1000);
         //trb->SCT_CalibrationPulse(moduleMask, delay);
         trb->GenerateL1A(moduleMask);
      }

      // stop DT
      sleep(1);
      std::cout << "Readout stopped "<<std::endl;
      trb->StopReadout();

      // Do a quick analysis:
      ed->LoadTRBEventData(trb->GetTRBEventData());
      //ed->DecodeData();
      for (unsigned int i = 0; i < 7; i++){
         unsigned int tmp = 0x1 << i;
         if (tmp & moduleMask){
            //ed->DecodeModuleData(i);
            PrintEventSummary(ed, i);
         }
      }

   }

   delete cfg;
   delete trb;
}

void NoiseRun(std::string cfgFileName, bool doCfg, uint8_t moduleMask, bool debug)
{
   unsigned int module = 0;

   std::cout << "Initialising TRB access "<<std::endl;
   TRBAccess *trb = new TRBAccess(false);
   trb->SetDebug(debug);
   trb->SetupStorageStream("ConfigReadback.daq");

   std::cout << "TRB Firmware versions: "<<std::endl;
   trb->GetFirmwareVersions();

   trb->GetConfig()->Set_Module_L1En(module, false);
   trb->GetConfig()->Set_Module_ClkCmdSelect(module, false); // switch between CMD_0 and CMD_1 lines
   trb->GetConfig()->Set_Module_LedxRXEn(module, true, true); // switch between led and ledx lines
   trb->GetConfig()->Set_Global_L1TimeoutDisable(true);

   trb->WriteConfigReg();
   trb->GetPhaseConfig()->SetFinePhase_Clk0(0);
   trb->GetPhaseConfig()->SetFinePhase_Led0(finePhaseDelay);
   trb->GetPhaseConfig()->SetFinePhase_Clk1(0);
   trb->GetPhaseConfig()->SetFinePhase_Led1(finePhaseDelay);
   trb->WritePhaseConfigReg();
   trb->ApplyPhaseConfig();

   if (doCfg){

      std::cout << "Loading configuration to modules 0x"<<std::hex<<moduleMask <<std::endl;
      ConfigHandlerSCT *cfg = new ConfigHandlerSCT();
      //cfg->ReadFromFile(cfgFileName);
      std::cout << "   Done."<<std::endl;

      // configure module
      std::cout << "Configuring module "<<module<<std::endl;
      sleep(1);
      trb->ConfigureSCTModule(cfg, moduleMask ); //  explicitely specify channel
      std::cout << "   Done."<<std::endl;
   }

   std::cout << "Reading back configuration "<<std::endl;
   // readback config: If not placed in Data Taking mode the SCT chips are in Send ID mode, sending their ID and config upon an L1A

   trb->FIFOReset();


   std::cout << "Sarting readout "<<std::endl;

   trb->StartReadout();
   for (int i = 0; i < 2; i++){
      usleep(1000);
      //std::cout << "Generating L1A for modules "<<std::hex<<(int)moduleMask<<std::endl;
      trb->GenerateL1A(moduleMask);
   }

   /*
      trb->SCT_EnableDataTaking(moduleMask);
      for (int i = 0; i < 10; i++){
      usleep(1000);
//std::cout << "Generating L1A for modules "<<std::hex<<(int)moduleMask<<std::endl;
trb->GenerateL1A(moduleMask);
}
*/
sleep(2);
std::cout << "Stopping readout"<<std::endl;
trb->StopReadout();
sleep(1);
std::cout << "   Done."<<std::endl;
//delete cfg;
delete trb;

}


/** *************** n-point gain ********************
 *
 ** *************************************************/

void RunNPointGain(std::vector<unsigned int> chargePoints,
                   unsigned int bias = 0x1819, unsigned int ThrStepSize = 2, unsigned int ThrMin = 0, unsigned int ThrMax = 100)
{

  ///
  /// Setup the TRB readout board
  ///
  TRBAccess *trb = new TRBAccess(false);
  trb->SetDebug(false);
  
  uint8_t moduleMask = 0xF; // enable a single module. In this case module 3
  
  trb->GetConfig()->Set_Module_L1En(0x0); // disable hardware L1A for all modules
  trb->GetConfig()->Set_Module_ClkCmdSelect(0x0); // all modules use CMD_0
  trb->GetConfig()->Set_Module_LedRXEn(moduleMask); // enable led lines
  trb->GetConfig()->Set_Module_LedxRXEn(moduleMask); // enable ledx lines
  
  trb->GetConfig()->Set_Global_RxTimeoutDisable(true);  // default config as specified by Yannick
  trb->GetConfig()->Set_Global_L1TimeoutDisable(false); // default config as specified by Yannick
  trb->GetConfig()->Set_Global_L2SoftL1AEn(true);       // default config as specified by Yannick
  trb->GetConfig()->Set_Global_Overflow(4095); 

  trb->WriteConfigReg(); // uplaod the config to the TRB
  
  /// Set the Fine phase adjustment
  trb->GetPhaseConfig()->SetFinePhase_Clk0(0);
  trb->GetPhaseConfig()->SetFinePhase_Led0(45);
  trb->GetPhaseConfig()->SetFinePhase_Clk1(0);
  trb->GetPhaseConfig()->SetFinePhase_Led1(45);
  trb->WritePhaseConfigReg();
  trb->ApplyPhaseConfig();
  
  std::vector<uint16_t> configIs  = trb->ReadConfigReg();
  ConfigReg actualCfg;
  actualCfg.FillFromPayload(configIs);
  actualCfg.Print();

   for (auto charge : chargePoints){
    std::stringstream ss;
    ss << "ThrScan_injCharge_0x"<<std::hex<<charge<<"_bias_0x"<<std::hex<<bias<<".daq";
    ThresholdScan(trb, moduleMask, ss.str(), charge, bias, ThrStepSize, ThrMin, ThrMax);
  }
  delete trb;
}


int main( int argc, char ** argv)
{
   unsigned char moduleMask = 0;
   bool doCfg = false;
   bool debug = false;
   bool TestMode = false;


   for (unsigned int i = 1 ; i < argc; i++){
      if (argv[i][0] == 'm') {
         moduleMask = atoi(argv[++i]);
         std::cout << "Enabled modules 0x"<<std::hex<< (int)moduleMask<<std::endl;
         continue;
      }
      if (argv[i][0] == 'c') {
         doCfg = true;
         std::cout << "Will config modules"<<std::endl;
      }
      if (argv[i][0] == 'd') {
         debug = true;
         std::cout << "Debug mode"<<std::endl;
      }
      if (argv[i][0] == 't') {
         std::cout << "FileWriting test mode"<<std::endl;
         TestMode = true;
      }

   }

   // if (TestMode){
   // ThrScanTest();
   // return 0;
   //}
   //ConfigurationReadback("TestModuleCfg.json", doCfg,  moduleMask, debug);

   //NoiseRun("TestModuleCfg.json", doCfg,  moduleMask, debug);

  //TestEmulator("/Users/ks/cernbox/WorkDir/Faser/Software/SampleData/FaserReadout/DataTaking_4modules_10kL1A.daq");
  
   // SimpleThresholdScan();
   // OnlineDecodingThresholdScan();
   //SimpleStrobeDelayScan();

   std::vector<unsigned int > chargePoints = {0x24, 0x32, 0x40};
    //RunNPointGain(chargePoints,  0x1819 /* bias */, 2 /* thr_stepSize */ ,30 /* thr_min */, 130 /* thr_max */);
  
   L1ADelayScan("TestModuleCfg.json", 0x4);

   return 0;
}
