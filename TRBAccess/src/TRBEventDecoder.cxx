/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 
#include "../TrackerReadout/TRBEventDecoder.h"

#include <algorithm>
#include <chrono>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <assert.h>
#include <memory>

using namespace FASER;
using std::ios;

/** \brief Constructor
    Initializes storage containers
 */
TRBEventDecoder::TRBEventDecoder() : m_PrintEventInformation(0), m_eventCnt(0), m_eventCntEOD(0)
{
  m_moduleData_led.resize(8);
  m_moduleData_ledx.resize(8);
}

TRBEventDecoder::~TRBEventDecoder()
{
  ClearEvents();
}

/** \brief Clearing internal event storage
 */
void TRBEventDecoder::ClearEvents()
{
  // sorting the pointers to events to easily find dubplicates
  std::sort(m_Events.begin(), m_Events.end());
  TRBEvent* lastPtr = nullptr;
  for (auto event : m_Events){
    if (lastPtr == nullptr) lastPtr = event;
    else if (lastPtr == event) { ERROR("Duplicate entry in list of events!! #FindMe# "); continue;}
    lastPtr = event;
    delete event;
  }
  m_Events.clear();
}

/** \brief Read and decode ROR event header
 */
void TRBEventDecoder::Read_RORHeader(std::ifstream &in, std::ofstream *out)
{
  m_RORHeader.Read(in);
 if (m_PrintEventInformation > 1)  m_RORHeader.Print();
  
  
  switch (m_RORHeader.GetDataStatus()){
    case RORHeader::DATA_STATUS_SCANSTEP:
      m_ScanStepConfig.Read(in);
      if (m_PrintEventInformation > 1) {
        INFO("Events decoded for previous ScanStep: "<<std::dec<<m_eventCnt<<" EODs found: "<<m_eventCntEOD);
        m_ScanStepConfig.Print();
      }
      m_eventCnt = 0;
      break;
      
    case RORHeader::DATA_STATUS_OK:
      m_dataBytes.clear();
      m_data.clear();
      if (m_RORHeader.GetFormatVersion() == 0x0){
        m_dataBytes.resize(m_RORHeader.GetDataSize()*4);
        in.read(&m_dataBytes[0], m_RORHeader.GetDataSize()*4);
        if (out->is_open()){
          out->write(&m_dataBytes[0], m_RORHeader.GetDataSize()*4);
        }
      } else {
        m_dataBytes.resize(m_RORHeader.GetDataSize());
        in.read(&m_dataBytes[0], m_RORHeader.GetDataSize());
        if (out->is_open()){
          out->write(&m_dataBytes[0], m_RORHeader.GetDataSize());
        }
      }
     
      
      break;
      
    case RORHeader::DATA_STATUS_EOD:
      m_dataBytes.clear();
      m_data.clear();
      if (m_RORHeader.GetFormatVersion() == 0x0){
        m_dataBytes.resize(m_RORHeader.GetDataSize()*4);
        in.read(&m_dataBytes[0], m_RORHeader.GetDataSize()*4);
      } else {
        m_dataBytes.resize(m_RORHeader.GetDataSize());
        in.read(&m_dataBytes[0], m_RORHeader.GetDataSize());
      }
      m_eventCntEOD++;
      m_EventsPerScanStep.push_back(m_Events);
      m_ScanSteps.push_back(m_ScanStepConfig);
            
      break;
      
    default:
      ERROR("Unknown ROR Header Tag, this shoud not have happened!");
  }
  
}

/** \brief Load tracker data from file (expecting ROR headers).
 ROR and ScanStep headers will be decoded.
 Data will be stord in m_dataBytes.
 */
void TRBEventDecoder::LoadRORDataFromFile(std::string filename, unsigned int module, std::vector< std::vector <uint16_t> > *moduleConfig, std::ofstream *outstream)
{
  ClearEvents();
  
  std::ifstream in(filename, std::ios::binary);
  if (!in.is_open()){
    ERROR("Can't open file "<<filename);
    return;
  }
  auto time_start = std::chrono::high_resolution_clock::now();
  auto time_end = std::chrono::high_resolution_clock::now();
  int evtCnt = 0;
  while (in.good()){
    try {
      Read_RORHeader(in, outstream);
      m_eventCnt++;
      TRBEvent *trbEvent = DecodeData(false, true); // only decode a single event
      if (trbEvent != nullptr) {
        if (module > 7){
          for (unsigned int i = 0; i < 8; i++) DecodeModuleData(i, trbEvent, moduleConfig);
        } else {
          DecodeModuleData(module, trbEvent, moduleConfig);
        }
      }
      
    } catch (...) {
      ERROR("Read_RORHeader threw an error. Will stop reading the input file but allow the application to continue.");
      break;
    }
    evtCnt++;
    if (evtCnt % 5000 == 0 && evtCnt != 0){
      time_end = std::chrono::high_resolution_clock::now();
      auto duration = std::chrono::duration_cast<std::chrono::microseconds>(time_end-time_start);
      DEBUG("# Evts read: "<<evtCnt<<" processing reate = " << 5e6/duration.count()<< " kHz");
      time_start = std::chrono::high_resolution_clock::now();
    }
  }
  DEBUG("# ROR_Headers read: "<<evtCnt);
  
}

/** \brief Read a single TRB data events.
 *  Raw TRB data of one complete event is stored expected in a vector<uint32_t>.
 *  only one event expected.
 *  Results are identical to reading data from File via LoadRORDataFromFile().
 */
void TRBEventDecoder::LoadTRBEventData(std::vector<uint32_t> event)
{
  ClearEvents();
  
  m_data.clear();
  m_data = event;
  TRBEvent *trbEvent = DecodeData(false, true); // only decode a single event
  if (trbEvent != nullptr) {
    // always decode all modules
    for (unsigned int i = 0; i < 8; i++) DecodeModuleData(i, trbEvent, nullptr /* moduleConfig */);
  }
}

/** \brief Read TRB data group by events.
 *  Raw TRB data of one complete event is stored expected in a vector<uint32_t>.
 *  Several events are again stored in an std::vector<>. Every event will be decoded separately.
 *  Results are identical to reading data from File via LoadRORDataFromFile().
 */
 void TRBEventDecoder::LoadTRBEventData(std::vector < std::vector<uint32_t> > data)
{
  ClearEvents();
  for (auto event : data){
    m_data.clear();
    m_data = event;
    TRBEvent *trbEvent = DecodeData(false, true); // only decode a single event
    if (trbEvent != nullptr) {
      // always decode all modules
      for (unsigned int i = 0; i < 8; i++) DecodeModuleData(i, trbEvent, nullptr /* moduleConfig */);
    }
  }
  // Now all events are decoded and properties can be retrieved unsing the usual accessors
}

/** \brief Copy data from vector
 */
void TRBEventDecoder::LoadData(std::vector<uint32_t> data)
{
  m_data.clear();
  ClearEvents();
  m_moduleData_led[0].clear();
  m_moduleData_ledx[0].clear();
  m_data = data;
}

/** \brief Load tracker readout board (TRB) bitstream from file.
    Data will be stord in m_data.
 */
void TRBEventDecoder::LoadDataFromFile(std::string filename)
{
  m_data.clear();
  ClearEvents();
  
  m_moduleData_led.clear();
  m_moduleData_ledx.clear();
  
  std::ifstream in(filename, std::ios::binary);
  if (!in.is_open()){
    ERROR("Can't open file "<<filename);
    return;
  }
  
  uint32_t inputBuffer;
  unsigned int byteCount = 4;
  char *charBuf = new char[byteCount]; // 4 vyte buffer = 32bits
  unsigned int wordCnt = 0;
  while (in) {
    for (unsigned int i = 0; i < byteCount; i++ ){
      charBuf[i] = 0;
    }
    in.seekg (wordCnt*byteCount, ios::beg);
    in.read (charBuf, byteCount);

    wordCnt++;
    inputBuffer = 0;
    inputBuffer |= (charBuf[3] & 0xff);
    inputBuffer = inputBuffer << 8;
    inputBuffer |= charBuf[2] & 0xff;
    inputBuffer = inputBuffer << 8;
    inputBuffer |= charBuf[1] & 0xff;
    inputBuffer = inputBuffer << 8;
    inputBuffer |= charBuf[0] & 0xff;

    // std::cout << "32 bit word = 0x"<<std::hex<<inputBuffer<<std::endl;
    m_data.push_back(inputBuffer);
  }
  // output some stats
  DEBUG("Read "<<wordCnt<<"  "<<sizeof(inputBuffer)<<" bytes words from file "<<filename);
  
}

/** \brief Load sct module bitstream from file
    Data will be stored separately by data lines in m_moduleData_led[0] and m_moduleData_ledx[0], i.e. as module 0.
    Only the 24 LSB of the 32 bit words stored are used. Bitwise allignment of data from subsequent word happens in Bitstream class.
    It is recommended to use the Bitstream class of this package when further processing the module. Module data is available via GetModuleBitstream(unisgned int module, bool dataline).
 */
void TRBEventDecoder::LoadModuleDataFromFile(std::string filename)
{
  ClearEvents();
  m_moduleData_led[0].clear();
  m_moduleData_ledx[0].clear();
  
  std::ifstream in(filename, std::ios::binary);
  if (!in.is_open()){
    ERROR("Can't open file "<<filename);
    return;
  }
  
  uint32_t inputBuffer;
  unsigned int byteCount = 3; // we only read 24bit at a time -> same alignment as data extracted from trb words
  char *charBuf = new char[byteCount]; // 4 vyte buffer = 32bits
  unsigned int wordCnt = 0;
  while (in) {
    for (unsigned int i = 0; i < byteCount; i++ ){
      charBuf[i] = 0;
    }
    in.seekg (wordCnt*byteCount, ios::beg);
    in.read (charBuf, byteCount);
    
    wordCnt++;
    inputBuffer = 0;
    inputBuffer |= (charBuf[0] & 0xff);
    inputBuffer = inputBuffer << 8;
    inputBuffer |= charBuf[1] & 0xff;
    inputBuffer = inputBuffer << 8;
    inputBuffer |= charBuf[2] & 0xff;
    
    // std::cout << "32 bit word = 0x"<<std::hex<<inputBuffer<<std::endl;
    m_moduleData_led[0].push_back(inputBuffer);
  }
  // output some stats
  DEBUG("Read "<<wordCnt<<"  "<<sizeof(inputBuffer)<<" bytes words from file "<<filename);
  
}

/** \brief Checks if passed data word is the End Of DAQ word
 */
bool  TRBEventDecoder::IsEndOfDAQ(uint32_t data)
{
  if (data== TRB_END) {
    return true;
  }
  return false;
}

/** \brief Workaround for FW version < 1.2: Decode SCT module data to find module header.
 */
bool  TRBEventDecoder::IsHeader(std::vector<uint32_t> data, unsigned int &L1ID, unsigned int &BCID)
{
  m_data.clear();
  m_data = data;
  if (m_PrintEventInformation > 3) DEBUG("Decoding data");
  DecodeData(false);
  if (m_PrintEventInformation > 3) DEBUG("Decoding module data");

  auto hasHeader = ModuleDataHasHeader(L1ID, BCID);
  if (m_PrintEventInformation > 3) DEBUG("Done");
  
  return hasHeader;
}

/** \brief Check TRB word for TRB header. Extracts L1ID.
 */
bool  TRBEventDecoder::IsHeader(uint32_t data, unsigned int &L1ID)
{
  bool isHeader = false;
  if ((data & MASK_TRBFRAME) == TRB_HEADER) {
    isHeader = true;
    if (m_PrintEventInformation > 1) DEBUG(" Found TRB header with EVENT CNT = "<<(data & MASK_EVNTCNT));
    L1ID = (data & MASK_EVNTCNT);
  }
  return isHeader;
}

/** \brief Check TRB word for Module data
 */
bool TRBEventDecoder::IsModuleData(uint32_t data, unsigned int &ModuleData)
{
  if ((data & MASK_WORDTYPE) == WORDTYPE_MODULEDATA_LED || (data & MASK_TRBFRAME) == WORDTYPE_MODULEDATA_LEDX ){
    ModuleData = (unsigned int)(data & MASK_MODULEDATA);
    return true;
  }
  return false;
}

/** \brief Workaround for FW version < 1.2: Decode SCT module data to find module trailer
 */
bool  TRBEventDecoder::IsTrailer(std::vector<uint32_t> data)
{
  m_data.clear();
  m_data = data;
  
  DecodeData(false);
  
  return ModuleDataHasTrailer();
}

/** \brief Check TRB word for TRB trailer. Extracts checksum.
 */
bool  TRBEventDecoder::IsTrailer(uint32_t data, uint32_t &checksum)
{
  bool isTrailer = false;
  if ((data & MASK_TRBFRAME) == TRB_TRAILER){
    isTrailer = true;
    checksum = data & MASK_CRC;
  }
  return isTrailer;
}

/** \brief Checks if pased data containes a TRB Data word: TODO: currently we look for a SCT header to bet BCID, as TRB data word is not sent
 */
bool TRBEventDecoder::IsTRBData(uint32_t data, unsigned int &BCID)
{
  bool isTRBData = false;
  if ((data & MASK_WORDTYPE) == WORDTYPE_TRBDATA && (data & MASK_TRBDATATYPE) == TRBDATATYPE_BCID) {
    BCID = data & MASK_BCID;
    isTRBData = true;
  }
  
  return isTRBData;
}

/** \brief Check TRB word for TRB ERROR
 */
bool TRBEventDecoder::HasError(uint32_t data, uint16_t &error)
{
  bool hasError = false;
  if (((data & MASK_WORDTYPE) == WORDTYPE_TRBDATA) && ((data & MASK_TRBDATATYPE) == TRBDATATYPE_TRBERROR)) {
    hasError = true;
    error = data & MASK_ERROR;
  }
  return hasError;
}

bool TRBEventDecoder::IsModuleError(uint32_t data, uint16_t &error)
{
  bool hasError = false;
  if (((data & MASK_WORDTYPE) == WORDTYPE_TRBDATA) && ( ((data & MASK_TRBDATATYPE) == TRBDATATYPE_MODULEERROR_LED) ||  ((data & MASK_TRBDATATYPE) == TRBDATATYPE_MODULEERROR_LEDX)))  {
    hasError = true;
    error = data & MASK_ERROR;
  }
  return hasError;
}

/** \brief Conversion of TRB Error codes to human readable strings
 */
std::string TRBEventDecoder::TRBErrorToString(uint16_t error)
{
  std::string errString = "Unknown error";
  switch(error) {
    case 0x0:
      errString = "L2 State machine Timeout (from L1A to missing module preamble in L1-FIFO)";
      break;
    case 0x1:
      errString = "L1A trigger detected while L1-FIFOs not empty";
      break;
    case 0x2:
      errString = "L2 State machine Timeout (from last word read in L1-FIFO to missing module trailer in L1-FIFO)";
      break;
    case 0x8:
      errString = "L2 Fifo Full";
      break;
  }
  
  return errString;
}

/** \brief Return frame count stored in TRB word.
*/
int TRBEventDecoder::GetTRBFrameCnt(uint32_t data)
{
  return int((data & MASK_FRAMECNT) >> RSHIFT_FRAMECNT);
}

/** \brief Looks at data stored in m_data and decodes TRB words.
    Module data is copied in m_moduleData_led and m_moduleData_ledx (separated by data lines), 24 bits in each 32bit word.
 */
TRBEvent *TRBEventDecoder::DecodeData(bool verbose, bool sinlgeEvent)
{
  if (m_PrintEventInformation == 0) { verbose = false;}
 
  //std::cout << "In DecodeData() "<<std::endl;
  
  TRBEvent *trbEvent = nullptr;
  std::unique_ptr<FletcherChecksum> l_checksum (new FletcherChecksum());
  if (m_data.size() == 0){
    if (m_dataBytes.size() > 0){
      // convert byte array into 32-bit words
      for (unsigned int i = 0; i+3 < m_dataBytes.size(); i += 4){
        uint32_t inputBuffer = 0;
        inputBuffer |= (m_dataBytes[i+3] & 0xff);
        inputBuffer = inputBuffer << 8;
        inputBuffer |= m_dataBytes[i+2] & 0xff;
        inputBuffer = inputBuffer << 8;
        inputBuffer |= m_dataBytes[i+1] & 0xff;
        inputBuffer = inputBuffer << 8;
        inputBuffer |= m_dataBytes[i] & 0xff;
        
        // std::cout << "32 bit word = 0x"<<std::hex<<inputBuffer<<std::endl;
        m_data.push_back(inputBuffer);
      }
      
    } else {
      WARNING("No data present. Please load data first");
      return nullptr;
    }
  }
  
  
  for (unsigned int i = 0; i < 8; i++){
    m_moduleData_led[i].clear();
    m_moduleData_ledx[i].clear();
  }
  
  auto time_start = std::chrono::high_resolution_clock::now();
  auto time_end = std::chrono::high_resolution_clock::now();
  long int eventCnt = 0;

  int frameCnt = -2; // first event
  
  for (unsigned int i = 0; i < m_data.size(); i++) {
    
    if (m_PrintEventInformation > 3) DEBUG("| Analysing word: 0x"<<std::hex<<m_data[i]);
    
    if (frameCnt == -2) frameCnt = ((m_data[i] & MASK_FRAMECNT) >> RSHIFT_FRAMECNT) -1; // first event, framecount does not need to start with 0
    if (frameCnt == 7) frameCnt = -1; // 3 bit frame counter
    frameCnt++;
    if (static_cast<unsigned int>(frameCnt) != (m_data[i] & MASK_FRAMECNT) >> RSHIFT_FRAMECNT){
      ERROR("Expected FrameCount = "<<frameCnt<<" But found: "<<(int)((m_data[i] & MASK_FRAMECNT) >> RSHIFT_FRAMECNT)<<"! Missing Data?  data index = "<<i);
      frameCnt = (m_data[i] & MASK_FRAMECNT) >> RSHIFT_FRAMECNT;
    }
    
    if (m_PrintEventInformation > 3) DEBUG(" frame = "<< std::dec<<frameCnt<<"  ");
    
    if (m_data[i] == TRB_END || m_data[i] == 0xCAFEDECA) {
       if (verbose) DEBUG("EOD: Found TRB end of DAQ word. nothing expected after this");
       return nullptr;
    }
    
    if ((m_data[i] & MASK_TRBFRAME) == TRB_HEADER) {
      l_checksum->InitialiseChecksum();
      l_checksum->AddData(m_data[i]);
      eventCnt ++;
      if (eventCnt % 10000 == 0){
         time_end = std::chrono::high_resolution_clock::now();
         auto duration = std::chrono::duration_cast<std::chrono::microseconds>(time_end-time_start);
         DEBUG(" LoadData processing speed: "<< 10.0e6/duration.count()<<"kHz");
         time_start = std::chrono::high_resolution_clock::now();
      }
      if (trbEvent == nullptr || m_Events.back() ==  trbEvent){
        if (trbEvent != nullptr) { ERROR("Found an existing trbEvent pointer where there should be none. Probably there's a missing TRBTRAILER word? This is a symptom of data corruption. Watch out for TRB ERRORs ... #FindMe# ");}
        trbEvent = new TRBEvent( (m_data[i] & MASK_EVNTCNT), 0);
      } else {
        trbEvent->SetL1ID(m_data[i] & MASK_EVNTCNT);
      }

      //DEBUG("Adding event A "<<(m_data[i] & MASK_EVNTCNT));
      m_Events.push_back(trbEvent);
      if (m_PrintEventInformation > 1) DEBUG(" Found TRB header at pos "<<i<< " with EVENT CNT = "<<(m_data[i] & MASK_EVNTCNT));
      continue;
    }
 
    if ((m_data[i] & MASK_TRBFRAME) == TRB_TRAILER) {
     uint32_t checksum = m_data[i] & MASK_CRC;
     if (checksum == l_checksum->ReturnChecksum()){ trbEvent->SetIsChecksumValid(true);}

     if (m_PrintEventInformation > 1) {
       DEBUG(" Found TRB trailer at pos "<<i);
       DEBUG(" Checksum from data: 0x" << std::hex << checksum << ", computed checksum: 0x" << l_checksum->ReturnChecksum() << std::dec);
     }
     if (sinlgeEvent){
        // DEBUG("DecodeData(): done, returning single event");
        return trbEvent;
     } else {
         for (unsigned int i = 0; i < 8; i++){
            DecodeModuleData(i, trbEvent, nullptr);
            for (unsigned int i = 0; i < 8; i++){
               m_moduleData_led[i].clear();
               m_moduleData_ledx[i].clear();
            }
         }
       trbEvent = nullptr;
    }
      continue;
    }
    if ((m_data[i] & MASK_WORDTYPE) == WORDTYPE_TRBDATA) {
      l_checksum->AddData(m_data[i]);

      if (m_PrintEventInformation > 2) DEBUG(" |- Found TRB Data word at pos "<<i);
      
      switch ((m_data[i] & MASK_TRBDATATYPE)) {
        case TRBDATATYPE_BCID:
          if (verbose) DEBUG(" |-- BCID = " << (m_data[i] & MASK_BCID));
          trbEvent->SetBCID((m_data[i] & MASK_BCID));
          break;
        case TRBDATATYPE_TRBERROR:
          if (verbose) DEBUG(" |-- TRB ERROR = " << (m_data[i] & MASK_ERROR)<<" data index "<<i);
          DEBUG("Adding TRB ERROR "<< (int)(m_data[i] & MASK_ERROR)<<" ..");
          if (trbEvent == nullptr) { trbEvent = new TRBEvent(0, 0); }
          trbEvent->AddError(m_data[i] & MASK_ERROR);
          //DEBUG("                 .. done adding TRB ERROR");
          break;
        case TRBDATATYPE_MODULEERROR_LED:
        case TRBDATATYPE_MODULEERROR_LEDX:
          if (verbose) DEBUG(" |-- Module ERROR for module "<<  ((m_data[i] & MASK_TRBDATA_MODULEID)>>SHIFT_TRBDATA_MODULEID) << " = " << (m_data[i] & MASK_ERROR));
          break;
        default:
          ERROR("Unknown word type discoverd. This is a sign of data corruption!");
      }
      continue;
    }
    
    if ((m_data[i] & MASK_WORDTYPE) == WORDTYPE_MODULEDATA_LED){
      l_checksum->AddData(m_data[i]);

      if (m_PrintEventInformation > 3){
      std::cout<<" |- Found Module Data: Frame = "<< std::dec<<((m_data[i] & MASK_FRAMECNT) >> RSHIFT_FRAMECNT)
      <<  " module = " << std::dec << ((m_data[i] & MASK_MODULEDATA_MODULEID) >> RSHIFT_MODULEDATA_MODULEID)
      <<  " data line = led"
      <<  " data = 0x"<<std::hex << (m_data[i] & MASK_MODULEDATA)
      << std::endl;
      }
      uint32_t moduleData = (m_data[i] & MASK_MODULEDATA); // 24 useful bits;
      m_moduleData_led[((m_data[i] & MASK_MODULEDATA_MODULEID) >> RSHIFT_MODULEDATA_MODULEID)].push_back(moduleData); // 24-bits of module data are delivered. Might want to align this on 32bit boundaries later.
      
      continue;
    }
    if ((m_data[i] & MASK_WORDTYPE) == WORDTYPE_MODULEDATA_LEDX) {
      l_checksum->AddData(m_data[i]);

      if (m_PrintEventInformation > 3) {
      std::cout<<" |- Found Module Data: Frame = "<< std::dec<<((m_data[i] & MASK_FRAMECNT) >> RSHIFT_FRAMECNT)
      <<  " module = " << std::dec << ((m_data[i] & MASK_MODULEDATA_MODULEID) >> RSHIFT_MODULEDATA_MODULEID)
      <<  " data line = ledx"
      <<  " data = 0x"<<std::hex << (m_data[i] & MASK_MODULEDATA)
      << std::endl;
      }
      uint32_t moduleData = (m_data[i] & MASK_MODULEDATA); // 24 useful bits;
      m_moduleData_ledx[((m_data[i] & MASK_MODULEDATA_MODULEID) >> RSHIFT_MODULEDATA_MODULEID)].push_back(moduleData); // 24-bits of module data are delivered. Might want to align this on 32bit boundaries later.
      
      continue;
    }
 
    
    // This should never be reached
    l_checksum->AddData(m_data[i]);
    ERROR("Unknown data word from TRB: 0x"<<std::hex<<m_data[i]);
  }
  
  //std::cout << "DecodeData(): done"<<std::endl;
  
  return nullptr;
}


/** \brief Decode internally stored module data.
 */
void TRBEventDecoder::DecodeModuleData(unsigned int module, TRBEvent *trbEvent, std::vector < std::vector <uint16_t> > *moduleConfig)
{
  try {
    if (m_PrintEventInformation > 0) DEBUG("Decoding module data from led data line: ");
    DecodeModuleData(module, m_moduleData_led, trbEvent, moduleConfig);
    if (m_PrintEventInformation > 0)DEBUG("Decoding module data from ledx data line: ");
    DecodeModuleData(module, m_moduleData_ledx, trbEvent, moduleConfig);
  } catch (...) {
    ERROR("Caught an exception ");
    throw;
  }
}

/** \brief Decode internally stored module data.
 */
void TRBEventDecoder::DecodeModuleData(unsigned int module, std::vector < std::vector <uint16_t> > *moduleConfig)
{
  try {
    if (m_PrintEventInformation > 0) DEBUG("Decoding module data from led data line: ");
    DecodeModuleData(module, m_moduleData_led, true, moduleConfig);
    if (m_PrintEventInformation > 0)DEBUG("Decoding module data from ledx data line: ");
    DecodeModuleData(module, m_moduleData_ledx, false, moduleConfig);
  } catch (...) {
    ERROR("Caught an exception ");
    throw;
  }
}

/** \brief Expecting 24-bit of payload data stored in each 32-bit word containg exactly 1 event.
 * Event data from data lines led and ledx (or in general data from different FE chips) may be processed in consecutive calles to this dunction.
 * Module data is preceeded by a 19 bit header. Followed by either:
 * ERROR word, Configuration register content, data or a no hit data tag.
 * SCTEvent object are newly created, and assigned to the passed TRBEvent object which is responisble for deleting the SCTEvent objects.
 * \param module: TRB channel to be decoded
 * \param moduleData: vector of 24-bit payload data
 * \param event: TRB event where the module data should be added to. If called for the second time with the same TRB event consistency checks on module L1ID and BCID will be made automatically.
 */
void TRBEventDecoder::DecodeModuleData(unsigned int module, std::vector< std::vector<uint32_t> > moduleData, TRBEvent *trbEvent, std::vector< std::vector <uint16_t> > *moduleConfig)
{
  if (module > 7){
    ERROR("Only modules 0-7 if this TRB are available. You specified module "<< module );
    return;
  }
  if (moduleData.at(module).size() == 0){
    // DEBUG("WARNING: no module data present. Please load data first");
    return;
  }
  if (trbEvent == nullptr){
    ERROR("No TRB event passed to fill Module data into");
    return;
  }
  
  
  if (m_PrintEventInformation > 0) DEBUG("Analysing "<<std::dec<<moduleData.at(module).size()<<" words of module data");
  
  SCTEvent *sctEvent = nullptr;
  
  bool praeambleFound = false;
  Bitstream bitstream(moduleData.at(module));
  int removedBits = 0;
  
  auto time_start = std::chrono::high_resolution_clock::now();
  auto time_end = std::chrono::high_resolution_clock::now();
  long int eventCount = 0;
  while (bitstream.BitsAvailable()){
    // DEBUG("Data word = "<<std::hex << bitstream.GetWord32() );
    if ((bitstream.GetWord32() & MASK_MODULE_HEADER) == TAG_MODULE_HEADER){
      eventCount++;
      if (eventCount %5000 == 0 ){
        time_end = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(time_end-time_start);
        DEBUG("Decodeding module data for evnt: "<<eventCount<< " rate = "<<5.0e6/duration.count()<<" kHz");
        time_start = std::chrono::high_resolution_clock::now();
      }
      unsigned int l1id = ((bitstream.GetWord32() >> RSHIFT_MODULE_L1ID)&MASK_MODULE_L1ID);
      unsigned int bcid = ((bitstream.GetWord32() >> RSHIFT_MODULE_BCID)&MASK_MODULE_BCID);
      
      if (m_PrintEventInformation > 1) DEBUG("Module Header: L1D = "<<std::dec<< l1id <<" BCID = "<< bcid);
      if (m_PrintEventInformation > 0) DEBUG("     removed bits bfore finding Module Header = "<<removedBits);
      
      sctEvent = trbEvent->GetModule(module);
      if (sctEvent == nullptr){
        if (m_PrintEventInformation > 2) DEBUG("Adding SCTEvent for module "<< module << " to TRBEvent ");
        sctEvent = new SCTEvent(module, l1id, bcid);
        trbEvent->AddModule(module, sctEvent);
      } else {
        sctEvent->AddHeader(l1id, bcid);
      }
      removedBits = 0;
      bitstream.RemoveBits(19);
      praeambleFound = true;
      continue;
    }
    if (!praeambleFound){
      bitstream.RemoveBits(1);
      removedBits++;
      continue;
    }
    if ((bitstream.GetWord32() & MASK_MODULE_TRAILER) == TAG_MODULE_TRAILER){
      if (m_PrintEventInformation > 1) DEBUG("Module Trailer ");
      bitstream.RemoveBits(16);
      praeambleFound = false;
      continue;
    }
    if ((bitstream.GetWord32() & MASK_MODULE_ERROR) == TAG_MODULE_ERROR && praeambleFound){
      unsigned int chip = ((bitstream.GetWord32() >> RSHIFT_CHIPADD_ERR)&MASK_CHIPADD_ERR);
      unsigned int err = ((bitstream.GetWord32() >> RSHIFT_ERR)&MASK_ERR);
      if (m_PrintEventInformation > 0) DEBUG("Module Data: ERROR code 0x"<<std::hex<< err << " for chip "<<std::dec<< chip );
      sctEvent->AddError(chip, err);
      bitstream.RemoveBits(11);
      continue;
    }
    if ((bitstream.GetWord32() & MASK_MODULE_CONFIG) == TAG_MODULE_CONFIG && praeambleFound){
      if (moduleConfig != nullptr){
        unsigned int chip = ((bitstream.GetWord32() >> RSHIFT_CHIPADD_CONF)&MASK_CHIPADD_CONF);
        uint16_t config = ((bitstream.GetWord32() >> RSHIFT_CONF1)&MASK_CONF) << 8;
        config |= ((bitstream.GetWord32() >> RSHIFT_CONF2)&MASK_CONF);
        if (moduleConfig->size() > chip){
          (*moduleConfig).at(chip).push_back(config);
        } else {
          ERROR("Unexpected (4-bit) chip id 0x"<<std::hex<<chip);
        }
      }
      if (m_PrintEventInformation > 2) {
        DEBUG("Module Data: CONFIG for chip "<<std::dec<< ((bitstream.GetWord32() >> RSHIFT_CHIPADD_CONF)&MASK_CHIPADD_CONF)
        << " conf = 0x"<<std::setw(2)<<std::setfill('0')<<std::hex<<((bitstream.GetWord32() >> RSHIFT_CONF1)&MASK_CONF)<<std::setw(2)<<std::setfill('0')<<((bitstream.GetWord32() >> RSHIFT_CONF2)&MASK_CONF) );
      }
      bitstream.RemoveBits(28);
      continue;
    }
    if ((bitstream.GetWord32() & MASK_MODULE_DATA) == TAG_MODULE_DATA && praeambleFound){
      unsigned int chip = ((bitstream.GetWord32() >> RSHIFT_CHIPADD_DATA)&MASK_CHIPADD_DATA);
      unsigned int channel = ((bitstream.GetWord32() >> RSHIFT_CHANNEL_DATA)&MASK_CHANNEL_DATA);
      if (m_PrintEventInformation > 5) { DEBUG("Bitstream analysed next: 0x"<<std::hex<<bitstream.GetWord32());}
      if (m_PrintEventInformation > 4) { DEBUG("Module Data: DATA for chip " << std::dec<< chip << " channel = "<< std::dec<< channel); }
      bitstream.RemoveBits(13); // after that we expect n-times <1><xxx> => check MSB to be 1
      while ( ((bitstream.GetWord32() & 0x80000000) == 0x80000000) ){ // data packet
        // !!! Trailer looks the same in this definition! => Always check if a trailer is found
        if ((bitstream.GetWord32() & MASK_MODULE_TRAILER) == TAG_MODULE_TRAILER){
          if (m_PrintEventInformation > 2) DEBUG("Module Trailer ");
          bitstream.RemoveBits(16);
          praeambleFound = false;
          continue;
        }
        unsigned int hit = (bitstream.GetWord32() >> 28) & 0x7;
        sctEvent->AddHit(chip, channel, hit);
        if (m_PrintEventInformation > 4) DEBUG("  Hit pattern = "<<hit);
        bitstream.RemoveBits(4);
      }
      continue;
    }
    if ((bitstream.GetWord32() & MASK_MODULE_NODATA) == TAG_MODULE_NODATA && praeambleFound) {
      if (m_PrintEventInformation > 4) DEBUG("  No Hit packet");
      bitstream.RemoveBits(3);
      continue;
    }
    // data is only valid after a praeamble (aka header) was found. Otherwise praeamble might be mistaken as an error code
    praeambleFound = false;
    // std::cout << "WARNING: unable to decode bitstream. Removing 1 bit until alignment is found again."<<std::endl;
    bitstream.RemoveBits (1);
  }
  
}


/** \brief Expecting 24-bit of payload data stored in each 32-bit word.
 * Module data is preceeded by a 19 bit header. Followed by either:
 * ERROR word, Configuration register content, data or a no hit data tag.
 */
void TRBEventDecoder::DecodeModuleData(unsigned int module, std::vector< std::vector<uint32_t> > moduleData, bool firstPass, std::vector< std::vector <uint16_t> > *moduleConfig)
{
  if (module > 7){
    ERROR("Only modules 0-7 if this TRB are available. You specified module "<< module );
    return;
  }
  if (moduleData.at(module).size() == 0){
    // std::cout << "WARNING: no module data present. Please load data first"<<);
    return;
  }
 
   if (m_PrintEventInformation > 0) DEBUG("Analysing "<<std::dec<<moduleData.at(module).size()<<" words of module data");
  
  SCTEvent *sctEvent=nullptr;
  TRBEvent *trbEvent;
  
  bool praeambleFound = false;
  Bitstream bitstream(moduleData.at(module));
  int removedBits = 0;

  auto time_start = std::chrono::high_resolution_clock::now();
  auto time_end = std::chrono::high_resolution_clock::now();
  long int eventCount = 0;
  while (bitstream.BitsAvailable()){
    // std::cout << "Data word = "<<std::hex << bitstream.GetWord32());
    if ((bitstream.GetWord32() & MASK_MODULE_HEADER) == TAG_MODULE_HEADER){
      eventCount++;
      if (eventCount %5000 == 0 ){ 
        time_end = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(time_end-time_start);
        DEBUG("Decodeding module data for evnt: "<<eventCount<< " rate = "<<5.0e6/duration.count()<<" kHz");
        time_start = std::chrono::high_resolution_clock::now();
      }
      unsigned int l1id = ((bitstream.GetWord32() >> RSHIFT_MODULE_L1ID)&MASK_MODULE_L1ID);
      unsigned int bcid = ((bitstream.GetWord32() >> RSHIFT_MODULE_BCID)&MASK_MODULE_BCID);
      
       if (m_PrintEventInformation > 1) DEBUG("Module Header: L1D = "<<std::dec<< l1id <<" BCID = "<< bcid);
       if (m_PrintEventInformation > 0) DEBUG("     removed bits bfore finding Module Header = "<<removedBits);
      
      if (firstPass){ // process first half of module data (led / ledx line)
        if (m_Events.size() == 0) { // first event
          trbEvent = new TRBEvent(l1id, bcid);
        } else { // this is a scan, expecting consecutive triggers
          unsigned int lastL1ID = m_Events.back()->GetModule(module)->GetL1ID();
          if (l1id == 0) { // expected lastL1ID to be 15
            while (lastL1ID < 15) { // fill incomplete events until lastL1ID == 15
              // DEBUG(" adding dummy event with L1ID "<<lastL1ID+1<< " l1id from module = "<< l1id );
              trbEvent = new TRBEvent(m_Events.back()->GetL1ID()+1, 0x0);
              sctEvent = new SCTEvent(module, lastL1ID+1, 0x0);
              sctEvent->SetMissingData();
              trbEvent->AddModule(module, sctEvent);
              m_Events.push_back(trbEvent);
              lastL1ID = m_Events.back()->GetModule(module)->GetL1ID();
            }
          } else {
            while (lastL1ID < l1id-1) { // fill incomplete events until lastL1ID == l1id - 1
              // DEBUG(" adding dummy event with lastL1ID +1 = "<<lastL1ID+1<< " l1id from module = "<< l1id << " events in vector = "<<m_Events.size());
              trbEvent = new TRBEvent(m_Events.back()->GetL1ID()+1, 0x0);
              sctEvent = new SCTEvent(module, lastL1ID+1, 0x0);
              sctEvent->SetMissingData();
              trbEvent->AddModule(module, sctEvent);
              m_Events.push_back(trbEvent);
              // DEBUG("   stored L1ID: from trbEvent "<< m_Events.back()->GetModule(module)->GetL1ID()<< " from sctEvent = " << sctEvent->GetL1ID());
              lastL1ID = m_Events.back()->GetModule(module)->GetL1ID();
            }
          }
          trbEvent = new TRBEvent(m_Events.back()->GetL1ID()+1, bcid);
        }
        sctEvent = new SCTEvent(module, l1id, bcid);
        trbEvent->AddModule(module, sctEvent);
        m_Events.push_back(trbEvent);
      } else { // process second half of module data (led / ledx line)
        for (auto event : m_Events){
          // find last incomplete event
          if (!event->GetModule(module)->IsComplete()){
            trbEvent = event;
            sctEvent = event->GetModule(module);
            if (sctEvent->GetL1ID() != l1id){
              //DEBUG("ERROR: data streams from led and ledx lines out of sync: L1ID/BCID ledx: "<<l1id << " / "<<bcid
              //<< " led: "<<trbEvent->GetL1ID()<< " / "<<trbEvent->GetBCID());
              event->GetModule(module)->SetComplete();
              event->GetModule(module)->SetMissingData();
              continue;
            }
            event->GetModule(module)->SetComplete();
            if (sctEvent->GetBCID() != bcid){
              sctEvent->SetBCIDMismatch();
            }
            break;
          }
        }
      }
      removedBits = 0;
      bitstream.RemoveBits(19);
      praeambleFound = true;
      continue;
    }
    if (!praeambleFound){
      bitstream.RemoveBits(1);
      removedBits++;
      continue;
    }
    if ((bitstream.GetWord32() & MASK_MODULE_TRAILER) == TAG_MODULE_TRAILER){
      if (m_PrintEventInformation > 1) DEBUG("Module Trailer ");
      bitstream.RemoveBits(16);
      praeambleFound = false;
      continue;
    }
    if ((bitstream.GetWord32() & MASK_MODULE_ERROR) == TAG_MODULE_ERROR && praeambleFound){
      unsigned int chip = ((bitstream.GetWord32() >> RSHIFT_CHIPADD_ERR)&MASK_CHIPADD_ERR);
      unsigned int err = ((bitstream.GetWord32() >> RSHIFT_ERR)&MASK_ERR);
      if (m_PrintEventInformation > 0) DEBUG("Module Data: ERROR code 0x"<<std::hex<< err << " for chip "<<std::dec<< chip);
      sctEvent->AddError(chip, err);
      bitstream.RemoveBits(11);
      continue;
    }
    if ((bitstream.GetWord32() & MASK_MODULE_CONFIG) == TAG_MODULE_CONFIG && praeambleFound){
      if (moduleConfig != nullptr){
        unsigned int chip = ((bitstream.GetWord32() >> RSHIFT_CHIPADD_CONF)&MASK_CHIPADD_CONF);
        uint16_t config = ((bitstream.GetWord32() >> RSHIFT_CONF1)&MASK_CONF) << 8;
        config |= ((bitstream.GetWord32() >> RSHIFT_CONF2)&MASK_CONF);
        if (moduleConfig->size() > chip){
          (*moduleConfig).at(chip).push_back(config);
        } else {
          ERROR("Unexpected (4-bit) chip id 0x"<<std::hex<<chip);
        }
      }
      if (m_PrintEventInformation > 2) {
        DEBUG("Module Data: CONFIG for chip "<<std::dec<< ((bitstream.GetWord32() >> RSHIFT_CHIPADD_CONF)&MASK_CHIPADD_CONF)
      << " conf = 0x"<<std::setw(2)<<std::setfill('0')<<std::hex<<((bitstream.GetWord32() >> RSHIFT_CONF1)&MASK_CONF)<<std::setw(2)<<std::setfill('0')<<((bitstream.GetWord32() >> RSHIFT_CONF2)&MASK_CONF) );
      }
      bitstream.RemoveBits(28);
      continue;
    }
    if ((bitstream.GetWord32() & MASK_MODULE_DATA) == TAG_MODULE_DATA && praeambleFound){
      unsigned int chip = ((bitstream.GetWord32() >> RSHIFT_CHIPADD_DATA)&MASK_CHIPADD_DATA);
      unsigned int channel = ((bitstream.GetWord32() >> RSHIFT_CHANNEL_DATA)&MASK_CHANNEL_DATA);
      if (m_PrintEventInformation > 5) { DEBUG("Bitstream analysed next: 0x"<<std::hex<<bitstream.GetWord32());}
      if (m_PrintEventInformation > 4) { DEBUG("Module Data: DATA for chip " << std::dec<< chip << " channel = "<< std::dec<< channel ); }
      bitstream.RemoveBits(13); // after that we expect n-times <1><xxx> => check MSB to be 1
      while ( ((bitstream.GetWord32() & 0x80000000) == 0x80000000) ){ // data packet
        // !!! Trailer looks the same in this definition! => Always check if a trailer is found
        if ((bitstream.GetWord32() & MASK_MODULE_TRAILER) == TAG_MODULE_TRAILER){
          if (m_PrintEventInformation > 1) DEBUG("Module Trailer ");
          bitstream.RemoveBits(16);
          praeambleFound = false;
          
          continue;
        }
        unsigned int hit = (bitstream.GetWord32() >> 28) & 0x7;
        sctEvent->AddHit(chip, channel, hit);
        if (m_PrintEventInformation > 4) DEBUG("  Hit pattern = "<<hit);
        bitstream.RemoveBits(4);
      }
      continue;
    }
    if ((bitstream.GetWord32() & MASK_MODULE_NODATA) == TAG_MODULE_NODATA && praeambleFound) {
      if (m_PrintEventInformation > 4) DEBUG("  No Hit packet");
      bitstream.RemoveBits(3);
      continue;
    }
    // data is only valid after a praeamble (aka header) was found. Otherwise praeamble might be mistaken as an error code
    praeambleFound = false;
    // std::cout << "WARNING: unable to decode bitstream. Removing 1 bit until alignment is found again."<<std::endl;
    bitstream.RemoveBits (1);
  }
  
}


/** \brief Decode internally stored module data.
 */
bool TRBEventDecoder::ModuleDataHasHeader(unsigned int &L1ID,  unsigned int &BCID)
{
  int foundHeaders = 0;
  int foundTrailers = 0;
  
  int module = 0; //  hardcoded for now TODO
  
  bool praeambleFound = false;
  Bitstream bitstream(m_moduleData_led[module]);
  Bitstream bitstream_2(m_moduleData_ledx[module]);
  
  bitstream.Add(bitstream_2);
  
  
  int removedBits = 0;
  while (bitstream.BitsAvailable()){
    //std::cout << "Data word = "<<std::hex << bitstream.GetWord32() <<std::endl;
    //std::cout << "Bits available = "<<bitstream.NBitsAvailable()<< std::endl;
    if ((bitstream.GetWord32() & MASK_MODULE_HEADER) == TAG_MODULE_HEADER){
      L1ID =  ((bitstream.GetWord32() >> RSHIFT_MODULE_L1ID)&MASK_MODULE_L1ID);
      BCID =  ((bitstream.GetWord32() >> RSHIFT_MODULE_BCID)&MASK_MODULE_BCID);
      removedBits = 0;
      bitstream.RemoveBits(19);
      praeambleFound = true;
      foundHeaders++;
      continue;
    }
    if (!praeambleFound){
      bitstream.RemoveBits(1);
      removedBits++;
      continue;
    }
    if ((bitstream.GetWord32() & MASK_MODULE_TRAILER) == TAG_MODULE_TRAILER){
      foundTrailers++;
      bitstream.RemoveBits(16);
      praeambleFound = false;
      continue;
    }
    if ((bitstream.GetWord32() & MASK_MODULE_ERROR) == TAG_MODULE_ERROR && praeambleFound){
      //std::cout << "Module Data: ERROR code 0x"<<std::hex<<((bitstream.GetWord32() >> RSHIFT_ERR)&MASK_ERR)
      //<< " for chip "<<std::dec<< ((bitstream.GetWord32() >> RSHIFT_CHIPADD_ERR)&MASK_CHIPADD_ERR)<<std::endl;
      bitstream.RemoveBits(11);
      continue;
    }
    if ((bitstream.GetWord32() & MASK_MODULE_CONFIG) == TAG_MODULE_CONFIG && praeambleFound){
      //std::cout << "Module Data: CONFIG for chip "<<std::dec<< ((bitstream.GetWord32() >> RSHIFT_CHIPADD_CONF)&MASK_CHIPADD_CONF)
      //<< " conf = 0x"<<std::setw(2)<<std::setfill('0')<<std::hex<<((bitstream.GetWord32() >> RSHIFT_CONF1)&MASK_CONF)<<std::setw(2)<<std::setfill('0')<<((bitstream.GetWord32() >> RSHIFT_CONF2)&MASK_CONF) <<std::endl;
      bitstream.RemoveBits(28);
      continue;
    }
    if ((bitstream.GetWord32() & MASK_MODULE_DATA) == TAG_MODULE_DATA && praeambleFound){
      //std::cout << "Module Data: DATA for chip "
      //<< std::dec<< ((bitstream.GetWord32() >> RSHIFT_CHIPADD_DATA)&MASK_CHIPADD_DATA)
      //<< " channel = "<< std::dec<< ((bitstream.GetWord32() >> RSHIFT_CHANNEL_DATA)&MASK_CHANNEL_DATA)
      //<< std::endl;
      bitstream.RemoveBits(13); // after that we expect n-times <1><xxx> => check MSB to be 1
      while ( ((bitstream.GetWord32() & 0x80000000) == 0x80000000) ){ // data packet
        if ((bitstream.GetWord32() & MASK_MODULE_TRAILER) == TAG_MODULE_TRAILER){
          foundTrailers++;
          bitstream.RemoveBits(16);
          praeambleFound = false;
          continue;
        }
        //int hit = (bitstream.GetWord32() >> 28) & 0x7;
        //std::cout << "  Hit pattern = "<<hit<<std::endl;
        bitstream.RemoveBits(4);
      }
      continue;
    }
    if ((bitstream.GetWord32() & MASK_MODULE_NODATA) == TAG_MODULE_NODATA && praeambleFound) {
      //std::cout << "  No Hit packet"<<std::endl;
      bitstream.RemoveBits(3);
      continue;
    }
    // data is only valid after a praeamble (aka header) was found. Otherwise praeamble might be mistaken as an error code
    praeambleFound = false;
    // std::cout << "WARNING: unable to decode bitstream. Removing 1 bit until alignment is found again."<<std::endl;
    bitstream.RemoveBits (1);
    // std::cout << "Bits available = "<<bitstream.NBitsAvailable()<< std::endl;
  }
  
  if (foundHeaders > 0){
    return true;
  }
  return false;
}

/** \brief Count Number of trailers in bitstream
 */
void TRBEventDecoder::CountHeaderTrailer( Bitstream bitstream, int &foundHeaders, int &foundTrailers)
{
  //int L1ID, BCID;
  
  //int module = 0; //  hardcoded for now TODO
  
  bool praeambleFound = false;
  
  int removedBits = 0;
  while (bitstream.BitsAvailable()){
    // std::cout << "BitsAvailable = "<< bitstream.NBitsAvailable()<<"   HasTrailer - Data word = "<<std::hex << bitstream.GetWord32() <<std::endl;
    if ((bitstream.GetWord32() & MASK_MODULE_HEADER) == TAG_MODULE_HEADER){
      //L1ID =  ((bitstream.GetWord32() >> RSHIFT_MODULE_L1ID)&MASK_MODULE_L1ID);
      //BCID =  ((bitstream.GetWord32() >> RSHIFT_MODULE_BCID)&MASK_MODULE_BCID);
      removedBits = 0;
      bitstream.RemoveBits(19);
      praeambleFound = true;
      foundHeaders++;
      continue;
    }
    if (!praeambleFound){
      bitstream.RemoveBits(1);
      removedBits++;
      continue;
    }
    if ((bitstream.GetWord32() & MASK_MODULE_TRAILER) == TAG_MODULE_TRAILER){
      foundTrailers++;
      bitstream.RemoveBits(16);
      praeambleFound = false;
      continue;
    }
    if ((bitstream.GetWord32() & MASK_MODULE_ERROR) == TAG_MODULE_ERROR && praeambleFound){
      //std::cout << "Module Data: ERROR code 0x"<<std::hex<<((bitstream.GetWord32() >> RSHIFT_ERR)&MASK_ERR)
      //<< " for chip "<<std::dec<< ((bitstream.GetWord32() >> RSHIFT_CHIPADD_ERR)&MASK_CHIPADD_ERR)<<std::endl;
      bitstream.RemoveBits(11);
      continue;
    }
    if ((bitstream.GetWord32() & MASK_MODULE_CONFIG) == TAG_MODULE_CONFIG && praeambleFound){
      //std::cout << "Module Data: CONFIG for chip "<<std::dec<< ((bitstream.GetWord32() >> RSHIFT_CHIPADD_CONF)&MASK_CHIPADD_CONF)
      //<< " conf = 0x"<<std::setw(2)<<std::setfill('0')<<std::hex<<((bitstream.GetWord32() >> RSHIFT_CONF1)&MASK_CONF)<<std::setw(2)<<std::setfill('0')<<((bitstream.GetWord32() >> RSHIFT_CONF2)&MASK_CONF) <<std::endl;
      bitstream.RemoveBits(28);
      continue;
    }
    if ((bitstream.GetWord32() & MASK_MODULE_DATA) == TAG_MODULE_DATA && praeambleFound){
      //std::cout << "Module Data: DATA for chip "
      //<< std::dec<< ((bitstream.GetWord32() >> RSHIFT_CHIPADD_DATA)&MASK_CHIPADD_DATA)
      //<< " channel = "<< std::dec<< ((bitstream.GetWord32() >> RSHIFT_CHANNEL_DATA)&MASK_CHANNEL_DATA)
      //<< std::endl;
      bitstream.RemoveBits(13); // after that we expect n-times <1><xxx> => check MSB to be 1
      while ( ((bitstream.GetWord32() & 0x80000000) == 0x80000000) ){ // data packet
        if ((bitstream.GetWord32() & MASK_MODULE_TRAILER) == TAG_MODULE_TRAILER){
          foundTrailers++;
          bitstream.RemoveBits(16);
          praeambleFound = false;
          continue;
        }
        //int hit = (bitstream.GetWord32() >> 28) & 0x7;
        //std::cout << "  Hit pattern = "<<hit<<std::endl;
        bitstream.RemoveBits(4);
      }
      continue;
    }
    if ((bitstream.GetWord32() & MASK_MODULE_NODATA) == TAG_MODULE_NODATA && praeambleFound) {
      //std::cout << "  No Hit packet"<<std::endl;
      bitstream.RemoveBits(3);
      continue;
    }
    // data is only valid after a praeamble (aka header) was found. Otherwise praeamble might be mistaken as an error code
    praeambleFound = false;
    //std::cout << "WARNING: unable to decode bitstream. Removing 1 bit until alignment is found again."<<std::endl;
    bitstream.RemoveBits (1);
  }
  
}

/** \brief Decode internally stored module data.
 */
bool TRBEventDecoder::ModuleDataHasTrailer()
{
  int foundHeaders1 = 0;
  int foundTrailers1 = 0;
  int foundHeaders2 = 0;
  int foundTrailers2 = 0;
  
  //int L1ID, BCID;
  
  int module = 0; //  hardcoded for now TODO
  
  //bool praeambleFound = false;
  Bitstream bitstream(m_moduleData_led[module]);
  Bitstream bitstream_2(m_moduleData_ledx[module]);
  // bitstream.Add(bitstream_2);
 
  CountHeaderTrailer(bitstream, foundHeaders1, foundTrailers1);
  CountHeaderTrailer(bitstream_2, foundHeaders2, foundTrailers2);
  
  if ((foundTrailers1 == foundTrailers2) && (foundTrailers1 != 0) ){
    return true;
  } else {
    return false;
  }
}


/* ***************************************************
 \brief: Constructor of bitstream class
 m_currentBuffer always holds a full 32bit word of the bitstream. It is filled in the cosntructor using the first
 two word from the data vector. Eich word conains m_usedBitsPerWord = 24 data bits, aligned at LSB.
 m_bitsUsedOfNextWord stores how many of the 24 bits are already copied to m_currentBuffer.
 * ************************************************* */
Bitstream::Bitstream(std::vector<uint32_t> data) : m_currentBuffer(0), m_bitsUsedOfNextWord(0), m_bitsAvailable(0)
{
  m_bitstreamData = data;
  
  if (m_bitstreamData.size() == 0){
    // std::cout << "WARNING: no data passed to Bitstrem class"<<std::endl;
    m_bitsAvailable = 0;
    return;
  }
  m_currentBuffer = (m_bitstreamData[0] << (32 - m_usedBitsPerWord)); // copy element 0 to buffer
  m_bitstreamData.erase(m_bitstreamData.begin()); // remove element 0
  m_bitsAvailable = (32 - m_usedBitsPerWord);
  // now we fill the remaining bits of the buffer
  if (m_bitsUsedOfNextWord > m_usedBitsPerWord) {
    ERROR("This case is not handeled!");
    throw;
  }
  if (m_bitstreamData.size() == 0){ // no more data
    return;
  }
  m_bitsUsedOfNextWord = (32 - m_usedBitsPerWord);
  m_currentBuffer |= (m_bitstreamData[0] >> ( m_usedBitsPerWord-m_bitsUsedOfNextWord ));
  m_bitsAvailable = 32;
}

/* ***************************************************
 \brief: Removes the n MSB from m_currentBuffer, shifts remaining content by << n and fills the freed space
 with bits from m_bitstreamData. If word 0 is used completely it is removed and the next word used.
 * ************************************************* */
void Bitstream::RemoveBits(unsigned int n)
{
  m_currentBuffer <<= n;
  //std::cout <<"avilable words = " << m_bitstreamData.size()<<"  m_bitsAvailable = "<<std::dec<<m_bitsAvailable<<" bits used: "<<m_bitsUsedOfNextWord<<" removing "<<n<<" bits"<<std::endl;
  if (m_bitstreamData.size() == 0){
    m_bitsAvailable -= n;
    if (m_bitsAvailable < 0) m_bitsAvailable = 0;
    return;
  }
  //  now we have n free bits;
  uint32_t mask = 1;
  for (unsigned int i = 1; i < n; i++){
    mask <<= 1;
    mask |= 1;
  }
  
  if ((m_usedBitsPerWord - m_bitsUsedOfNextWord) >= n){ // still enough bits in m_bitstreamData[0] to use
    // std::cout <<"Enough usable bits in current word"<<std::endl;
    m_currentBuffer |= (m_bitstreamData[0] >> (m_usedBitsPerWord - (m_bitsUsedOfNextWord + n))) & mask;
    m_bitsUsedOfNextWord += n;
    if (m_bitsUsedOfNextWord == m_usedBitsPerWord){ // all bits of word used
      m_bitstreamData.erase(m_bitstreamData.begin());
      m_bitsUsedOfNextWord = 0;
    }
  } else { // need more bits than in next word available
    // std::cout <<"Not enough usable bits in current word. "<<std::endl;
    int missingBits = ( (m_bitsUsedOfNextWord + n) -m_usedBitsPerWord);
    // std::cout << "   Left shift of word by "<<std::dec<<missingBits<<std::endl;
    
    m_currentBuffer |= (m_bitstreamData[0] << ( missingBits)) & mask;
    m_bitstreamData.erase(m_bitstreamData.begin());
    m_bitsUsedOfNextWord = 0;
    if (m_bitstreamData.size() == 0){
      m_bitsAvailable -= n-(m_usedBitsPerWord-m_bitsUsedOfNextWord) ;
      if (m_bitsAvailable < 0) m_bitsAvailable = 0;
      return; // no more data
    }
    // still need to copy n-(m_usedBitsPerWord-m_bitsUsedOfNextWord) bits
    while ((int)(missingBits - m_usedBitsPerWord) > 0){
      // copy full word, aligned with last valid bit
      // std::cout << "   Left shift of word by "<<std::dec<<(missingBits - m_usedBitsPerWord)<<std::endl;
      m_currentBuffer |= (m_bitstreamData[0] << (missingBits - m_usedBitsPerWord) );
      missingBits -= m_usedBitsPerWord;
      m_bitstreamData.erase(m_bitstreamData.begin());
      m_bitsUsedOfNextWord = 0;
      if (m_bitstreamData.size() == 0){
        m_bitsAvailable -= n-(m_usedBitsPerWord-m_bitsUsedOfNextWord) ;
        if (m_bitsAvailable < 0) m_bitsAvailable = 0;
        return; // no more data
      }
    }
    // std::cout << "   Right shift of next word by "<<std::dec<<(m_usedBitsPerWord - missingBits )<<std::endl;
    m_currentBuffer |= (m_bitstreamData[0] >> (m_usedBitsPerWord - missingBits ) );
    m_bitsUsedOfNextWord += (missingBits);
    
  }
}

/** \brief returns a pointer to a Bitstream object containing the raw data of the specidied module.
    Only modules 0-7 connected to one TRB are accessible.
 \param module = module 0-7 connected to one TRB
 */
Bitstream* TRBEventDecoder::GetModuleBitstream(unsigned int module, bool led = true)
{
  if (module > 7){
    ERROR("Only modules 0-7 attached to this TRB are allowed. You requested data for module "<<module);
    return nullptr;
  }
  
  if (led) return (new Bitstream(m_moduleData_led[module]));
  else return  (new Bitstream(m_moduleData_ledx[module]));
  
}


/** ****************************** EventInformation storage ************************************** **/

/** \brief Cleanup
 */
TRBEvent::~TRBEvent()
{
  for (auto it : m_HitsPerModule){
    delete it;
  }
  m_HitsPerModule.clear();
}

/** \brief Adds hit information class for one SCT module to the event. Module ID is mapped to a vector index for fast access
 */
void TRBEvent::AddModule(unsigned int moduleID, SCTEvent *event)
{
  if (event->GetErrors().size() > 13) {ERROR("trying to add broken SCTEventObject "); throw(std::runtime_error( "trying to add broken SCTEventObject" ));}
  m_HitsPerModule.push_back(event);
  m_moduleID_Index.insert(std::pair<unsigned int, unsigned int>(moduleID, m_HitsPerModule.size()-1));
}

/** \brief Adding TRB ERROR code to list
*/
void TRBEvent::AddError(uint16_t error)
{
  //std::cout << "Address of m_Errors object = "<<&m_Errors<<std::endl;
  //std::cout << "AddError(): CurretnVectorSize = "<<m_Errors.size()<<std::endl;
  //std::cout << "TRBEvent::AddError("<<error<<")"<<std::endl;
  m_Errors.push_back(error);
}

/** \brief Returns hit information class for one SCT module from the event.
 */
SCTEvent* TRBEvent::GetModule(unsigned int moduleID)
{
  if (m_moduleID_Index.find(moduleID) == m_moduleID_Index.end()){
    return nullptr;
  }
  return m_HitsPerModule.at(m_moduleID_Index.at(moduleID));
}

/** \brief Cosntructor: created vector evtries for all 12 chips
 */
SCTEvent::SCTEvent (unsigned int moduleID, unsigned int l1id, unsigned int bcid) : m_moduleID(moduleID), m_bcid(bcid), m_l1id(l1id), m_complete(false), m_missingData(false), m_bcidMismatch(false), m_hasError(false)
{
  m_Hits.resize(13);
  std::vector<uint8_t> vec;
  for (unsigned int i = 0; i < 13; i++) m_Errors.push_back(vec);
  
  m_chipIDMap.insert(std::pair<unsigned int, unsigned int>(0x20, 0));
  m_chipIDMap.insert(std::pair<unsigned int, unsigned int>(0x21, 1));
  m_chipIDMap.insert(std::pair<unsigned int, unsigned int>(0x22, 2));
  m_chipIDMap.insert(std::pair<unsigned int, unsigned int>(0x23, 3));
  m_chipIDMap.insert(std::pair<unsigned int, unsigned int>(0x24, 4));
  m_chipIDMap.insert(std::pair<unsigned int, unsigned int>(0x25, 5));
  m_chipIDMap.insert(std::pair<unsigned int, unsigned int>(0x28, 6));
  m_chipIDMap.insert(std::pair<unsigned int, unsigned int>(0x29, 7));
  m_chipIDMap.insert(std::pair<unsigned int, unsigned int>(0x2a, 8));
  m_chipIDMap.insert(std::pair<unsigned int, unsigned int>(0x2b, 9));
  m_chipIDMap.insert(std::pair<unsigned int, unsigned int>(0x2c, 10));
  m_chipIDMap.insert(std::pair<unsigned int, unsigned int>(0x2d, 11));
}

/** \brief Destructor: cleanup
 */
SCTEvent::~SCTEvent()
{
  for (unsigned int i = 0; i < m_Errors.size(); i++){
    m_Errors.at(i).clear();
  }
  m_Errors.clear();
}

/** \brief Add another header to  module. This happens of data from led and ledx lines are processed.
 *  Consistency checks on l1id and bcid are performed. If a bcid mismatch is detected BCIDMismatch()
 *  will return true afterwards. If an l1id mismatch is detected MissingData() will return true.
 *  Events are marked as complete if no mismatch is detected.
 */
void SCTEvent::AddHeader(unsigned int l1id, unsigned int bcid)
{
  if (m_l1id == l1id && m_bcid == bcid){
    this->SetComplete();
    return;
  }
  if (m_l1id != l1id){
    m_complete = false;
    m_missingData = true;
  }
  if (m_bcid != bcid){
    m_bcidMismatch = true;
  }
}



/** \brief Add error to module
 */
void SCTEvent::AddError (unsigned int chip, unsigned int err)
{
  INFO("SCTEvent::AddError()");
  chip |= 20; // Add const 2MSB of chip address
  m_hasError = true;
  auto it = m_chipIDMap.find(chip);
  if (it == m_chipIDMap.end()) {
    m_Errors.back().push_back(0xff);
    ERROR("Passed chipID is not known! chipID = 0x"<<std::hex<<chip);
    // std::cout << "Known IDs: "<<std::endl;
    return;
  }
  assert(it->second < m_Errors.size());
  m_Errors.at(it->second).push_back(err);
}

/** \brief Add a hit to this module
 */
void SCTEvent::AddHit (unsigned int chip, unsigned int strip, unsigned int pattern)
{
  chip |= 0x20; // adding 2 MSB for chip address. All chips are served by "primary fiber".
  auto it = m_chipIDMap.find(chip);
  if (it == m_chipIDMap.end()) {
    if (false){
      ERROR("Passed chipID is not known! chipID = 0x"<<std::hex<<chip
      <<"\nKnown IDs: ");
      for (auto it : m_chipIDMap){
        std::cout<<" ChipID = 0x"<<std::hex<<it.first<<" Index = "<<std::dec<<it.second;
        m_Hits.back().push_back(std::make_pair(chip, strip));
      }
      std::cout<<std::endl;
    }
    return;
  }
  m_Hits.at(it->second).push_back(std::make_pair(strip, pattern));
}

/** \brief returns number of hits found in this event in this module
 */
unsigned int SCTEvent::GetNHits()
{
  unsigned int hits = 0;
  for (auto chip : m_Hits){
    hits += chip.size();
  }
  return hits;
}

/** \brief returns number of hits found in this event in the given chip
 */
unsigned int SCTEvent::GetNHits(int chip)
{
  auto it = m_chipIDMap.find(chip);
  if (it == m_chipIDMap.end()) {
    if (chip == 0){
      return m_Hits.back().size(); // unknown chip occurences
    }
    ERROR("GetNHits(): passed chipID is not known! chipID = 0x"<<std::hex<<chip);
    throw;
  }
  return m_Hits.at(it->second).size();
}

/** \brief returns hits for the given chip
 */
std::vector < std::pair<uint8_t, uint8_t> > SCTEvent::GetHits(int chip)
{
  auto it = m_chipIDMap.find(chip);
  if (it == m_chipIDMap.end()) {
    ERROR("GetHits(): passed chipID is not known! chipID = 0x"<<std::hex<<chip);
    throw;
  }
  return m_Hits.at(it->second);
}


/** \brief returns errors for the given chip
 */
std::vector < uint8_t > SCTEvent::GetErrors(int chip)
{
  auto it = m_chipIDMap.find(chip);
  if (it == m_chipIDMap.end()) {
    ERROR("GetErrors(): passed chipID is not known! chipID = 0x"<<std::hex<<chip);
    throw;
  }
  assert(it->second < m_Errors.size());
  return m_Errors.at(it->second);
}




