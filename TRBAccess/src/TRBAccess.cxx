/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <chrono>
#include <stdexcept>
#include "../TrackerReadout/TRBAccess.h"
#include "../TrackerReadout/TRBEventDecoder.h"
#include <bitset>

#define _ms 1000 // used for usleep

using namespace FASER;

TRBAccess::~TRBAccess()
{
  if (m_dataOutStream.is_open()){
    m_dataOutStream.close();
  }
}

/** *******************************************************
 \brief open an output file for writing binary data
 \param filename: file name
 ******************************************************** */
void TRBAccess::SetupStorageStream(std::string filename)
{ 
  m_writeDataFile = true;
  if (m_dataOutStream.is_open()){
    m_dataOutStream.close();
  }
  m_dataOutStream.open(filename, std::ios::trunc | std::ios::binary);
  if (!m_dataOutStream.is_open()){
     m_writeDataFile = false;
     ERROR("Could not open file "<<filename<<" no data will be written to disk!");
  }
}

/** *******************************************************
 \brief Send 'start readout'command to TRB
 ******************************************************** */
void TRBAccess::StartReadout(uint16_t param){
  if ((param & 0x1) == 0x1) {
    throw(std::runtime_error("ERROR in parameter passed to StartReadout: bit 0 has to be set to 0."));
  }
  
  {
    std::lock_guard<std::mutex> lock(mMutex_TRBByteBuffer);
    m_DataByteBuffer.clear();
  }
  
  m_DataWordOutputBuffer.clear();
  while (!mMutex_TRBEventData.try_lock()){
    INFO("Waiting for lock mMutex_TRBEventData");
    usleep(200000);
  }
  m_TRBEventData.clear();
  mMutex_TRBEventData.unlock();
  m_dataRate = 0;
  
  SendAndRetrieve(TRBCmdID::DATA_READOUT, param);
  
  // now start the data polling in a separate thread
  
  m_EODWordFound = false;
  m_daqRunning = true;
  if (m_processingThread != nullptr) {
    WARNING("Data processing thread already started or still running. Call StopReadout() to stop old thread.");
  }
  else m_processingThread = new std::thread(&TRBAccess::ProcessData, this);
  if (m_daqThread != nullptr) {
    WARNING("DAQ thread already started or still running. Call StopReadout() to stop old thread.");
  }
  else m_daqThread = new std::thread(&TRBAccess::PollData, this);
  
  // old working version
  //m_daqThread = new std::thread(&TRBAccess::PollData_working, this);
}

/** *******************************************************
 \brief Send 'stop readout'command to TRB
 ******************************************************** */
void TRBAccess::StopReadout(){
  uint16_t param = 0x1;
  SendAndRetrieve(TRBCmdID::DATA_READOUT, param);
  // and now stop the data polling thread
  
  int sleepCnt = 1;
  while (!m_EODWordFound && sleepCnt > 0){
    sleepCnt--;
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }
 
  m_daqRunning = false;
  if (m_daqThread != nullptr){
    if (!m_daqThread->joinable()){
      ERROR("Readout thread is not joinable!");
    }
    m_daqThread->join();
    delete m_daqThread;
    m_daqThread = nullptr;

    if (m_DEBUG) DEBUG("Waiting for processing thread to finish");
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
   
    // wait for up to 1 sec for the EOD word to arrive
    int sleepCnt = 10;
    while (!m_EODWordFound && sleepCnt > 0){
       sleepCnt--;
       std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
    // if EOD word not found by now: this is a fatal error. Force stopping of processing thread
    if (!m_EODWordFound){
       ERROR("EOD word did not arrive from TRB after > 1sec! This indicates 1) to a problem in the TRB <-> readout PC communication or 2) Overflowing buffers in the TRB. If this was a calibration run: is the dynamic delay enabled? \n Forcing processing thread to continue;");
       m_EODWordFound = true;
    }
    
    m_processingThread->join();
    delete m_processingThread;
    m_processingThread = nullptr;
    
    if (!m_EODWordFound) WARNING("End Of DAQ word not found, possible data loss!");
    
  }
  /*
  if (sleepCnt == 0){
     std::cout << "FATAL: End Of DAQ word not received. Expect data to be missing / corrupted! "<<std::endl;
     return false;
  }
   */
  if (m_DEBUG) DEBUG("[UPDATE] PLL counter : "<<ReadPLLErrorCounter());
}

/** *******************************************************
 \brief Return a vector of TRB RAW data per event.
 Note: the internally stored raw data will be deleted when called!
 ******************************************************** */
std::vector< std::vector<uint32_t> > TRBAccess::GetTRBEventData() 
{
   mMutex_TRBEventData.lock(); 
   auto copy = m_TRBEventData; 
   m_TRBEventData.clear();
   mMutex_TRBEventData.unlock(); 
   // std::cout << "Returning "<<copy.size()<<" events"<<std::endl;
   return copy;
}

/** *******************************************************
\brief This function processes the received data. Data is formatted as events.
 HEADER, TRRAILER markers as well as L1ID and BCID are extracted from the bytestream. So are ERRORS.
 This fuction continously looks at the internal buffer m_DataWordOutputBuffer if new data is available.
 Lives in an individual thread.
 
 ** Logic:
 - loop over words in buffer.
 - if header is found -> event open
 - expect atl least one TRB data word after words with BCID
 - look for TRB error words
 - if trailer is found && event open: event closed -> store all data words of event m_TRBEventData
    additionally write ROR_Header +  event data to m_dataOutStream (used for calibration)
    remove written data words from internal buffer
    - event open & last index of m_DataWordOutputBuffer is stored for next call to ProcessData()
 ******************************************************** */
void TRBAccess::ProcessData(){
   uint32_t TRBWord = 0;
   uint32_t L1ID = 0;
   unsigned int BCID = 0;
   int TRBWord_ID = 0;
   int nTRBError_8 = 0;
   int nTRBErrors = 0;
   //unsigned int nextWordId = 0;
  
   int nTRBData = 0;
   int nTRBError = 0;
   int nHEADER = 0;
   int nTrailer = 0;
   int nModuleData = 0;
   int nModuleError = 0;
   int nUnknown = 0;
  
   //unsigned long waitCnt = 0;
   //int lastFrameCount = -2; // -2 indicates first word received

   TRBEventDecoder *evtDecoder = new TRBEventDecoder();
   //bool eventOpen = false; //  true if a header was found
   //bool waitingForEOD = true; // wait for EOD word before terminating the processing
   //auto start = std::chrono::high_resolution_clock::now();
   //auto stop = start;
   //loop over internal buffer
   //unsigned int dataWordBufferSize = 0;
   while (m_daqRunning || !m_EODWordFound ){
      std::this_thread::sleep_for(std::chrono::milliseconds(20));
      // copy data to process
      std::vector<unsigned char> byteBuffer;
      {
         mMutex_TRBByteBuffer.lock();
         byteBuffer  = std::move(m_DataByteBuffer);
         mMutex_TRBByteBuffer.unlock();
      }
      
      auto bytesRead = byteBuffer.size();
      if (bytesRead == 0) continue;

      for (size_t i = 0; i < bytesRead; i++){

         TRBWord |= ((uint32_t)byteBuffer[i]) << (TRBWord_ID*8);
         TRBWord_ID++;
         if (TRBWord_ID > 4) { ERROR("THIS IS BAD: invalid TRBWord_ID");}
         if (TRBWord_ID == 4) { // word complete
            TRBWord_ID = 0;
            {
               std::lock_guard<std::mutex> lock(mMutex_TRBDataBuffer);
               auto oldSize = m_DataWordOutputBuffer.size();
               m_DataWordOutputBuffer.push_back(TRBWord);
               if (m_DataWordOutputBuffer.size() != oldSize +1){
                  ERROR("THIS IS BAD: could not add word to m_DataWordOutputBuffer. Size() stuck at " << oldSize);
               }
               if (m_DataWordOutputBuffer.back() != TRBWord){
                  ERROR("THIS IS BAD: TRBWord not correctly stored ");
               }
            } 
            
            uint32_t val;
            if (evtDecoder->IsModuleData(TRBWord, val)) nModuleData++;
            
            bool eventComplete = false;

            if (m_DEBUG) DEBUG("TRB Data Word = 0x"<<std::hex<<m_DataWordOutputBuffer.back());
            if (evtDecoder->IsHeader(TRBWord, L1ID)){
               m_RORHeader.SetEventID(L1ID, 0 /* ECR to be implemented */);
               nHEADER++;
            }

            // std::cout << "Decoding data, trbDAta"<<std::endl;
            if (evtDecoder->IsTRBData(TRBWord, BCID)){
               m_RORHeader.SetBCID(BCID);
               nTRBData++;
            }
            uint16_t error;
            if (evtDecoder->HasError(TRBWord, error) && TRBWord != 0x0){
              if (error < m_maxTRBErrorID) m_TRBErrors[error]++;
              else ERROR("THIS IS BAD: TRB returned unknown error ID");
             nTRBError++;
             if (error == 0x8) { // suppress output for this error by defaut. Error still stored in data stream
                  nTRBError_8++;
               } else {
                  nTRBErrors++;
                  WARNING("Retrieved ERROR word from TRB: 0x"<<std::hex<<error<< " TRB word = 0x"<<TRBWord);
               }
            }
            uint32_t checksum;
            if (evtDecoder->IsTrailer(TRBWord, checksum)){
               // TODO: add checksum verification
               eventComplete = true;
               nTrailer++;
            }
            if (eventComplete){ 
               // We have a full event -> Write to disk
               std::lock_guard<std::mutex> lock(mMutex_TRBDataBuffer);
               if (m_DEBUG) DEBUG("Event complete, writing ROR header + data. DataSize = "<<std::dec<<m_DataWordOutputBuffer.size() * sizeof(m_DataWordOutputBuffer[0])<<" Bytes");
               if (m_writeDataFile) {
                  m_RORHeader.SetDataSize(m_DataWordOutputBuffer.size() * sizeof(m_DataWordOutputBuffer[0])); // size in bytes
                  m_RORHeader.SetFormatVersion(0x1); // size in bytes
                  m_RORHeader.SetDataStatus(RORHeader::DATA_STATUS_OK); // data OK
                  m_RORHeader.Write(m_dataOutStream);
                  if (m_DEBUG) { DEBUG("Header written, now writing data: "<< std::dec<<m_DataWordOutputBuffer.size() <<"words"); }

                  m_dataOutStream.write((char*)&m_DataWordOutputBuffer[0], sizeof(m_DataWordOutputBuffer[0])*m_DataWordOutputBuffer.size());
               }
               if (m_DEBUG) DEBUG("Data written, adding data to m_TRBEventData");
               
               mMutex_TRBEventData.lock();
               m_TRBEventData.push_back(m_DataWordOutputBuffer); // store complete event
               mMutex_TRBEventData.unlock();

               m_DataWordOutputBuffer.clear();
               m_DataWordOutputBuffer.resize(0);
               TRBWord = 0;
               continue;
            }

            if (evtDecoder->IsEndOfDAQ(TRBWord)){
               std::lock_guard<std::mutex> lock(mMutex_TRBDataBuffer);
               if (m_DataWordOutputBuffer.size() > 4){
                  // there is unexpected data left, write it to disk
                  m_RORHeader.SetDataStatus(0xff); // set an ERROR data status, to be defined TOFDO!
               }
               //if (true) std::cout << "Found EOD word, writing ROR header + data: 0x"<<std::hex<< TRBWord<<  std::endl;

               if (m_writeDataFile) {
                  m_RORHeader.SetDataSize(m_DataWordOutputBuffer.size() * sizeof(m_DataWordOutputBuffer[0])); // size in bytes
                  m_RORHeader.SetFormatVersion(0x1); // size in bytes
                  m_RORHeader.SetDataStatus(RORHeader::DATA_STATUS_EOD);
                  m_RORHeader.Write(m_dataOutStream);
                  m_dataOutStream.write((char*)&m_DataWordOutputBuffer[0], sizeof(m_DataWordOutputBuffer[0]*m_DataWordOutputBuffer.size()));
               }
               while (!mMutex_TRBEventData.try_lock()){
                  WARNING("Can't lock mMutex_TRBEventData");
                  usleep(10000);
               }
               m_TRBEventData.push_back(m_DataWordOutputBuffer); // store complete event
               mMutex_TRBEventData.unlock();

               m_DataWordOutputBuffer.clear();
               m_DataWordOutputBuffer.resize(0);
               m_EODWordFound = true;
               TRBWord = 0;
               continue;
            }
            TRBWord = 0;
         }
      }

   } // while()
   delete evtDecoder;
   if (m_DEBUG) DEBUG(std::dec<<"TRBHeaders: "<<nHEADER<<" TRBTrailers: "<<nTrailer<<" TRBData: "<<nTRBData<<" TRBErrors(!0x8): "<<nTRBErrors<<" TRBErrors(0x8) = "<<nTRBError_8<< " ModuleErrors: "<<nModuleError<<"  ModuleData: "<<nModuleData<< " unknown data word: "<<nUnknown);
}


/** *******************************************************
 \brief Read any data pending in the data-out buffer of the TRB.
 Data is packet into 32-bit word and analysed to get HEADER marker,
 L1ID and BCID from bytstream, as well as any ERRORS.
 ******************************************************** */
void TRBAccess::PollData_working(){
  std::vector<unsigned char> tmp;
  int bytesRead = 0;
  long sumBytesTransferred = 0;
  unsigned long totalBytesRead(0);
  long transfers = 0;
  uint32_t TRBWord = 0;
  uint32_t L1ID = 0;
  unsigned int BCID = 0;
  int TRBWord_ID = 0;
  int nTRBError_8 = 0;
  int nTRBErrors = 0;

  TRBEventDecoder *evtDecoder = new TRBEventDecoder();
 
  auto start = std::chrono::high_resolution_clock::now();
  auto stop = start;
  
  if (m_DEBUG) DEBUG("[UPDATE] PLL counter : "<<ReadPLLErrorCounter());
  auto tProcessStart = start;
  auto tReadStart = start; 
  auto timeBetweenReads = std::chrono::duration_cast<std::chrono::microseconds>(stop - stop);

  while (m_daqRunning || bytesRead != 0){
    try {
      tReadStart = std::chrono::high_resolution_clock::now();
      timeBetweenReads += std::chrono::duration_cast<std::chrono::microseconds>(tReadStart - tProcessStart);
      tmp = m_interface->ReadData(bytesRead);
      tProcessStart = std::chrono::high_resolution_clock::now();
      

      if (bytesRead < 0 ){
        ERROR("Bytes read returned "<<std::dec<<bytesRead);
      }

      stop = std::chrono::high_resolution_clock::now();
      sumBytesTransferred += bytesRead;
      totalBytesRead += bytesRead;
      transfers++;

      auto duration = std::chrono::duration_cast<std::chrono::seconds>(stop - start);
      if (duration.count() > m_rateMathInterval){
        m_dataRate = (double)sumBytesTransferred/(double)(1000.*duration.count());
        start =  std::chrono::high_resolution_clock::now();
        sumBytesTransferred = 0;
        if (m_showDataRate || m_DEBUG) {
          std::cout << " Data rate = "<<m_dataRate/1000. << "MB/sec"<<std::endl;
        }
      }
      
    } catch (...){
      ERROR("Read Serial threw an error. Continuing anyways");
    }

    if (m_DEBUG && bytesRead != 0){DEBUG("PollData(): bytes retrieved = "<<bytesRead);}
    
    auto startFor = std::chrono::high_resolution_clock::now();
    auto now = start;
    for (int i = 0; i < bytesRead; i++){
        now = std::chrono::high_resolution_clock::now();
        auto durationFor = std::chrono::duration_cast<std::chrono::microseconds>(now - startFor);
        if (durationFor.count() > 1000000){
         WARNING("Stuck in for loop for more than one second ... loop index = "<<std::dec<< i << " out of "<<bytesRead);
         startFor = std::chrono::high_resolution_clock::now();
        }
        TRBWord |= ((uint32_t)tmp[i]) << (TRBWord_ID*8);
        TRBWord_ID++;
        if (TRBWord_ID > 4) { ERROR("THIS IS BAD: invalid TRBWord_ID");}
        //if ( i % 1000 == 0) { std::cout << "For loop i = "<<i <<std::endl;}
        if (TRBWord_ID == 4) { // word complete
          TRBWord_ID = 0;
          
          auto oldSize = m_DataWordOutputBuffer.size();
          m_DataWordOutputBuffer.push_back(TRBWord);
          if (m_DataWordOutputBuffer.size() != oldSize +1){
            ERROR("THIS IS BAD: could not add word to m_DataWordOutputBuffer. Size() stuck at " << oldSize);
          }
          if (m_DataWordOutputBuffer.back() != TRBWord){
            ERROR("THIS IS BAD: TRBWord not correctly stored ");
          }
          
          TRBWord = 0;
          
          //int wordCnt_led = 0;
          //int wordCnt_ledx = 0;
          
          bool eventComplete = false;

          //if (m_DEBUG) std::cout << "TRB Data Word = 0x"<<std::hex<<m_DataWordOutputBuffer.back()<<std::endl;
          if (evtDecoder->IsHeader(m_DataWordOutputBuffer.back(), L1ID)){
             m_RORHeader.SetEventID(L1ID, 0 /* ECR to be implemented */);
          }

          // std::cout << "Decoding data, trbDAta"<<std::endl;
          if (evtDecoder->IsTRBData(m_DataWordOutputBuffer.back(), BCID)){
             m_RORHeader.SetBCID(BCID);
          }
            uint16_t error;
            if (evtDecoder->HasError(m_DataWordOutputBuffer.back(), error) && m_DataWordOutputBuffer.back() != 0x0){
              if (error == 0x8) { // suppress output for this error by defaut. Error still stored in data stream
                nTRBError_8++;
              } else {
                nTRBErrors++;
                WARNING("Retrieved ERROR word from TRB: 0x"<<std::hex<<error<< " TRB word = 0x"<<m_DataWordOutputBuffer.back());
              }
            }
            uint32_t checksum;
            if (evtDecoder->IsTrailer(m_DataWordOutputBuffer.back(), checksum)){
              eventComplete = true;

           }
          if (eventComplete){
            // TODO: here a Checksum Check could be done... leaving this for now
            // We have a full event -> Write to disk
            if (m_DEBUG) std::cout << "DEBUG: Event complete, writing ROR header + data. DataSize = "<<std::dec<<m_DataWordOutputBuffer.size() * sizeof(m_DataWordOutputBuffer[0])<<" Bytes"<<std::endl;
            if (m_writeDataFile) {
               m_RORHeader.SetDataSize(m_DataWordOutputBuffer.size() * sizeof(m_DataWordOutputBuffer[0])); // size in bytes
               m_RORHeader.SetFormatVersion(0x1); // size in bytes
               m_RORHeader.SetDataStatus(RORHeader::DATA_STATUS_OK); // data OK
               m_RORHeader.Write(m_dataOutStream);
               if (m_DEBUG) { std::cout << "DEBUG: header written, now writing data: "<< std::dec<<m_DataWordOutputBuffer.size() <<"words"<<std::endl; }  

               m_dataOutStream.write((char*)&m_DataWordOutputBuffer[0], sizeof(m_DataWordOutputBuffer[0])*m_DataWordOutputBuffer.size());
            }
            if (m_DEBUG) DEBUG("Data written, adding data to m_TRBEventData");
            while (!mMutex_TRBEventData.try_lock()){
              WARNING("Can't lock mMutex_TRBEventData");
              usleep(10000);
            }
            m_TRBEventData.push_back(m_DataWordOutputBuffer); // store complete event
            mMutex_TRBEventData.unlock();
            
            m_DataWordOutputBuffer.clear();
            m_DataWordOutputBuffer.resize(0);
            continue;
          }
          
          if (evtDecoder->IsEndOfDAQ(m_DataWordOutputBuffer.back())){
            if (m_DataWordOutputBuffer.size() > 4){
              // there is unexpected data left, write it to disk
              m_RORHeader.SetDataStatus(0xff); // set an ERROR data status, to be defined TOFDO!
            }
            if (m_DEBUG) DEBUG("Found EOD word, writing ROR header + data: 0x"<<std::hex<< TRBWord);
            
            if (m_writeDataFile) {
               m_RORHeader.SetDataSize(m_DataWordOutputBuffer.size() * sizeof(m_DataWordOutputBuffer[0])); // size in bytes
               m_RORHeader.SetFormatVersion(0x1); // size in bytes
               m_RORHeader.SetDataStatus(RORHeader::DATA_STATUS_EOD);
               m_RORHeader.Write(m_dataOutStream);
               m_dataOutStream.write((char*)&m_DataWordOutputBuffer[0], sizeof(m_DataWordOutputBuffer[0]*m_DataWordOutputBuffer.size()));
            }
            while (!mMutex_TRBEventData.try_lock()){
              WARNING("Can't lock mMutex_TRBEventData");
              usleep(10000);
            }
            m_TRBEventData.push_back(m_DataWordOutputBuffer); // store complete event
            mMutex_TRBEventData.unlock();
            
            m_DataWordOutputBuffer.clear();
            m_DataWordOutputBuffer.resize(0);
            m_EODWordFound = true;
            continue;
          }
        }
    }
  }
  
  delete evtDecoder;
  if(m_showTransfers) {
    INFO("DAQ: bytes transfered "<<std::dec<<totalBytesRead<<" in "<<transfers<<" transfers");
    INFO("Average time between USB reads (processiing time): "<<timeBetweenReads.count()/transfers<< " musec");
    INFO("Found "<<nTRBErrors<<" TRB errors and additionally "<<nTRBError_8<<" TRB FIFO full errors (0x8)");
    if (m_DataWordOutputBuffer.size() > 0) INFO("DAQ: last word recorded = 0x"<<std::hex<<m_DataWordOutputBuffer.back() << std::dec); 
  }
}

/** *******************************************************
 \brief Read any data available from the TRB interface.
 Data is packet into 32-bit words and stored in the std::vector m_DataWordOutputBuffer
 TODO: use try_lock to lock mutex. If locking failes, store data in temporary buffer and copy over next time. Only exit from thread once temporary buffer is empty!!
 ******************************************************** */
void TRBAccess::PollData(){
  std::vector<unsigned char> tmp;
  std::vector<uint32_t> localBuffer;
  int bytesRead = 0;
  //  bool EndOfDAQReceived(false);
  unsigned sumBytesTransferred = 0;
  unsigned long totalBytesRead(0);
  int transfers = 0;
  //uint32_t TRBWord = 0;
  //uint32_t L1ID = 0;
  //unsigned int BCID = 0;
  //int TRBWord_ID = 0;
  //long byteCount = 0;
  long int cntFailedMutexLocks = 0;
  
  auto start = std::chrono::high_resolution_clock::now();
  auto stop = start;
  
  while (m_daqRunning || bytesRead != 0){
    try {
      tmp = m_interface->ReadData(bytesRead);
      if (bytesRead > static_cast<int>(tmp.size())) {
         ERROR("PollData() read more bytes "<<bytesRead<<" than size of buffer "<<tmp.size());
         bytesRead = tmp.size();
      }
    
      stop = std::chrono::high_resolution_clock::now();
      sumBytesTransferred += bytesRead;
      totalBytesRead += bytesRead;
      transfers++;

      auto duration = std::chrono::duration_cast<std::chrono::seconds>(stop - start);
      if (duration.count() > m_rateMathInterval){
        m_dataRate = (double)sumBytesTransferred/(double)(1000.*duration.count());
        start =  std::chrono::high_resolution_clock::now();
        sumBytesTransferred = 0;
        if (m_showDataRate || m_DEBUG) {
          std::cout << " Data rate = "<<m_dataRate/1000. << "MB/sec"<<std::endl;
        }
      }
      if (bytesRead == 0) continue;
    } catch (...){
      // std::this_thread::sleep_for(std::chrono::milliseconds(50));
      WARNING("Error caught in ReadData()");
    }
    if (m_DEBUG)  {DEBUG("PollData(): bytes retrieved = "<<bytesRead);}
   
    {
      mMutex_TRBByteBuffer.lock();
      m_DataByteBuffer.insert(m_DataByteBuffer.end(), tmp.begin(), tmp.end());
      mMutex_TRBByteBuffer.unlock();
    }
    
  }

  std::this_thread::sleep_for(std::chrono::milliseconds(30)); // give pocessing thread some time to finish
  if(m_showTransfers) {
    INFO("INFO: DAQ polling thread ended. bytes transfered "<<std::dec<<totalBytesRead<<" in "<<transfers<<" transfers");
    INFO("INFO: mutex on mMutex_TRBDataBuffer was not availables: "<< cntFailedMutexLocks <<  " times " );
  }
}

/** *******************************************************
 \brief Writes configuration of next scan step to output file, preceeded by an ROR header
 ******************************************************** */
void TRBAccess::SaveScanStepConfig(ScanStepConfig scanConfig)
{
  m_RORHeader.SetDataStatus(0xf0);
  m_RORHeader.SetDataSize(scanConfig.size());
  m_RORHeader.Write(m_dataOutStream);
  scanConfig.Write(m_dataOutStream);
  m_scanMode = true;
}


/** *******************************************************
 \brief Read phase-lock loop error counter: 1 word, 8 LSB is the value of the counter, bits 8-15 are reserved
  If no other register was querried before this one, the first word returned will contain the value from the last querry to this register.
  Hence the first word read will be ommited.
 ******************************************************** */
uint16_t TRBAccess::ReadPLLErrorCounter(){
  uint16_t param = 0x3;
  uint16_t answer = 0x0;
  uint8_t cmd = TRBCmdID::USER_GET;
  GPIOCheck(this, &TRBAccess::SendAndRetrieve, cmd, param, &answer);
  return answer;
}

/** *******************************************************
 \brief Read configuratino from TRB: 6 words of 16bit, 3MSB used as addressing, 13 LSB are configuration data
  If no other register was querried before this one, the first word returned will contain the value from the last querry to this register.
  Hence the first word read will be ommited.
 ******************************************************** */
std::vector<uint16_t> TRBAccess::ReadConfigReg(){
  uint16_t param = 0x2;
  uint16_t answer = 0x0;
  std::vector<uint16_t> configReg;
  int wordsToRead = 8;
  int addressShift = 13;
  if ((m_FPGA_version >= 0x1C && m_FPGA_version < 0x110) || m_FPGA_version >= 0x11C) { wordsToRead = 10; addressShift = 12;}
  for (int i = -1; i < wordsToRead; i++){
    SendAndRetrieve(TRBCmdID::USER_GET, param, &answer);
    INFO("Config read back: 0x"<<std::hex<<std::setw(4)<<answer<< " address = "<<(answer >> addressShift));
  
    if (i > 0 && (answer >> addressShift) == 0) { configReg.clear(); i = 0; } // start over to avoid getting an extra word 0
    if (i >= 0) configReg.push_back(answer);
  }
  return configReg;
}

/** *******************************************************
 \brief Read phase configuratino from TRB: 2 words of 16bit, 1MSB used as addressing, 15 LSB are configuration data.
 If no other register was querried before this one, the first word returned will contain the value from the last querry to this register.
 Hence the first word read will be ommited.
 ******************************************************** */
std::vector<uint16_t> TRBAccess::ReadPhaseConfigReg(){
  uint16_t param = 0x0;
  uint16_t answer = 0x0;
  std::vector<uint16_t> configReg;
  for (int i = -1; i < 2; i++){
    SendAndRetrieve(TRBCmdID::USER_GET, param, &answer);
    //std::cout << "Config Redback 0x"<<std::hex<<std::setw(4)<<answer<< " address = "<<(answer >> 15)<< std::endl;
    if (i > 0 && (answer >> 15) == 0) { configReg.clear(); i = 0; } // start over to avoid getting an extra word 0
    if (i >= 0) configReg.push_back(answer);
  }
  return configReg;
}

/** *******************************************************
 \brief Read soft counters from TRB: 3 words of 16bit, 2 MSB used as addressing, 14 LSB are configuration data
 If no other register was querried before this one, the first word returned will contain the value from the last querry to this register.
 Hence the first word read will be ommited.
 \param bcid: 12 LSB correspond to bcid
 \param L1A: 24 bits
 ******************************************************** */
void TRBAccess::ReadSoftCounters(unsigned int &bcid, unsigned int &L1A){
  uint16_t param = 0x1;
  uint16_t answer = 0x0;
  std::vector<uint16_t> configReg;
  for (int i = -1; i < 3; i++){
    SendAndRetrieve(TRBCmdID::USER_GET, param, &answer);
    //std::cout << "Config Redback 0x"<<std::hex<<std::setw(4)<<answer<< " address = "<<(answer >> 13)<< std::endl;
    if (i > 0 && (answer >> 14) == 0) { configReg.clear(); i = 0; } // start over to avoid getting an extra word 0
    if (i >= 0) configReg.push_back(answer);
  }
  bcid = (configReg[0] & 0xFFF); // 12 LSB
  L1A  = ((configReg[0] >>12 ) & 0x3); // 2 bits
  L1A |= ((configReg[1] << 2) & 0xFFFC); // 14 bits
  int tmp = configReg[2];
  L1A |= ((tmp << (2+16)) & 0xFF0000); // 8 bits
}

/** *******************************************************
 \brief Put all chips on specified module into data taking mode
 ******************************************************** */
void TRBAccess::SCT_EnableDataTaking(unsigned int module)
{
  GetSCTSlowCommandBuffer()->EnableDataTaking();
  WriteSCTSlowCommandBuffer();
  SendSCTSlowCommandBuffer(module);
}

/** *******************************************************
 \brief Issue a calibration pule followed by aa L1A to all chips on specified modules.
 \param module: module mask
 \param delay:  Delay between calibration pulse and L1A in BCs [0 .. 157]
 \param CalLoop: to enable CalLoop bit in TRB Configuration register. 

Note that if using CalLoop=true, the user is supposed to have set previously CalLoopNb 
(ConfigReg::Set_Global_CalLoopNb) and CalLoopDelay (ConfigReg::Set_Global_CalLoopDelay)
in the TRB config register. 
 ******************************************************** */
void TRBAccess::SCT_CalibrationPulse(unsigned int module, unsigned int delay, bool CalLoop)
{
  GetSCTSlowCommandBuffer()->CalibrationPulse(delay);
  WriteSCTSlowCommandBuffer();
  SendSCTSlowCommandBuffer(module, CalLoop);
}

/** *************************************************** 
\brief Read status and return CalLoopRunning bit.
 *************************************************** */
bool TRBAccess::IsCalibrationLoopRunning()
{
  uint16_t status;
  ReadStatus(status);
  
  return (status & 0x40); // calCoolRunning = bit 6
}


/** *******************************************************
 \brief Enable every 8th strip on all chips of selected module.
 \param module: module mask
 \param maskSelect: 0..7. 0: 1000000100 .... and so on
 ******************************************************** */
void TRBAccess::SCT_WriteStripmask(unsigned int module, int maskSelect)
{
  //uint16_t maskword = 0x0101; // This is the mask to be used: every 8th strip is active
  uint16_t maskword = 0x1111; // This is the mask to be used: every 4th strip is active
  maskword <<= maskSelect;
  std::vector<uint16_t> mask;
  for (unsigned int i = 0; i < 128; i+=16) mask.push_back(maskword);
  GetSCTSlowCommandBuffer()->SetStripMask(0x3f, mask);
  WriteSCTSlowCommandBuffer();
  SendSCTSlowCommandBuffer(module);
}


void TRBAccess::SCT_WriteStripmaskAllON(unsigned int module)
{
  uint16_t maskword = 0xffff; 
  std::vector<uint16_t> mask;
  for (unsigned int i = 0; i < 128; i+=16) mask.push_back(maskword);
  GetSCTSlowCommandBuffer()->SetStripMask(0x3f, mask);
  WriteSCTSlowCommandBuffer();
  SendSCTSlowCommandBuffer(module);
}

/** *******************************************************
 \brief Load BCID and L1A counts to SoftCounters register
 \param bcid: 12 bit number, max value 2095
 \param L1A:  24 bit number, max value 16777215
 ******************************************************** */
void TRBAccess::PresetSoftCounters(unsigned int bcid, unsigned int L1A)
{
  if (bcid > 2095) {
    WARNING("bcid passed exeeds 12bit range, setting to 0xfff.");
    bcid = 0xfff;
  }
  if (L1A > 0xffffff) {
    WARNING("bcid passed exeeds 24bit range, setting to 0xffffff.");
    L1A = 0xffffff;
  }
  // generate 3x 16-bit payload word: 2MSB = word count, 14LSB = counter values
  uint16_t word = 0;
  unsigned int tmp = 0;
  
  word = 0; // wordID 0
  word = bcid; // 12-bits
  tmp = L1A; // 24-bits
  tmp <<= 12;
  word |= (tmp & 0x3000); // use only bits 12&13
  
  SendAndRetrieve(TRBCmdID::USER_SET_SOFTCOUNTERS, word);
  
  word = 0x4000; // wordID 1
  tmp = L1A; // 24-bits
  tmp >>= 2; // 2 bits already written
  word |= (tmp & 0x3fff); // use only 14 bits
  
  SendAndRetrieve(TRBCmdID::USER_SET_SOFTCOUNTERS, word);
  
  word = 0x8000; // wordID 2
  tmp = L1A; // 24-bits
  tmp >>= 16; // 16 bits already written
  word |= (tmp & 0xff); // use only remaining 8 bits
  
  SendAndRetrieve(TRBCmdID::USER_SET_SOFTCOUNTERS, word);
  
}

/** *******************************************************
 \brief Upload TRB phase configuration stored in ConfigReg m_phaseReg
 ******************************************************** */
void TRBAccess::WritePhaseConfigReg()
{
  // we will write 6 32bit words. Of the payload 3 MSB are the word number (aka address)
  // and the remaining 13 LSB contain the config data;
  // The configuration object returns a vector of readily prpared 16-bit words to send
  for (auto payload : m_phaseReg.GetPayloadVector()){
    if (m_DEBUG) { DEBUG("writing payload 0x"<<std::hex <<payload);}
    SendAndRetrieve(TRBCmdID::USER_SET_PHASE, payload);
  }
}

/** *******************************************************
 \brief Apply TRB phase configuration previously uploaded uing WritePhaseConfigReg()
 ******************************************************** */
void TRBAccess::ApplyPhaseConfig()
{
  SendAndRetrieve(TRBCmdID::USER_SET_APPLYPHASE, 0x0);
}


/** *******************************************************
 \brief Upload SCT slow command m_SCTSlowCommandReg buffer to TRB.
 ******************************************************** */
void TRBAccess::WriteSCTSlowCommandBuffer()
{
  // we will write 15 32bit words. Of the payload 4 MSB are the word number (aka address)
  // and the remaining 12 LSB contain the config data;
  // The configuration object returns a vector of readily prepared 16-bit words to send
  for (auto payload : m_SCTSlowCommandReg.GetPayloadVector()){
    if (m_DEBUG) { DEBUG("writing payload 0x"<<std::hex <<payload);}
    SendAndRetrieve(TRBCmdID::USER_SET_SCTSLOWCOMMAND, payload);
  }
}


/** *******************************************************
 Set parameter to the direct_register port of the FPGA
 ******************************************************** */
void TRBAccess::SetDirectParam(uint16_t param)
{
  SendAndRetrieve(TRBCmdID::SET_DIRECT_PARAM, param);
}

/** *******************************************************
 \brief Send a software generated L1A sinal to specified modules
 \param module: Bitmask of enabled modules 0-7
 ******************************************************** */
void TRBAccess::GenerateL1A(uint8_t modules, bool loop)
{
  uint8_t cmd = TRBCmdID::USER_SET_SENDCOMMAND;
  uint16_t word = modules;
  word <<= 5; // module mask from bit 4-11
  word |= 0x1; // bit0 = SoftL1A
  
  if (loop) {
    word |= 0x4000; // enable calibration loop = bit 14
  }

  if (GPIOCheck(this,&TRBAccess::SendAndRetrieve, cmd, word, nullptr)){
    WARNING("Errors encountered while sending soft L1A command. An L1A may not have been generated."); 
  }
}

/** *******************************************************
 \brief Send a soft-reset sinal to specified modules
 \param module: Bitmask of enabled modules 0-7
 ******************************************************** */
void TRBAccess::GenerateSoftReset(uint8_t modules)
{
  uint16_t word = modules;
  word <<= 5; // module mask from bit 4-11
  word |= 0x2; // bit1 = SoftReset
  uint8_t cmd = TRBCmdID::USER_SET_SENDCOMMAND;
  
  if (GPIOCheck(this,&TRBAccess::SendAndRetrieve, cmd, word, nullptr)){
     THROW(TRBAccessException, "ERROR sending SoftReset command. Try resync to clock?");
  }
}

/** *******************************************************
 \brief Generate hard-reset sinal for specified modules
 \param module: Bitmask of enabled modules 0-7
 ******************************************************** */
void TRBAccess::GenerateHardReset(uint8_t modules)
{
  uint16_t word = modules;
  word <<= 5; // module mask from bit 4-11
  word |= 0x4; // bit2 = HardReset
  
  SendAndRetrieve(TRBCmdID::USER_SET_SENDCOMMAND, word);
}

/** *******************************************************
 \brief Send content of SCT_SLOW_COMMAND register to specified modules
 \param module: Bitmask of enabled modules 0-7\
 SCT_SLOW_COMMAND register has to be loaded before using WriteSCTSlowCommandBuffer()
 TODO: add a check if the buffer is filled!
 ******************************************************** */
void TRBAccess::GenerateBCReset(uint8_t modules)
{
  uint16_t word = modules;
  word <<= 5; // module mask from bit 4-11
  word |= 0x8; // bit3 = BCReset
  
  SendAndRetrieve(TRBCmdID::USER_SET_SENDCOMMAND, word);
}

/** *******************************************************
 *  \brief Abort running FW loop
 *   ******************************************************** */
void TRBAccess::AbortLoop()
{
   uint16_t word = 0;
   word |= 0x8000; // bit15 = LoopAbort

   SendAndRetrieve(TRBCmdID::USER_SET_SENDCOMMAND, word);
}

/** \brief Reset TRB FIFO */
void TRBAccess::FIFOReset()
{
  SetDirectParam(m_configReg.GetGlobalDirectParam() | TRBDirectParameter::FifoReset);
  usleep(20*_ms);
}

/** TRB reset ERR counter */
void TRBAccess::ErrCntReset()
{
  SetDirectParam(m_configReg.GetGlobalDirectParam() | TRBDirectParameter::ErrCntReset);
  usleep(20*_ms);
}

/** TRB reset L1A counter */
void TRBAccess::L1CounterReset()
{
  SetDirectParam(m_configReg.GetGlobalDirectParam() | TRBDirectParameter::L1CounterReset);
  usleep(20*_ms);
}

/** *******************************************************
 \brief Send content of SCT_SLOW_COMMAND register to specified modules
 \param module: Bitmask of enabled modules 0-7\
 SCT_SLOW_COMMAND register has to be loaded before using WriteSCTSlowCommandBuffer()
 TODO: add a check if the buffer is filled!
 ******************************************************** */
void TRBAccess::SendSCTSlowCommandBuffer(uint8_t modules, bool CalLoop)
{
  uint16_t word = modules;
  word <<= 5; // module mask from bit 5-12
  word |= 0x10; // bit4 = SCT Slow Command
  
  if (CalLoop) {
    word |= 0x4000; // enable calibration loop = bit 14
  }
  // std::cout << "Sending SCT Slow command req: 0x"<<word<<std::endl;
  SendAndRetrieve(TRBCmdID::USER_SET_SENDCOMMAND, word);
}


/** *******************************************************
 \brief Upload TRB global configuration stored in ConfigReg m_configReg
 ******************************************************** */
void TRBAccess::WriteConfigReg()
{
  // we will write 6 32bit words. Of the payload 3 MSB are the word number (aka address)
  // and the remaining 13 LSB contain the config data;
  // For Firmware version >= v1.5 8 32bit words are written
  // The configuration object returns a vector of readily prepared 16-bit words to send
  for (auto payload : m_configReg.GetPayloadVector()){
    if (m_DEBUG) { DEBUG("writing payload 0x"<<std::hex <<payload);}
    SendAndRetrieve(TRBCmdID::USER_SET_CONFIG, payload);
  }
  usleep(200*_ms);
}

/** *******************************************************
 \brief Configure a full SCT module.
 The TRB channel the module is connected to is stored in the configuration. It can be overwritten by specifying a module.
 \param cfg an object of type ConfigurationHandlerSCT.
 \param module overrites the TRB channel used to configure the attached module.
 ******************************************************** */
void TRBAccess::ConfigureSCTModule(ConfigHandlerSCT *cfg, int module = -1)
{
  if (module < 0){
    module = cfg->GetTRBChannel();
    WARNING("Conversion to bitmask missing! ");
  }
  
  // reset module
  GenerateSoftReset(module);
  usleep(200*_ms);
  
  std::vector<unsigned int> chipIDs = cfg->GetChipIDs();
  
  for (auto chip : chipIDs){

    try {
      // set chip mask
      std::vector<uint16_t> vmask = cfg->GetStripMaskRegister(chip);
      
      if (cfg->GetApplySCTMasks()){
        INFO("Applying strip masks set in SCT input config file for chip ID "<<chip);
        GetSCTSlowCommandBuffer()->SetStripMask(chip, vmask);
      }
      else {
        INFO("Unmasking all strips for chip ID "<<chip);
        GetSCTSlowCommandBuffer()->SetStripMask(chip, constant::unmasked_vmask);
      }
      WriteSCTSlowCommandBuffer();
      SendSCTSlowCommandBuffer(module);

      unsigned int cfgReg = cfg->GetConfigReg(chip);
      INFO("Configuring chip 0x"<<std::hex<<(int)chip<<" as = 0x"<<cfgReg);
      GetSCTSlowCommandBuffer()->SetConfigReg(chip, cfgReg);
      WriteSCTSlowCommandBuffer();
      SendSCTSlowCommandBuffer(module);
      
  
      INFO(" Thr ");
      uint16_t threshCalReg(0);
      threshCalReg = cfg->GetThresholdCalibReg(chip); // ThreshCal register value with threshold from TrimTarget and calibration pulse charge = 0
      INFO(std::endl<<"Setting threshold "<<std::dec<<cfg->GetTrimTarget(chip)<<" mV");
      GetSCTSlowCommandBuffer()->SetThreshold(chip, threshCalReg);
      WriteSCTSlowCommandBuffer();
      SendSCTSlowCommandBuffer(module);

      INFO(" Bias = 0x"<<std::hex<<cfg->GetBiasDac(chip));
      GetSCTSlowCommandBuffer()->SetBiasDac(chip, cfg->GetBiasDac(chip));
      WriteSCTSlowCommandBuffer();
      SendSCTSlowCommandBuffer(module);
  
      INFO(" Strobe ");
      GetSCTSlowCommandBuffer()->SetStrobeDelay(chip, cfg->GetStrobeDelay(chip));
      WriteSCTSlowCommandBuffer();
      SendSCTSlowCommandBuffer(module);

      INFO("Config Trim");
      for (unsigned int channel = 0; channel < 128; channel++){
        unsigned int cfg_TrimDAC = cfg->GetEncodedTrimDac(chip, channel);
        //DEBUG("Sending trimDAC 0x"<<std::hex<<cfg_TrimDAC<<std::dec<<" for chip "<<chip<<" channel "<<channel);
        GetSCTSlowCommandBuffer()->SetTrimDac(chip, cfg_TrimDAC);
        WriteSCTSlowCommandBuffer();
        SendSCTSlowCommandBuffer(module);
      }
    } catch (std::exception &e){
      ERROR(e.what());
      THROW(TRBConfigurationException, "Tracker modules could not be configured due to missing or invalid configurations.");
    }
  }
  
  /*
  std::cout << "Config bias"<<std::endl;
  GetSCTSlowCommandBuffer()->SetBiasDac(0x3f, 0x180d);
  WriteSCTSlowCommandBuffer();
  SendSCTSlowCommandBuffer(module);
   */
}

void TRBAccess::VerifyConfigReg(){

  std::vector<uint16_t> rawCfg  = ReadConfigReg();
  ConfigReg returnedCfg;
  returnedCfg.FillFromPayload(rawCfg);

  if ( returnedCfg != m_configReg ) THROW(TRBConfigurationException, "Configurations read back do not match configurations set.");

  INFO("Configurations succesfully set.");
  return;

}

/** Read and decode the TRB status bits
 */
void TRBAccess::PrintStatus()
{
  uint16_t status;
  ReadStatus(status);
  INFO("\n________________________________"<<std::endl
       <<" TRB status is 0x"<<std::hex<<status<<std::endl
       <<"   Fine Phase               "<<(status & 0x1)<<std::endl
       <<"   LocalClkSel              "<<(status & 0x2)<<std::endl
       <<"   TLBClkSel                "<<(status & 0x4)<<std::endl
       <<"   Busy                     "<<(status & 0x8)<<std::endl
       <<"   SafePhaseDetection0      "<<(status & 0x10)<<std::endl
       <<"   SafePhaseDetection1      "<<(status & 0x20)<<std::endl
       <<"   CalLoopRunning           "<<(status & 0x40)<<std::endl
       <<"________________________________");
}

void TRBAccess::ReadbackAndPrintConfig() {

  std::vector<uint16_t> rawCfg  = ReadConfigReg();
  ConfigReg returnedCfg;
  returnedCfg.FillFromPayload(rawCfg);
  returnedCfg.Print();

}
