/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/
 

#include "../TrackerReadout/ConfigurationHandling.h"
#include <fstream>
#include <iostream>
#include <iomanip>
#include <unistd.h>

using namespace FASER;
using nlohmann::json;

/** \brief This will generate a default configuration for an SCT module in JSON format
    Configuration stored in m_cfg. Use WriteToFile() to store on disk.
 */
void ConfigHandlerSCT::MakeTemplateConfig ()
{
  m_cfg["ID"] = 0;
  m_cfg["PlaneID"] = 0;
  m_cfg["TRBChannel"] = 0;
  
  std::vector<json> Chips;
  for (unsigned int i = 0; i < 12; i++){
    json ChipRegisters;
    ChipRegisters["Address"] = 0;
    ChipRegisters["StrobeDelay"] = 0;
    ChipRegisters["Threshold"] = 0;
    ChipRegisters["BiasDAC"] = 0x1819;
    std::vector<int> trimDacs;
    trimDacs.resize(128); // 1 11 bit word per strip, 4 LSB = value, 7 MBS = channel
    ChipRegisters["TrimDAC"] = trimDacs;
    ChipRegisters["ConfigRegister"] = 0x800;
    
    if (i < 6)  { ChipRegisters["Address"] = 0x20+i; ChipRegisters["ConfigRegister"] = 0x84E; }
    else        { ChipRegisters["Address"] = 0x28+(i-6); ChipRegisters["ConfigRegister"] = 0x84E; }
    if (i == 0) ChipRegisters["ConfigRegister"] = 0x204E;
    if (i == 6) ChipRegisters["ConfigRegister"] = 0x204E; // Master-Bit enabeled
    if (i == 5) ChipRegisters["ConfigRegister"] = 0x104E; // End-Bit enabeled
    if (i == 11)ChipRegisters["ConfigRegister"] = 0x104E; // End-Bit enabeled
    
    std::vector<int> stripmask = {0,0,0,0}; // 4*32bit = 128 bit strip mask
    ChipRegisters["StripMask"] = stripmask;

    ChipRegisters["TrimTarget"]=0;
    ChipRegisters["p0"]=0;
    ChipRegisters["p1"]=0;
    ChipRegisters["p2"]=0;
    
    Chips.push_back(ChipRegisters);
  }
  m_cfg["Chips"] = Chips;
}

/** \brief Enables only a single chip
 */
void ConfigHandlerSCT::EnableSingleChip (unsigned int chipID)
{
  std::vector<unsigned int> vec{chipID};
  EnableChips(vec);
  vec.clear();
}

/** \brief Enables a set of chips
 */
void ConfigHandlerSCT::EnableChips( std::vector<unsigned int> chipIDs )
{
  if (m_cfg["Chips"].size() != 0){
    m_cfg["Chips"].clear();
  }
  std::vector<json> Chips;

  for(auto id : chipIDs) {

    if(id<32 || id==38 || id==39 || id>45){
      ERROR("invalid chip address " << id );
      throw;
    }
    
    json ChipRegisters;
    ChipRegisters["Address"] = id;
    ChipRegisters["StrobeDelay"] = 0;
    ChipRegisters["Threshold"] = 0;
    ChipRegisters["BiasDAC"] = 0x1819;
    std::vector<int> trimDacs;
    trimDacs.resize(128); // 1 11 bit word per strip, 4 LSB = value, 7 MBS = channel
    ChipRegisters["TrimDAC"] = trimDacs;

    int idx = (id < 40) ? id-32 : id-40;

    ChipRegisters["ConfigRegister"] =  0x800; 
    if (idx == 0) ChipRegisters["ConfigRegister"] = 0x2000; // Master-Bit enabled
    if (idx == 5) ChipRegisters["ConfigRegister"] = 0x1000; // End-Bit enabled
    
    std::vector<int> stripmask = {0,0,0,0}; // 4*32bit = 128 bit strip mask
    ChipRegisters["StripMask"] = stripmask;
  
    Chips.push_back(ChipRegisters);
  }
  
  m_cfg["Chips"] = Chips;
}


/** \brief Write current configuration object to file
 */
void ConfigHandlerSCT::WriteToFile (std::string filename)
{
  std::ofstream file;
  file.open(filename);
  if (!file.is_open()){
    ERROR("could not open output file "<<filename);
    return;
  }
  
  file << std::setw(2)<<m_cfg;
  file.close();
}

/** \brief Read configuration object from file
 */
void ConfigHandlerSCT::ReadFromFile (std::string filename)
{
  std::ifstream file;
  file.open(filename);
  if (!file.is_open()){
    ERROR("could not open input file "<<filename);
    return;
  }
  m_cfg.clear();
  file >> m_cfg;
  file.close();
  // ensure some fields exist
  if (!m_cfg.contains("ApplySCTMasks")){ 
    SetApplySCTMasks(defaults::sct_masking);
  }
}

/** *****************************************************************
 * \brief Set the 2 bit readout mode (bits 0-1 of the configuratino register).
 * 0x0: 1XX or X1X or XX1 (Hit): used for detector alignment
 * 0x1: X1X (Level): data taking
 * 0x2: 01X (Edge) : data taking
 * 0x3: XXX (Test) : Test mode: sends a hit if the mask register hold a 0
 * \param value: 0x0 .. 0x3
 ****************************************************************** */
void ConfigHandlerSCT::SetReadoutMode(uint16_t value)
{
  if (value > 0x3) {
    ERROR("invalid readout mode passed "<<std::hex<<value);
    throw;
  }

  for (unsigned int chip = 0; chip < m_cfg["Chips"].size(); chip++ ){
    m_cfg["Chips"][chip]["ConfigRegister"] = ((uint16_t)m_cfg["Chips"][chip]["ConfigRegister"]) & 0xFFFC; // set bits 0,1 to 0
    m_cfg["Chips"][chip]["ConfigRegister"] = ((uint16_t)m_cfg["Chips"][chip]["ConfigRegister"]) | value;
  }
}


/** *****************************************************************
 * \brief Set the 2 bit calibration mode. WARNING: bits behave differently than documented!
 * 0x0: strip 0 ... 4 ... active
 * 0x1: strip 1 ... 5 ... active
 * 0x2: strip 2 ... 6 ... active
 * 0x3: strip 3 ... 7 ... active
 * \param value: 0x0 .. 0x3
 ****************************************************************** */
void ConfigHandlerSCT::SetCalMode(uint16_t value)
{
  if (value > 0x3) {
    ERROR("invalid calibration mode passed "<<std::hex<<value);
    throw;
  }
  value <<= 2; // move 2 bit cal mode to bits 2-3
  for (unsigned int chip = 0; chip < m_cfg["Chips"].size(); chip++ ){
    m_cfg["Chips"][chip]["ConfigRegister"] = ((uint16_t)m_cfg["Chips"][chip]["ConfigRegister"]) & 0xFFF3; // set bits 2,3 to 0
    m_cfg["Chips"][chip]["ConfigRegister"] = ((uint16_t)m_cfg["Chips"][chip]["ConfigRegister"]) | value;
  }
}


/** *****************************************************************
 * \brief Enable / disable edge detection mode 
 ****************************************************************** */
void ConfigHandlerSCT::SetEdgeDetection(bool value)
{
  for (unsigned int chip = 0; chip < m_cfg["Chips"].size(); chip++ ){
    m_cfg["Chips"][chip]["ConfigRegister"] = ((uint16_t)m_cfg["Chips"][chip]["ConfigRegister"]) & 0xFFBF; // set bits 6 to 0
    if (value == true)
      m_cfg["Chips"][chip]["ConfigRegister"] = ((uint16_t)m_cfg["Chips"][chip]["ConfigRegister"]) | 0x40; // enable bit 6
  }
}

/** *****************************************************************
 * \brief Set the strobe delay
 * \param chipID this is the chipID on the module 0x20 .. 0x38
 * \param value strobe delay (6-bit value)
 ****************************************************************** */
void ConfigHandlerSCT::SetStrobeDelay(unsigned int chipID, unsigned int value)
{
  if(!check_chipID(chipID)){ throw; }
  
  if (value > 0x3f) {
    ERROR("strobe delay value larger than 0x3f.");
    return;
  }
  json chips = m_cfg["Chips"];
  unsigned int i = 0;
  for (auto chip : chips){
    if (chip["Address"] == chipID){ break; }
    i++;
  }
  m_cfg["Chips"][i]["StrobeDelay"] = value;
  
}

/** *****************************************************************
 * \brief Get the strobe delay
 * \param chipID this is the chipID on the module 0x20 .. 0x38
 ****************************************************************** */
unsigned int ConfigHandlerSCT::GetStrobeDelay(unsigned int chipID)
{
  if(!check_chipID(chipID)){ throw; }
  
  json chips = m_cfg["Chips"];
  unsigned int i = 0;
  for (auto chip : chips){
    if (chip["Address"] == chipID){ break; }
    i++;
  }
  return m_cfg["Chips"][i]["StrobeDelay"];
}

/** *****************************************************************
 * \brief Get the threshold calibration register value
 * \param chipID this is the chipID on the module 0x20 .. 0x38
 * \param thr Threshold value in DAC units
 * \param calib Input charge in units fC
 * \note This is what is to be sent to SCTSlowCommandBuffer
 ****************************************************************** */
unsigned int ConfigHandlerSCT::GetThresholdCalibReg(unsigned int chipID, unsigned int thr, unsigned int calib)
{
  if(!check_chipID(chipID)){ throw; }
  
  if (thr > 0xff) {
    ERROR("threshold value larger than 0xff.");
    throw;
  }
  if (calib > 0xff) {
    ERROR("calibration value larger than 0xff.");
    throw;
  }
  
  json chips = m_cfg["Chips"];
  unsigned int i = 0;
  for (auto chip : chips){
    if (chip["Address"] == chipID){ break; }
    i++;
  }
  uint16_t value = thr << 8;
  value |= calib & 0xff;

  return value;
}

/** *****************************************************************
 * \brief Get the threshold calibration register value
 * \details The threshold value is retrieved from the "TrimTarget" field of the loaded configuration.
 * \param chipID this is the chipID on the module 0x20 .. 0x38
 * \param calib This is the input charge in units fC. Default is 0.
 * \note This is what is to be sent to SCTSlowCommandBuffer
 * \throws std::runtime_error if "TrimTarget" field not found.
 ****************************************************************** */
unsigned int ConfigHandlerSCT::GetThresholdCalibReg(unsigned int chipID, unsigned int calib)
{

 if (hasTrimTarget( chipID )) {
   unsigned int value = (unsigned int)mV2dac(GetTrimTarget( chipID ));
   INFO("Setting threshold "<<GetTrimTarget(chipID)<<" mV"<<"( "<<value<<" DAC)");
   return GetThresholdCalibReg( chipID, value, calib); 
 }
 else { 
    throw std::runtime_error("No field 'TrimTarget' found in configurations.");
 }
}

/** *****************************************************************
 * \brief Set the threshold calib register value
 * \param chipID this is the chipID on the module 0x20 .. 0x38
 * \param thr Threshold in DAC units
 * \param calib Input charge in units fC
 ******************************************************************* */
void ConfigHandlerSCT::SetThresholdCalibReg(unsigned int chipID, unsigned int thr, unsigned int calib)
{
  if(!check_chipID(chipID)){ throw; }
  
  if (thr > 0xff) {
    ERROR("Threshold value larger than 0xff.");
    return;
  }
  if (calib > 0xff) {
    ERROR("Calibration value larger than 0xff.");
    return;
  }
  
  json chips = m_cfg["Chips"];
  unsigned int i = 0;
  for (auto chip : chips){
    if (chip["Address"] == chipID){ break; }
    i++;
  }
  uint16_t value = thr << 8;
  value |= calib & 0xff;
  m_cfg["Chips"][i]["Threshold"] = value;
}

/** *****************************************************************
 * \brief Set the threshold
 * \param chipID this is the chipID on the module 0x20 .. 0x38
 * \param value content of the threshold register, holding the threshold in the 8 MSB and the calibration value in the 8 LSB
 ****************************************************************** */
void ConfigHandlerSCT::SetThreshold(unsigned int chipID, unsigned int value)
{
  if(!check_chipID(chipID)){ throw; }
  
  if (value > 0xff) {
    ERROR("Threshold value larger than 0xff.");
    return;
  }
  json chips = m_cfg["Chips"];
  unsigned int i = 0;
  for (auto chip : chips){
    if (chip["Address"] == chipID){ break; }
    i++;
  }
  m_cfg["Chips"][i]["Threshold"] = value;  
}

/** *****************************************************************
 * \brief Get the threshold
 * \param chipID this is the chipID on the module 0x20 .. 0x38
 * \attention Threshold in DAC units!
 ****************************************************************** */
unsigned int ConfigHandlerSCT::GetThreshold(unsigned int chipID)
{
  if(!check_chipID(chipID)){ throw; }
  
  json chips = m_cfg["Chips"];
  unsigned int i = 0;
  for (auto chip : chips){
    if (chip["Address"] == chipID){ break; }
    i++;
  }
  return m_cfg["Chips"][i]["Threshold"];
}

/** *****************************************************************
 * \brief Set the Bias DAC value
 * \param chipID this is the chipID on the module 0x20 .. 0x38
 * \param value bias dac value. Note: bits 0..4 = Shaper bias, bits 8 .. 12 = input bias
 ****************************************************************** */
void ConfigHandlerSCT::SetBiasDac(unsigned int chipID, unsigned int value)
{
  if(!check_chipID(chipID)){throw; }
  
  if (value > 0x1fff) {
    ERROR("bias DAC value larger than 0x1fff.");
    return;
  }
  json chips = m_cfg["Chips"];
  unsigned int i = 0;
  for (auto chip : chips){
    if (chip["Address"] == chipID){ break; }
    i++;
  }
  m_cfg["Chips"][i]["BiasDAC"] = value;
  
}

void ConfigHandlerSCT::SetBiasDac(uint16_t value)
{
  for (unsigned int chip = 0; chip < m_cfg["Chips"].size(); chip++ ){
    m_cfg["Chips"][chip]["BiasDAC"] =  value;
  }
}

/** *****************************************************************
 * \brief Get the bias dac value
 * \param chipID this is the chipID on the module 0x20 .. 0x38
 ****************************************************************** */
unsigned int ConfigHandlerSCT::GetBiasDac(unsigned int chipID)
{
  if(!check_chipID(chipID)){ throw; }
  
  json chips = m_cfg["Chips"];
  unsigned int i = 0;
  for (auto chip : chips){
    if (chip["Address"] == chipID){ break; }
    i++;
  }
  return m_cfg["Chips"][i]["BiasDAC"];
}

/** *****************************************************************
 * \brief Set the Trim DAC value
 * \param chipID this is the chipID on the module 0x20 .. 0x38
 * \param value trim DAC value. Note: bits 0..3 = DAC value, Bits 4..10 = channel ID
 ****************************************************************** */
void ConfigHandlerSCT::SetTrimDac(unsigned int chipID, unsigned int channel, unsigned int value)
{
  if(!check_chipID(chipID)){ throw; }
  
  if (value > 0xf) {
    ERROR("Trim DAC value larger than 0xf.");
    return;
  }
  if (channel > 0x7f) {
    ERROR("Trim DAC channel out of range.");
    return;
  }
  
  json chips = m_cfg["Chips"];
  unsigned int i = 0;
  for (auto chip : chips){
    if (chip["Address"] == chipID){ break; }
    i++;
  }
  unsigned int word = value;
  word |= (channel << 4);
  m_cfg["Chips"][i]["TrimDAC"][channel] = word;
  
}

/** *****************************************************************
 * \brief Get the encoded trim dac value
 * \param chipID this is the chipID on the module 0x20 .. 0x38
 * \note If the encoded TrimDAC value retrieved from the SCT config file 
 *       does not have the expected channel ID encoded, the channel ID is
 *       encoded here along with a WARNING.
 ****************************************************************** */
unsigned int ConfigHandlerSCT::GetEncodedTrimDac(unsigned int chipID, unsigned int channel)
{
  if(!check_chipID(chipID)){ throw; }
  if (channel > 0x7f) {
    ERROR("Trim DAC channel out of range.");
    return 0;
  }
  
  json chips = m_cfg["Chips"];
  unsigned int i = 0;
  for (auto chip : chips){
    if (chip["Address"] == chipID){ break; }
    i++;
  }
  unsigned int word = m_cfg["Chips"][i]["TrimDAC"][channel];
  unsigned int encoded_channel = channel<<4;
  if ((word & 0xFF0) != encoded_channel) { 
    WARNING("Encoded TrimDAC word "<<std::hex<<word<<std::dec<<" does not have the expected channel ID "<<channel<<". Embedding the channel id before returning the encoded TrimDAC.");
    word = encoded_channel|(word&0x00F);
    usleep(2e3); // wait a short bit while WARNING msg propagated to log.
  }
  return word;
}

/** *****************************************************************
 * \brief Set the chip configuration register
 * \param chipID this is the chipID on the module 0x20 .. 0x38
 * \param value chip config register
 ****************************************************************** */
void ConfigHandlerSCT::SetConfigReg(unsigned int chipID, uint16_t value)
{
  if(!check_chipID(chipID)){ throw; }
  
  json chips = m_cfg["Chips"];
  unsigned int i = 0;
  for (auto chip : chips){
    if (chip["Address"] == chipID){ break; }
    i++;
  }
  m_cfg["Chips"][i]["ConfigRegister"] = value;
  
}

/** *****************************************************************
 * \brief Get the configuration register
 * \param chipID this is the chipID on the module 0x20 .. 0x38
 ****************************************************************** */
unsigned int ConfigHandlerSCT::GetConfigReg(unsigned int chipID)
{
  if(!check_chipID(chipID)){ throw; }
  
  json chips = m_cfg["Chips"];
  unsigned int i = 0;
  for (auto chip : chips){
    if (chip["Address"] == chipID){ break; }
    i++;
  }
  return m_cfg["Chips"][i]["ConfigRegister"];
}

/** *****************************************************************
 * \brief Set the StripMask
 *
 * \param chipID this is the chipID on the module 0x20 .. 0x38
 * \param vec vector of size 8 containing 16-bit words corresponding to 
 * channel mask. 
 ****************************************************************** */
void ConfigHandlerSCT::SetStripMask(unsigned int chipID, std::vector<uint16_t> vec)
{
  if(!check_chipID(chipID)){ throw; }
  if( (int)vec.size() != 8 ) { 
    ERROR("Size of mask vector is not 4 but " << vec.size());
    throw;
  }
  
  json chips = m_cfg["Chips"];
  unsigned int i = 0;
  for (auto chip : chips){
    if (chip["Address"] == chipID){ break; }
    i++;
  }
  
  m_cfg["Chips"][i]["StripMask"].clear();
  for(auto v : vec)
    m_cfg["Chips"][i]["StripMask"].push_back(v);
}


void ConfigHandlerSCT::SetStripMaskBit(unsigned int /*chipID*/, unsigned int /*strip*/, bool /*mask*/)
{
}

/** *****************************************************************
 * \brief Returns a vector containing 16-bit words corresponding to 
 * channel mask.
 * \throws std::runtime_error
 ****************************************************************** */
std::vector<uint16_t> ConfigHandlerSCT::GetStripMask(unsigned int chipID)
{
  if(!check_chipID(chipID)){ throw; }
  
  json chips = m_cfg["Chips"];
  unsigned int i = 0;
  for (auto chip : chips){
    if (chip["Address"] == chipID){ break; }
    i++;
  }

  std::vector<uint16_t> vec;
  if ( m_cfg["Chips"][i].contains("StripMask") ) {
    for(auto m : m_cfg["Chips"][i]["StripMask"] ){
      vec.push_back(m);
    }
    if (vec.size() != 8 ) { throw std::runtime_error("Invalid number of elements in StripMask configurations."); }
  }
  else { throw std::runtime_error("No field 'StripMask' found in configurations."); }

  return vec;
}

/** *****************************************************************
 * \brief Returns a vector containing 16-bit words corresponding to 
 * channel mask used for enabling strips/masking noisy strips.
 ****************************************************************** */
std::vector<uint16_t> ConfigHandlerSCT::GetStripMaskRegister(unsigned int chipID)
{
  if(!check_chipID(chipID)){ throw; }
  
  json chips = m_cfg["Chips"];
  unsigned int i = 0;
  for (auto chip : chips){
    if (chip["Address"] == chipID){ break; }
    i++;
  }
  
  auto cfg_stripmasks = GetStripMask(chipID); // may throw exception

  // write out to mask array 
  int mask[128];

  for(unsigned int i=0; i<8; i++){
    uint16_t word = cfg_stripmasks[i];
    for(unsigned int j=0; j<16; j++){
      int ichan(16*i+j);
      int val = word & (0x1 << j);
      mask[ichan] = val;
    }
  }

  std::vector<uint16_t> vmask;
  
  for(int i=7; i>=0; i--){
    // word corresponding to enable all channels    
    uint16_t maskword = 0xFFFF; 	
    
    uint16_t block = 0;	   
    for(int j=15; j>=0; j--){
      unsigned int istrip = 16*i+j;
      block <<= 1;
      if( mask[istrip] == 0 ){ // channel to be masked
	block |= 1;
      }
      else INFO(" masking strip "<<std::dec<<istrip);
    }
    
    maskword &= block;
    vmask.push_back(maskword);
  }

  return vmask;

}

/** *****************************************************************
 * \brief Returns a boolean flag if the chip ID has a 
 * TrimTarget attribute.
 ****************************************************************** */
bool ConfigHandlerSCT::hasTrimTarget(unsigned int chipID){
  if(!check_chipID(chipID)){ throw; }
  
  json chips = m_cfg["Chips"];
  unsigned int i = 0;
  for (auto chip : chips){
    if (chip["Address"] == chipID){ break; }
    i++;
  }
  return m_cfg["Chips"][i].contains("TrimTarget");
}

/** *****************************************************************
 * \brief Returns a boolean flag if the chip ID has a 
 * TrimDAC attribute.
 ****************************************************************** */
bool ConfigHandlerSCT::hasTrimDAC(unsigned int chipID){
  if(!check_chipID(chipID)){ throw; }
  
  json chips = m_cfg["Chips"];
  unsigned int i = 0;
  for (auto chip : chips){
    if (chip["Address"] == chipID){ break; }
    i++;
  }
  return m_cfg["Chips"][i].contains("TrimDAC");
}

/** *****************************************************************
 * \brief Returns a boolean flag if the chip ID has a 
 * StripMask attribute. 
 ****************************************************************** */
bool ConfigHandlerSCT::hasStripMask(unsigned int chipID){
  if(!check_chipID(chipID)){ throw; }
  
  json chips = m_cfg["Chips"];
  unsigned int i = 0;
  for (auto chip : chips){
    if (chip["Address"] == chipID){ break; }
    i++;
  }
  return m_cfg["Chips"][i].contains("StripMask");
}

/** *****************************************************************
 * \brief Set the trim target
 * \param chipID this is the chipID on the module 0x20 .. 0x38
 * \param value threshold target to which the chip has been trimmed
 ****************************************************************** */
void ConfigHandlerSCT::SetTrimTarget(unsigned int chipID, double value){
  if(!check_chipID(chipID)){ throw; }
  
  json chips = m_cfg["Chips"];
  unsigned int i = 0;
  for (auto chip : chips){
    if (chip["Address"] == chipID){ break; }
    i++;
  }
  m_cfg["Chips"][i]["TrimTarget"] = value;  
}

/** *****************************************************************
 * \brief Get the trim target
 * \param chipID this is the chipID on the module 0x20 .. 0x38
 * \attention TrimTarget in units mV!
 ****************************************************************** */
double ConfigHandlerSCT::GetTrimTarget(unsigned int chipID){
  if(!check_chipID(chipID)){ throw; }
  
  json chips = m_cfg["Chips"];
  unsigned int i = 0;
  for (auto chip : chips){
    if (chip["Address"] == chipID){ break; }
    i++;
  }
  return m_cfg["Chips"][i]["TrimTarget"];
}

/** *****************************************************************
 * \brief Set parameter from response-curve fit
 * \param chipID this is the chipID on the module 0x20 .. 0x38
 * \param value of the parameter
 * \param param string for the fit parameter (p0, p1 or p2)
 ****************************************************************** */
void ConfigHandlerSCT::SetParam(unsigned int chipID, double value,
				std::string param){
  if(!check_chipID(chipID)){ throw; }

  if(param!="p0" && param!="p1" && param!="p2"){ throw;  }
  
  json chips = m_cfg["Chips"];
  unsigned int i = 0;
  for (auto chip : chips){
    if (chip["Address"] == chipID){ break; }
    i++;
  }
  m_cfg["Chips"][i][param] = value;  
}

void ConfigHandlerSCT::SetP0(unsigned int chipID, double value){
  SetParam(chipID,value,"p0");
}

void ConfigHandlerSCT::SetP1(unsigned int chipID, double value){
  SetParam(chipID,value,"p1");  
}

void ConfigHandlerSCT::SetP2(unsigned int chipID, double value){
  SetParam(chipID,value,"p2");
}

/** *****************************************************************
 * \brief Get parameter from response-curve fit
 * \param chipID this is the chipID on the module 0x20 .. 0x38
 * \param param string for the fit parameter (p0, p1 or p2)
 ****************************************************************** */
double ConfigHandlerSCT::GetParam(unsigned int chipID, std::string param){
  if(!check_chipID(chipID)){ throw; }
  if(param!="p0" && param!="p1" && param!="p2"){ throw;  }
  
  json chips = m_cfg["Chips"];
  unsigned int i = 0;
  for (auto chip : chips){
    if (chip["Address"] == chipID){ break; }
    i++;
  }
  return m_cfg["Chips"][i][param];
}

double ConfigHandlerSCT::GetP0(unsigned int chipID){
  return GetParam(chipID,"p0");
}

double ConfigHandlerSCT::GetP1(unsigned int chipID){
  return GetParam(chipID,"p1");  
}

double ConfigHandlerSCT::GetP2(unsigned int chipID){
  return GetParam(chipID,"p2");
}

/** *****************************************************************
 * \brief Returns a vector of all chip IDs in the configuration
 ****************************************************************** */
std::vector <unsigned int> ConfigHandlerSCT::GetChipIDs()
{
  std::vector<unsigned int> chipIDs;
  json chips = m_cfg["Chips"];
  for (auto chip : chips){
    chipIDs.push_back(chip["Address"]);
  }
  return chipIDs;
}

/** helper function to check if the chip ID is valid */
bool ConfigHandlerSCT::check_chipID(unsigned int chipID)
{
  if (chipID < 0x20 || chipID > 0x3D ){
    ERROR("Chip address out of range: "<<std::hex<<chipID);
    return false;
  }
  return true;
}

unsigned int ConfigHandlerSCT::mV2dac( float mV ){
  unsigned int dac = mV/2.5;
  return dac;
}
